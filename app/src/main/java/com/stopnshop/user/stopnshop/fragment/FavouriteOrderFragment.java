package com.stopnshop.user.stopnshop.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.stopnshop.user.stopnshop.NetworkUtil;
import com.stopnshop.user.stopnshop.adapter.FavouriteOrderAdapter;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.model.orderList.Datum;
import com.stopnshop.user.stopnshop.model.orderList.OrderListData;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FavouriteOrderFragment extends Fragment {
    private static AQuery query;
    private static RecyclerView recyclerView;

    static List<Datum> favOrderList;

//    public static FavouriteOrderFragment favouriteOrderFragment;

    Activity context;
    private Gson gson;

    private Login_AllData logindata;
    private String flag;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context= (Activity) context;
//        favouriteOrderFragment=this;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(Constant.INTERNET_NOT_CONNECTED!= NetworkUtil.getConnectionStatus(context)){

            getFavoriteItemList(logindata,context);

        }else {

            UtilView.showToast(context,"Internet not Connected");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favourite_order, container, false);

        gson = new Gson();

        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_favouriteorder);

        LinearLayoutManager layoutManager=new LinearLayoutManager(context);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);

       flag= SharedPreferenceHelper.getSharedPreferenceString(getActivity(),Constant.skip,"");

        if (flag!=null&&flag.equals("skip")){

        }else {

            logindata=Constant.getlogindata(getActivity());
        }

        getFavoriteItemList(logindata,context);

      return view;
    }

   public static void getFavoriteItemList(Login_AllData logindata, final Activity context){
        final Data data=new Data();
//       logindata=Constant.getlogindata(context);
        data.setUserId(logindata.getId());

        ServerRequestApi pozo = new ServerRequestApi("favorite_orderList", data);

       final Gson gson = new Gson();
       query=new AQuery(context);

        String gsonString = gson.toJson(pozo, ServerRequestApi.class);

        UtilView.showLogCat("orderfavoriteList",gsonString);

       ProgressDialog progressbar = UtilView.showProgressDialog(context);

       Map<String, Object> params = null;
       StringEntity sEntity = null;

       try {
           sEntity = new StringEntity(gsonString, "UTF-8");
       } catch (UnsupportedEncodingException e) {
           e.printStackTrace();
       }
       params = new HashMap<String, Object>();

       params.put(query.POST_ENTITY, sEntity);

       query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

           @Override
           public void callback(String url, JSONObject json, AjaxStatus status) {
               super.callback(url, json, status);

               if (json != null) {

                   UtilView.showLogCat("signinresult",json.toString());
                   try {

                       if (json.getInt("msg_code") == 1) {

                           //AllOrderFragment.allOrderFragment.updateAdapter();
                           recyclerView.setVisibility(View.VISIBLE);

                           OrderListData favOrder=gson.fromJson(json.toString(),OrderListData.class);
                           favOrderList=favOrder.getData();

                           List<Datum>favDatumList=new ArrayList<Datum>();
                           for(Datum datum:favOrderList) {
                               if (datum.getOrder_status() != null && datum.getOrder_status().equals("0")
                                       || datum.getOrder_status().equals("1")
                                       || datum.getOrder_status().equals("2")
                                       || datum.getOrder_status().equals("3")) {

                                   favDatumList.add(datum);
                               }
                           }
                               FavouriteOrderAdapter mAdapter = new FavouriteOrderAdapter(context, favDatumList);
                               recyclerView.setAdapter(mAdapter);
                               // mAdapter.notifyDataSetChanged();

                       } else {

                           recyclerView.setVisibility(View.GONE);
                           Log.e("message", json.getString("message"));
                       }

                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
               } else {recyclerView.setVisibility(View.GONE);

               }
           }
       }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    public void updateAdapter(){
        getFavoriteItemList(logindata,context);
    }
}