package com.stopnshop.user.stopnshop.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.activity.ComboDealActivity;
import com.stopnshop.user.stopnshop.activity.ComboProductDetailActivity;
import com.stopnshop.user.stopnshop.model.ComboDeal.ComboDatum;

import java.util.List;


/**
 * Created by neha on 6/12/17.
 */

public class ComboAdapter extends RecyclerView.Adapter<ComboAdapter.ViewHolder> {

    List<ComboDatum> comboList;
    Activity context;

    public ComboAdapter(ComboDealActivity comboDealActivity, List<ComboDatum> comboDatumList) {
        this.context=comboDealActivity;
        this.comboList=comboDatumList;
    }

    @Override
    public ComboAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adpter_combodeal,parent,false);
        return new ComboAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ComboAdapter.ViewHolder holder, final int position) {

        ComboDatum datum=comboList.get(position);

        if (datum.getName()!=null){
            holder.tvComboName.setText(datum.getName());
        }
        if (datum.getDescription()!=null){
            holder.tvDescription.setText(datum.getDescription());
        }
        if (datum.getPrice1()!=null){
            holder.tvComboPrice.setText("Price: $"+datum.getPrice1());
        }
        if (datum.getImage()!=null){

            Picasso.with(context)
                    .load(datum.getImage())
                    .error(R.drawable.icon)
                    .into(holder.imgCombo);
        }

        holder.llComboDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context,ComboProductDetailActivity.class);
                intent.putExtra("comboList", comboList.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return comboList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvComboName,tvDescription,tvComboPrice;
        ImageView imgCombo;
        LinearLayout llComboDeal;

        public ViewHolder(View itemView) {
            super(itemView);

            tvComboName=(TextView)itemView.findViewById(R.id.tv_combodealName);
            tvDescription=(TextView)itemView.findViewById(R.id.tv_description);
            tvComboPrice=(TextView)itemView.findViewById(R.id.tv_price);
            imgCombo=(ImageView)itemView.findViewById(R.id.image_combo);
            llComboDeal=(LinearLayout)itemView.findViewById(R.id.ll_comboDeal);
        }
    }
}
