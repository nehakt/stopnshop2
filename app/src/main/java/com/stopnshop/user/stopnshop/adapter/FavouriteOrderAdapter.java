package com.stopnshop.user.stopnshop.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.stopnshop.user.stopnshop.View.MainActivity;
import com.stopnshop.user.stopnshop.activity.FavouriteOrderDetail_Activity;
import com.stopnshop.user.stopnshop.model.orderList.Datum;
import com.stopnshop.user.stopnshop.R;

import java.util.List;

/**
 * Created by saurabh on 28-Aug-17.
 */

public class FavouriteOrderAdapter  extends RecyclerView.Adapter<FavouriteOrderAdapter.MyViewHolder>
{
    private List<Datum> favOrderList;
    MainActivity context;
    Cart cart;

        public FavouriteOrderAdapter(Activity context, List<Datum> favOrderList) {
       this.favOrderList=favOrderList;
        this.context = (MainActivity) context;
    }

    @Override
    public FavouriteOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_favitemlist, parent, false);

        cart= CartHelper.getCart();

        return new FavouriteOrderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FavouriteOrderAdapter.MyViewHolder holder, final int position) {

        Datum favData=favOrderList.get(position);

        if (favData.getOrderId()!=null){
            holder.tvOrderNo.setText("OrderId: "+favData.getOrderId());
        }
        if (favData.getAddedDate()!=null){
            holder.tvOrderDate.setText(favData.getAddedDate());
        }
        if (favData.getPickuptime()!=null){
            holder.tvPickupDate.setText("Pickup Time: "+favData.getPickuptime());
        }
        if(favData.getTotolItem()!=null){
            holder.tvTotalItems.setText("Total Items: "+favData.getTotolItem());
        }
        if (favData.getAmout()!=null){
            holder.tvTotalAmount.setText("Total Amount: $"+favData.getAmout());
        }
        if (favData.getOrder_status().equals("0")){
            holder.tvNotificationTxt.setText("I'am outside");
            holder.tvNotificationTxt.setTextColor(Color.WHITE);
            holder.tvNotificationTxt.setBackgroundColor(Color.RED);
        }else if(favData.getOrder_status().equals("1"))
        {
            holder.tvNotificationTxt.setText("In Progress");
            holder.tvNotificationTxt.setTextColor(Color.GREEN);
            holder.tvNotificationTxt.setBackgroundColor(0x00000000);
        }else if (favData.getOrder_status().equals("2"))
        {
            holder.tvNotificationTxt.setText("Order Picked Up");
            holder.tvNotificationTxt.setTextColor(Color.GREEN);
            holder.tvNotificationTxt.setBackgroundColor(0x00000000);
//            holder.tvReOrder.setVisibility(View.VISIBLE);
        }
        else if (favData.getOrder_status().equals("3"))
        {
            holder.tvNotificationTxt.setText("Cancelled");
            holder.tvNotificationTxt.setTextColor(Color.RED);
            holder.tvNotificationTxt.setBackgroundColor(0x00000000);
//            holder.tvReOrder.setVisibility(View.VISIBLE);
        }

        holder.imgFav.setImageResource(R.drawable.favorite_white_b);

        holder.favlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context, FavouriteOrderDetail_Activity.class);
                            intent.putExtra("favorderList", favOrderList.get(position));
                            context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount()

    {
        return favOrderList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvOrderNo,tvOrderDate,tvPickupDate,tvTotalItems,tvTotalAmount,tvNotificationTxt,tvReOrder;
        ImageView imgFav;
        LinearLayout favlayout;

    public MyViewHolder(View view) {
            super(view);

        tvOrderNo=(TextView)view.findViewById(R.id.tv_orderNo);
        tvOrderDate=(TextView)view.findViewById(R.id.tv_orderDate);
        tvPickupDate=(TextView)view.findViewById(R.id.tv_pickupDate);
        tvTotalItems=(TextView)view.findViewById(R.id.tv_totalItems);
        tvTotalAmount=(TextView)view.findViewById(R.id.tv_totalAmount);
        imgFav=(ImageView) view.findViewById(R.id.img_fav);
        tvNotificationTxt=(TextView)view.findViewById(R.id.notification_txt);
        tvReOrder=(TextView)view.findViewById(R.id.tv_reOrder);
        favlayout=(LinearLayout)view.findViewById(R.id.layout1);

        }
    }

//    public void getFavDeatil(final int position){
//
//        Data data=new Data();
//        data.setOrderId(favOrderList.get(position).getOrderId());
//
//        ServerRequestApi server_request=new ServerRequestApi("FavoriteorderDetail",data);
//        Gson gson =new Gson();
//        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
//        UtilView.showLogCat("FavOrderDetailReq",gsonString);
//
//        ProgressDialog progressbar = UtilView.showProgressDialog(context);
//        AQuery query=new AQuery(context);
//
//        Map<String, Object> params = null;
//        StringEntity sEntity = null;
//
//        try {
//            sEntity = new StringEntity(gsonString, "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        params = new HashMap<String, Object>();
//
//        params.put(query.POST_ENTITY, sEntity);
//        //Intent intent=new Intent(context, OrderSummryActivity.class);
////                        startActivity(intent);
//        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {
//
//            @Override
//            public void callback(String url, JSONObject json, AjaxStatus status) {
//                super.callback(url, json, status);
//
//                if (json != null) {
//
//                    UtilView.showLogCat("orderListResult",json.toString());
//                    try {
//                        if (json.getInt("msg_code") == 1) {
//
//                            Intent intent=new Intent(context, FavouriteOrderDetail_Activity.class);
//                            intent.putExtra("favorderList", favOrderList.get(position));
//                            context.startActivity(intent);
//
//                        } else
//                        {
//                            Log.e("message", json.getString("message"));
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//
//                }
//            }
//        }.header("Content-Type", "application/x-www-form-urlencoded"));
//    }

}