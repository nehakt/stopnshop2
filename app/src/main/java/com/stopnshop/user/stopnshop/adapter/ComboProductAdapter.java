package com.stopnshop.user.stopnshop.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.activity.ComboProductDetailActivity;
import com.stopnshop.user.stopnshop.model.ComboDeal.ProductDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neha on 6/12/17.
 */

public class ComboProductAdapter extends RecyclerView.Adapter<ComboProductAdapter.ViewHolder> {

    Activity context;
    List<ProductDetail> comboProductList;

    public ComboProductAdapter(ComboProductDetailActivity comboProductDetailActivity, ArrayList<ProductDetail> comboProductDatum) {

        this.context=comboProductDetailActivity;
        this.comboProductList=comboProductDatum;
    }
    @Override
    public ComboProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_comboproduct,parent,false);
        return new ComboProductAdapter.ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(ComboProductAdapter.ViewHolder holder, int position) {

        ProductDetail proDetail=comboProductList.get(position);

        if(proDetail.getProductName()!=null){
            holder.tvProductName.setText(proDetail.getProductName());
        }
        if (proDetail.getDescription()!=null){
            holder.tvProductDescription.setText(proDetail.getDescription());
        }
        if (proDetail.getAddedDate()!=null){
            holder.tvAddedDate.setText(proDetail.getAddedDate());
        }
        if (proDetail.getImage()!=null){
            Picasso .with(context)
                    .load(proDetail.getImage())
                    .error(R.drawable.icon)
                    .into(holder.imgProduct);
        }
    }

    @Override
    public int getItemCount() {
        return comboProductList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView tvProductName,tvProductDescription,tvAddedDate;
        ImageView imgProduct;

        public ViewHolder(View itemView) {
            super(itemView);
            tvProductName=(TextView)itemView.findViewById(R.id.tv_productName);
            tvProductDescription=(TextView)itemView.findViewById(R.id.tv_productDescription);
            tvAddedDate=(TextView)itemView.findViewById(R.id.tv_addedDate);
            imgProduct=(ImageView)itemView.findViewById(R.id.img_productImage);
        }
    }
}