package com.stopnshop.user.stopnshop.adapter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.stopnshop.user.stopnshop.View.MainActivity;
import com.stopnshop.user.stopnshop.activity.ItemDetailActivity;
import com.stopnshop.user.stopnshop.fragment.Fragment_OnlineBuy;
import com.stopnshop.user.stopnshop.model.GetProductListData;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.Msg;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ItemsAdapter1 extends RecyclerView.Adapter<ItemsAdapter1.MyViewHolder>implements Filterable
{
    private Login_AllData logindata = null;

    MainActivity context;
    AQuery query;
    Gson gson;

    String productid;
    Cart cart;

    String flag;

    private List<GetProductListData1> data1;
//    private List<GetProductListData1> filterdata = new ArrayList<>();
    List<GetProductListData> datum1;
    List<GetProductListData1> productdata;

//    1 true(isFavourite)
//    0 false(notFavourite)

    public ItemsAdapter1(MainActivity context, List<GetProductListData1> data, List<GetProductListData> datum) {
        this.context=context;
        this.datum1=datum;
        this.data1=data;
        this.productdata=data;
        this.productid= SharedPreferenceHelper.getSharedPreferenceString(context, Constant.skip,"");

        if (productid!=null&&productid.equals("skip")){

        }else {

            this.logindata= Constant.getlogindata(context);
        }
    }
    @Override
    public ItemsAdapter1.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_itemslist, parent, false);

        query = new AQuery(context);
        gson = new Gson();

        cart = CartHelper.getCart();

        // AllFragment.allFragment.getUpdateMenu();

        return new ItemsAdapter1.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final GetProductListData1 datum = data1.get(position);

        flag=context.getIntent().getStringExtra("flag");

        if (flag!=null&&flag.equals("skip")){
            holder.llFav.setVisibility(View.GONE);
        }else {
            holder.llFav.setVisibility(View.VISIBLE);
        }
        //----------if status is 1 means item is Favourite-------------
        if (datum.getFavoriteStatus()!=null&&data1.get(position).getFavoriteStatus().equals("1")){

            holder.favorite_image.setImageResource(R.drawable.favorite_btn_b);
        }
        //----------if status is 0 means item is not Favourite-------------
        if (datum.getFavoriteStatus()!=null&&data1.get(position).getFavoriteStatus().equals("0")){
            holder.favorite_image.setImageResource(R.drawable.favorite_btn_a);
        }

        holder.itemName.setText( datum.getName());
        holder.price.setText("$ " +datum.getPrice1());

        Picasso.with(context)
                .load(datum.getImage())
                .error(R.drawable.iimg_not_avlbl)
                .placeholder(R.drawable.iimg_not_avlbl)
                .into(holder.imgItem);

        holder.frameItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ItemDetailActivity.class);
                intent.putExtra("datum",data1.get(position));//send datum in ItemDetailActivity
                context.startActivity(intent);
            }
        });

        holder.tv_incerement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String quantity =holder.tv_qty.getText().toString();
                int finalquantity = Integer.parseInt(quantity)+1;
                holder.tv_qty.setText(String.valueOf(finalquantity));
                notifyDataSetChanged();
            }
        });

        holder.tv_decerement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String quantity = holder.tv_qty.getText().toString();
                int finalquantity = Integer.parseInt(quantity)-1;

                if(finalquantity<1){
                    Toast.makeText(context,"Quantity should not be less then 1.",Toast.LENGTH_LONG).show();
                }else {
                    holder.tv_qty.setText(String.valueOf(finalquantity));
                }
                notifyDataSetChanged();

            }
        });

        holder.llAddtoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String quantity = holder.tv_qty.getText().toString();

                BigDecimal price=new BigDecimal(datum.getPrice1());
                datum.setPrice(price);
                datum.setpId1(Integer.parseInt(datum.getId()));
                cart.add(datum, Integer.parseInt(quantity),context);

                storedataToLocal(datum);
                //UtilView.setBadgeCount(context, cartmenuIcon, "" + cart.getTotalQuantity());

                SweetAlertDialog sweetAlertDialogView;
                sweetAlertDialogView=  new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                sweetAlertDialogView.setTitleText(data1.get(position).getName())
                        .setContentText("Added successfully.")
                        .show();

                context.getUpdateMenu();

            }
        });

        holder.favorite_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   pos=position;
                // favorite=!favorite;

                if (data1.get(position).getFavoriteStatus()!=null && data1.get(position).getFavoriteStatus().equals("1")){

                    holder.favorite_image.setImageResource(R.drawable.favorite_btn_a);

                    if (InternetConnection.isNetworkAvailable(context)) {

                        if (productid!=null&&productid.equals("skip")){

                        }else {

                            getRemoveFavourite(position);
                        }

                    } else {

                        Msg.t(context, "Please Check Internet Connection");
                    }
                }

                if (data1.get(position).getFavoriteStatus()!=null && data1.get(position).getFavoriteStatus().equals("0")){

                    holder.favorite_image.setImageResource(R.drawable.favorite_btn_b);

                    if (InternetConnection.isNetworkAvailable(context)) {

                        if (productid!=null&&productid.equals("skip")){

                        }else {

                            getAddFavourite(position);
                        }
                    } else {

                        Msg.t(context, "Please Check Internet Connection");
                    }
                }
                // allFragment.favoriteitem();
            }
        });
    }

    private void storedataToLocal(GetProductListData1 productDetail) {

        Gson gson = new Gson();

        String data =  SharedPreferenceHelper.getSharedPreferenceString(context,"cartdata","");

        Type type = new TypeToken<List<GetProductListData1>>(){}.getType();

        if(data!=null  && !data.equals("")){

            List<GetProductListData1> detail = gson.fromJson(data,type);

            for(int i=0;i<detail.size();i++){

                if(detail.get(i).getId().equals(productDetail.getId())){

                    int quantity = detail.get(i).getQuantity();

                    detail.remove(productDetail);

                    productDetail.setQuantity(quantity+1);

                }else {

                    productDetail.setQuantity(1);


                }
            }

            detail.add(productDetail);
            String storedata = gson.toJson(detail);

            SharedPreferenceHelper.setSharedPreferenceString(context,"cartdata",storedata);



        }else {

            List<GetProductListData1> list = new ArrayList<>();

            productDetail.setQuantity(1);

            list.add(productDetail);

            String storedata = gson.toJson(list);

            SharedPreferenceHelper.setSharedPreferenceString(context,"cartdata",storedata);
        }
    }

    public void getRemoveFavourite(final int position) {
        //   final int position=ItemsAdapter.pos;
        query = new AQuery(context);
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        Map<String, Object> params = null;
        StringEntity sEntity = null;

        Data data=new Data();

        data.setUserId(logindata.getId());
        data.setProductId(data1.get(position).getId());

        ServerRequestApi pozo = new ServerRequestApi("removeFavorite", data);
        final Gson gson = new Gson();
        String s = gson.toJson(pozo, ServerRequestApi.class);
        Log.e("@flow", s);

        try {
            sEntity = new StringEntity(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);

        query.progress(progressDialog).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                Log.e("@flow", json.toString());

                if (status.getCode() == 200) {

                    Log.e("@flow", json.toString());

                    try {

                        if (json.getInt("msg_code") == 1) {

                            GetProductListData1 object = data1.get(position);
                            object.setFavoriteStatus("0");
                            data1.set(position,object);
                            notifyDataSetChanged();

                            // data1.get(position).setFavoriteStatus(false);
//                            SharedPreferenceHelper.setSharedPreferenceBoolean(getActivity(),Constant.favorite,false);
//                            SharedPreferenceHelper.setSharedPreferenceString(getActivity(),Constant.productid,datum.get(position).getProductData().get(position).getId());
                        } else {

                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    public void getAddFavourite(final int position) {
        //   final int position=ItemsAdapter.pos;

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        Map<String, Object> params = null;
        StringEntity sEntity = null;

        Data data=new Data();

        data.setUserId(logindata.getId());
        data.setProductId(data1.get(position).getId());

        ServerRequestApi pozo = new ServerRequestApi("addFavorite", data);
        final Gson gson = new Gson();
        String s = gson.toJson(pozo, ServerRequestApi.class);
        Log.e("@flow", s);

        try {
            sEntity = new StringEntity(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);

        query.progress(progressDialog).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                Log.e("@flow", json.toString());

                if (status.getCode() == 200) {

                    Log.e("@flow", json.toString());

                    try {

                        if (json.getInt("msg_code") == 1) {

                            GetProductListData1 object = data1.get(position);
                            object.setFavoriteStatus("1");
                            data1.set(position,object);
                            notifyDataSetChanged();

                            //  data1.get(position).setFavoriteStatus(true);

//                            SharedPreferenceHelper.setSharedPreferenceBoolean(getActivity(),Constant.favorite,true);
//                            SharedPreferenceHelper.setSharedPreferenceString(getActivity(),Constant.productid,datum.get(position).getProductData().get(position).getId());
                        } else {

                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    @Override
    public int getItemCount()
    {
            return data1.size();
    }
//    public Object getItem(int position) {
//        return data1.get(position);
//    }
//    public long getItemId(int position) {
//        return position;
//    }

//-------------For Search Functionality------------
    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                int position = Fragment_OnlineBuy.position;
                if(position==0){

                }else {
                    productdata = datum1.get(position - 1).getProductData();
                }

                if (charString.isEmpty()) {

                    data1 = productdata;
                } else {
                    ArrayList<GetProductListData1> filteredList = new ArrayList<>();

                    for (GetProductListData1 productData : productdata) {

                        if (productData.getName().toLowerCase().contains(charString)) {
//                            || productData.getDescription().toLowerCase().contains(charString)) {

                            filteredList.add(productData);
                        }
                    }
                    data1 = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = data1;
                return filterResults;
            }
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                data1 = (List<GetProductListData1>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
//---------------- End Search Functionality-------

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView price, itemName,tv_incerement,tv_decerement,tv_qty;
        public ImageView imgItem,favorite_image;
        public FrameLayout frameItem;
        public LinearLayout llAddtoCart,llFav;

        public MyViewHolder(View view) {
            super(view);
            price = (TextView) view.findViewById(R.id.price);
            itemName = (TextView) view.findViewById(R.id.itemName);
            tv_incerement = (TextView) view.findViewById(R.id.tv_incerement);
            tv_decerement = (TextView) view.findViewById(R.id.tv_decerement);
            tv_qty = (TextView) view.findViewById(R.id.tv_qty);
            imgItem = (ImageView) view.findViewById(R.id.img_item);
            favorite_image = (ImageView) view.findViewById(R.id.favorite_image);
            frameItem=(FrameLayout) view.findViewById(R.id.frame_Item);
            llAddtoCart=(LinearLayout) view.findViewById(R.id.ll_addToCard);
            llFav=(LinearLayout)view.findViewById(R.id.ll_fav);
        }
    }
}