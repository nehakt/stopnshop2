package com.stopnshop.user.stopnshop.util;

import android.app.Activity;
import android.content.Context;

import com.google.gson.Gson;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;

/**
 * Created by shailendra on 13/9/17.
 */

public class Constant {

    public static final int INTERNET_NOT_CONNECTED = 0;

    public static String shopdata;
    public static String favorite;
    public static String productid;
    public static String usrid;
    public static String FRAGMENT_MYCART_CALL="fragment_mycart";
    public static String FRAGMENT_CALL="fragment";

    public static String usertlogindata;
    public static String skip="";
//    public static String loginurl = "http://energiia.co.in/webservices/service.php";

    public static String loginurl = "http://polandstopnshop.com/webservices/service.php";
    public static String device_token="token";
    public static String deviceType="android";
    public static String user_type="admin";
    public static String user_type1="user";

    public static Login_AllData getlogindata(Context activity) {
        Gson gson= new Gson();
        String logindata = SharedPreferenceHelper.getSharedPreferenceString(activity, Constant.usertlogindata, "");
        return gson.fromJson(logindata.toString(), Login_AllData.class);
    }

    public static void saveLogindata(Activity login_activity_login, Login_AllData agent) {

        Gson gson= new Gson();
        String logindata=gson.toJson(agent,Login_AllData.class);
        SharedPreferenceHelper.setSharedPreferenceString(login_activity_login,Constant.usertlogindata,logindata);

    }

    public static void clearLoginData(Activity login_activity_login, Login_AllData agent) {

        Gson gson= new Gson();
        String logindata=gson.toJson(agent,Login_AllData.class);
        SharedPreferenceHelper.setSharedPreferenceString(login_activity_login,Constant.usertlogindata,logindata);

    }

}