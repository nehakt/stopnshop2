package com.stopnshop.user.stopnshop.View;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.Msg;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.stopnshop.user.stopnshop.util.UtilView;
import com.yinglan.keyboard.HideUtil;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class SignUpActivity extends AppCompatActivity implements Validator.ValidationListener,View.OnClickListener{

    @NotEmpty
    @BindView(R.id.ed_firstName)
    EditText edFirstName;
    @NotEmpty
    @BindView(R.id.ed_lastName)
    EditText edLastName;

    @NotEmpty
    @Email
    @BindView(R.id.ed_email)
    EditText edEmail;
    @NotEmpty
    @Password(min = 4, scheme = Password.Scheme.ALPHA_NUMERIC)
    @BindView(R.id.ed_password)
    EditText edPassword;
    @NotEmpty
    @ConfirmPassword
    @BindView(R.id.ed_confirmPassword)
    EditText edConfirmPassword;
    @NotEmpty
    @BindView(R.id.ed_contactNo)
    EditText edContactNo;
    @NotEmpty
    @BindView(R.id.dob_edt)
    EditText dob_edt;
    //@NotEmpty
//    @InjectView(R.id.ed_monthType)
//    EditText edMonthType;
//    @NotEmpty
//    @InjectView(R.id.ed_yearType)
//    EditText edYearType;

    List<String> listItems = new ArrayList<>();
    ArrayAdapter<String> adapter;
    ImageView back_btn;
    TextView save_btn;
   // Spinner spinner;
    String text;
    private AQuery aq;
    private Gson gson;
    private Validator validator;

    private DatePickerDialog fromDatePickerDialog;

    private SimpleDateFormat dateFormatter;

    String name="",lastName="",dob="",email="",password="",cpass="",phone,device_token="";
    private SweetAlertDialog sweetAlertDialogView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);
        HideUtil.init(this);

        intiview();
        validator = new Validator(this);
        validator.setValidationListener(this);

        initItems();
        aq = new AQuery(this);
        gson=new Gson();

        dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        setDateTimeField();
//       // spinner =(MaterialSpinner)findViewById(R.id.spinner);
//
////        adapter =new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,listItems);
////        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
////        spinner.setAdapter(adapter);
////        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
//
//        {
//            @Override
//            public void onItemSelected (AdapterView < ? > parent, View view,int position, long l){
//                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
//                text = spinner.getSelectedItem().toString();
//                Log.e("spinner slecteed",text);
//            }
//            @Override
//            public void onNothingSelected (AdapterView < ? > parent){
//            }
//        });
        save_btn.setOnClickListener(this);
        back_btn.setOnClickListener(this);
        dob_edt.setOnClickListener(this);

        device_token=SharedPreferenceHelper.getSharedPreferenceString(this,Constant.device_token,"");
    }

    private void setDateTimeField() {
        dob_edt.setOnClickListener(this);
        // toDateEtxt.setOnClickListener(this);

        final Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this,  R.style.DialogTheme,new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                if ((newCalendar.before(newDate))){
                    UtilView.showToast(SignUpActivity.this, "Please select a valid date");
                    dob_edt.setText("");
                } else {

                    dob_edt.setText(dateFormatter.format(newDate.getTime()));
                }
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    }


    private void intiview() {

        back_btn = (ImageView) findViewById(R.id.back_btn);
        save_btn = (TextView) findViewById(R.id.save_btn);
    }
    private void initItems() {

        listItems.add("Jaurary");
        listItems.add("Feburay");
        listItems.add("March");
        listItems.add("April");
        listItems.add("May");
        listItems.add("Jun");
        listItems.add("July");
        listItems.add("August");
    }
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.save_btn:

                validator.validate();
                break;

            case R.id.back_btn:
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.dob_edt:
                fromDatePickerDialog.show();

                break;
        }
    }

    @Override
    public void onValidationSucceeded() {

        name =  edFirstName.getText().toString();
        lastName =  edLastName.getText().toString();
        email =  edEmail.getText().toString();
        password =  edPassword.getText().toString();
        dob = dob_edt.getText().toString();
//        year = edYearType.getText().toString();
        phone = edContactNo.getText().toString();
        cpass =edConfirmPassword.getText().toString();

      //  dob = text +"-" +mnth +"-" +year;

        if (InternetConnection.isNetworkAvailable(SignUpActivity.this)){

         //   if( phone.equals(10)) {
               // if (password.equals(cpass)){

            if(TextUtils.isEmpty(phone) || phone.length() < 10) {
                edContactNo.setError("invalid mobile no");
                return;
            }else {
                getSignUp();

            }


//                }else {
//                    edConfirmPassword.setError("confrm password should be same as password");
//                    edConfirmPassword.requestFocus();
//                }
//            }else {
//                //edContactNo.setError("invalid mobile no");
//            }
        }else {

            Msg.t(SignUpActivity.this, "Please Check Internet Connection");
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {

                if (view.equals(edPassword)){

                    ((EditText) view).setError("Please enter alpha numeric");

                }else {

                    ((EditText) view).setError(message);

                }


            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
    private void getSignUp() {

        Data data = new Data();
        data.setDob(dob);
        data.setName(name);
        data.setLastName(lastName);
        data.setEmail(email);
        data.setPassword(password);
        data.setContact(phone);
        data.setDevice_token(device_token);
        data.setDevice_type(Constant.deviceType);

        ServerRequestApi pozo = new ServerRequestApi("userRegistration", data);
        String gsonString = gson.toJson(pozo, ServerRequestApi.class);
        callSignUpQuery(gsonString);
    }

    private void callSignUpQuery(String gsonString) {

        ProgressDialog progressbar = UtilView.showProgressDialog(this);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(aq.POST_ENTITY, sEntity);

        aq.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("signinresult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {
//
//                            SharedPreferenceHelper.setSharedPreferenceString(SignUpActivity.this,"token",Constant.token);
//                            SharedPreferenceHelper.setSharedPreferenceString(SignUpActivity.this,"devicetype",Constant.deviceType);

                            Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();

                        } else {

                            sweetAlertDialogView = new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialogView.setTitleText("Oops...")
                                    .setContentText(" Email Already Registered")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();
                            Button viewGroup = (Button) sweetAlertDialogView.findViewById(R.id.confirm_button);
                            viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.unsetColor));

                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }
}