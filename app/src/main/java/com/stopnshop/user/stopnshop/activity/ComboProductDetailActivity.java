package com.stopnshop.user.stopnshop.activity;

import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.google.gson.Gson;

import com.google.gson.reflect.TypeToken;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.adapter.ComboProductAdapter;
import com.stopnshop.user.stopnshop.model.ComboDeal.ComboDatum;
import com.stopnshop.user.stopnshop.model.ComboDeal.ProductDetail;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.stopnshop.user.stopnshop.util.UtilView;


import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by neha on 6/12/17.
 */
public class ComboProductDetailActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView txtToolbar;
    RecyclerView recyclerComboDetail;
    Button btnAddToCart;
    TextView tvComboPrice,tvQty;

    ComboProductAdapter cpAdapter;

    Cart cart;
    LayerDrawable cartmenuIcon;

    ComboDatum comboProductDatum;
    private ArrayList<ProductDetail> comboProductList;

    @Override
    protected void onResume() {
        super.onResume();
        cart= CartHelper.getCart();
        invalidateOptionsMenu();
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comboprodeatil);

        initView();
        cart= CartHelper.getCart();

        LinearLayoutManager layout_manager=new LinearLayoutManager(this);
        recyclerComboDetail.setLayoutManager(layout_manager);

        comboProductDatum= (ComboDatum) getIntent().getSerializableExtra("comboList");

        if (comboProductDatum.getPrice1()!=null){
            tvComboPrice.setText("Combo Pack Price: $ "+comboProductDatum.getPrice1());
        }

        comboProductList = new ArrayList<ProductDetail>();
        comboProductList = (ArrayList<ProductDetail>) comboProductDatum.getcomboProductDetail();

        cpAdapter=new ComboProductAdapter(ComboProductDetailActivity.this,comboProductList);
        recyclerComboDetail.setAdapter(cpAdapter);

        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String quantity = tvQty.getText().toString();

                //----------cast comboProductDatum to productListData1-------------

                GetProductListData1 productListData1=new GetProductListData1();
                productListData1.setName(comboProductDatum.getName());
                productListData1.setDescription(comboProductDatum.getDescription());
                productListData1.setImage(comboProductDatum.getImage());
                productListData1.setPrice1(comboProductDatum.getPrice1());
                productListData1.setId(comboProductDatum.getComboId());

                BigDecimal price=new BigDecimal(comboProductDatum.getPrice1());
                productListData1.setPrice(price);
                productListData1.setpId1(Integer.parseInt(comboProductDatum.getComboId()));
                cart.add(productListData1, Integer.parseInt(quantity),ComboProductDetailActivity.this);

                storedataToLocal(productListData1);
                //UtilView.setBadgeCount(context, cartmenuIcon, "" + cart.getTotalQuantity());

                SweetAlertDialog sweetAlertDialogView;
                sweetAlertDialogView=  new SweetAlertDialog(ComboProductDetailActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                sweetAlertDialogView.setTitleText(comboProductDatum.getName())
                        .setContentText("Added successfully.")
                        .show();

                Button viewGroup = (Button) sweetAlertDialogView.findViewById(R.id.confirm_button);
                viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.unsetColor));

                invalidateOptionsMenu();
            }
        });
    }

    private void storedataToLocal(GetProductListData1 productDetail) {

        Gson gson = new Gson();

        String data =  SharedPreferenceHelper.getSharedPreferenceString(this,"cartdata","");

        Type type = new TypeToken<List<GetProductListData1>>(){}.getType();

        if(data!=null  && !data.equals("")){

            List<GetProductListData1> detail = gson.fromJson(data,type);

            for(int i=0;i<detail.size();i++){

                if(detail.get(i).getId().equals(productDetail.getId())){

                    int quantity = detail.get(i).getQuantity();

                    detail.remove(productDetail);

                    productDetail.setQuantity(quantity+1);

                }else {

                    productDetail.setQuantity(1);


                }
            }

            detail.add(productDetail);
            String storedata = gson.toJson(detail);

            SharedPreferenceHelper.setSharedPreferenceString(this,"cartdata",storedata);



        }else {

            List<GetProductListData1> list = new ArrayList<>();

            productDetail.setQuantity(1);

            list.add(productDetail);

            String storedata = gson.toJson(list);

            SharedPreferenceHelper.setSharedPreferenceString(this,"cartdata",storedata);
        }
    }

    private void initView() {
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        txtToolbar=(TextView)findViewById(R.id.toolbar_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtToolbar.setText("Combo Deal Products");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recyclerComboDetail=(RecyclerView)findViewById(R.id.recycler_combodetail);
        tvComboPrice=(TextView)findViewById(R.id.tv_comboprice);
        btnAddToCart=(Button)findViewById(R.id.btn_addtocard);
        tvQty=(TextView)findViewById(R.id.tv_qty);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.startinfo_menu, menu);
        //------For Search Functionality-------
        MenuItem item = menu.findItem(R.id.logout);
        MenuItem item1 = menu.findItem(R.id.favorite_list);
        MenuItem item2 = menu.findItem(R.id.notification_bttn);
        MenuItem search = menu.findItem(R.id.search_button);

        item.setVisible(false);
        item1.setVisible(false);
        item2.setVisible(false);
        search.setVisible(false);

        MenuItem itemCart = menu.findItem(R.id.shopping_bttn);
        cartmenuIcon = (LayerDrawable) itemCart.getIcon();
        showBadgeCount();
        //UtilView.setBadgeCount(this, cartmenuIcon, "" + Cart.mapSize);

        return true;
    }

    private void showBadgeCount() {

        Gson gson = new Gson();
        Type type = new TypeToken<List<GetProductListData1>>(){}.getType();

        String data = SharedPreferenceHelper.getSharedPreferenceString(this,"cartdata","");

        List<GetProductListData1> list = gson.fromJson(data,type);

        int quantity = 0;

        if(list!=null && list.size()>0) {

//            for (int i = 0; i < list.size(); i++) {
//
//                quantity = quantity + list.get(i).getQuantity();
//            }

            UtilView.setBadgeCount(this, cartmenuIcon, "" + list.size());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.shopping_bttn) {
            Intent intent = new Intent(ComboProductDetailActivity.this, MyCartActivity.class);
            startActivity(intent);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}