package com.stopnshop.user.stopnshop.model.ComboDeal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by neha on 6/12/17.
 */

public class ProductDetail implements Serializable{

    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("addedDate")
    @Expose
    private String addedDate;

    /**
     * No args constructor for use in serialization
     *
     */
    public ProductDetail() {
    }
    /**
     *
     * @param addedDate
     * @param description
     * @param image
     * @param productName
     */
    public ProductDetail(String productName, String description, String image, String addedDate) {
        super();
        this.productName = productName;
        this.description = description;
        this.image = image;
        this.addedDate = addedDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }
}
