package com.stopnshop.user.stopnshop.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.stopnshop.user.stopnshop.NetworkUtil;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.StoreInfoData;
import com.stopnshop.user.stopnshop.model.logindata_pojo.LoginClassPojo;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.stopnshop.user.stopnshop.util.UtilView;
import com.yinglan.keyboard.HideUtil;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener,View.OnClickListener {

    @BindView(R.id.signup_btn)
    Button btnSignUp;
    @BindView(R.id.signin_btn)
    Button btnSignIn;

    @NotEmpty
    @Email
    @BindView(R.id.email_txt)
    EditText edEmail;
    @NotEmpty
    @BindView(R.id.pass_txt)
    EditText edPass;

    @BindView(R.id.tv_skip)
    TextView tvSkip;
    @BindView(R.id.tv_forgot)
    TextView tvForgot;

    AQuery query;
    Gson gson;

    private List<Login_AllData> logindata;
    private Login_AllData loginid;
    private List<StoreInfoData> shopdata;

    private Validator validator;
    private String email="",password="",device_token="";

    Intent intent;

//   public static String flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        HideUtil.init(this);

        validator = new Validator(this);
        validator.setValidationListener(this);

        query = new AQuery(this);
        gson = new Gson();

        btnSignIn.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        tvForgot.setOnClickListener(this);
        tvSkip.setOnClickListener(this);
    }

    @OnClick({R.id.signin_btn, R.id.signup_btn, R.id.tv_forgot, R.id.tv_skip})
    @Override
    public void onClick(View view) {

        switch(view.getId()){

            case R.id.signin_btn:

                validator.validate();

                break;

            case R.id.signup_btn:

                intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);

                break;

            case R.id.tv_forgot:
                SharedPreferenceHelper.setSharedPreferenceString(LoginActivity.this, Constant.skip,"");

                intent=new Intent(LoginActivity.this,ForgetPasswordActivity.class);
                startActivity(intent);

                break;

            case R.id.tv_skip:
//                flag="skip";
                String flag="skip";
                SharedPreferenceHelper.setSharedPreferenceString(LoginActivity.this,Constant.skip,flag);
                intent = new Intent(LoginActivity.this, MainActivity.class);
//                intent.putExtra("flag",flag);
                startActivity(intent);

                break;
        }
    }

    @Override
    public void onValidationSucceeded() {

        email=edEmail.getText().toString();
        password=edPass.getText().toString();

        if(Constant.INTERNET_NOT_CONNECTED!= NetworkUtil.getConnectionStatus(this)){

            getLogin();
        }else {

            UtilView.showToast(this,"Internet not Connected");
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void getLogin() {

        //----------get device_token from shared prefernce-----------------

        device_token=SharedPreferenceHelper.getSharedPreferenceString(this,Constant.device_token,"");

        Data data = new Data();
        data.setEmail(email);
        data.setPassword(password);
        data.setDevice_token(device_token);
        data.setDevice_type(Constant.deviceType);

        SharedPreferenceHelper.setSharedPreferenceString(LoginActivity.this,Constant.skip,"");
        ServerRequestApi pozo = new ServerRequestApi("customerLogin", data);
        final Gson gson = new Gson();
        String gsonString = gson.toJson(pozo, ServerRequestApi.class);
        Log.e("@flow", gsonString);
        callLoginQuery(gsonString);
    }

    private void callLoginQuery(String gsonString) {

        ProgressDialog progressbar = UtilView.showProgressDialog(this);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);

        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("signinresult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {
                            LoginClassPojo listData=gson.fromJson(json.toString(), LoginClassPojo.class);
                            //EditProfileData listdata=gson.fromJson()
                            logindata=listData.getData();

                            SharedPreferenceHelper.setSharedPreferenceString(LoginActivity.this,"iserid",logindata.get(0).getId());

                            Constant.saveLogindata(LoginActivity.this,logindata.get(0));
                            //  String userid=  json.getString("id");
                            //
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                        } else {

                            UtilView.showErrorDialog(LoginActivity.this,"Email and password does not match");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }
}