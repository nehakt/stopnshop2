package com.stopnshop.user.stopnshop.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.stopnshop.user.stopnshop.R;

import java.util.ArrayList;

/**
 * Created by neha on 21/9/17.
 */

public class FullImageAdapter extends RecyclerView.Adapter<FullImageAdapter.ViewHolder> {

    Activity context;
    ArrayList<String> imagelist;
    int position;

    public FullImageAdapter(Activity activity, ArrayList<String> imagelist, int position) {
        this.context=activity;
        this.imagelist=imagelist;
        this.position=position;
    }
    @Override
    public FullImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_fullimg,parent,false);
        return new FullImageAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FullImageAdapter.ViewHolder holder, int position) {


        Picasso.with(context)
                .load(imagelist.get(position))
                .error(R.drawable.logo)
                .placeholder(R.drawable.logo)
                .into(holder.imgFullImg);
    }

    @Override
    public int getItemCount() {
        return imagelist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imgFullImg;

        public ViewHolder(View itemView) {
            super(itemView);

            imgFullImg=(ImageView)itemView.findViewById(R.id.img_fullImg);
        }
    }
}
