package com.stopnshop.user.stopnshop.model;

/**
 * Created by neha on 23/9/17.
 */

public class CartItem {


    private GetProductListData1 product;
    private int quantity;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public GetProductListData1 getProduct() {
        return product;
    }

    public void setProduct(GetProductListData1 product) {
        this.product = product;
    }
}
