package com.stopnshop.user.stopnshop.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.model.orderList.Datum;
import com.stopnshop.user.stopnshop.model.orderList.ProductDatum;
import com.stopnshop.user.stopnshop.model.productlist_pojo.CartProduct;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.adapter.OrderSummaryAdapter;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by neha on 15/11/17.
 */

public class FavouriteOrderDetail_Activity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtToolbar;

    RecyclerView recyclerView;
    TextView tvOrderNo, tv_orderDate, tvPickupTime, tvTotalItems, tvTotalAmt,tvNotificationText,tvCashPaying;
    Button btnReorder;

    private ArrayList<ProductDatum> productData;
    Datum favDetailData;

    OrderSummaryAdapter mAdapter;
    Activity activity;
    Login_AllData logindata;

    private static final int MAX_LENGTH = 10;
    String randomOrder;
    private SweetAlertDialog sweetAlertDialogView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_orderdetail);

       initview();

       favDetailData= (Datum) getIntent().getSerializableExtra("favorderList");

        if (favDetailData.getOrderId()!=null){
            tvOrderNo.setText("Order #"+favDetailData.getOrderId());
        }
        if (favDetailData.getAddedDate()!=null){
            tv_orderDate.setText(favDetailData.getAddedDate());
        }
        if (favDetailData.getPickuptime()!=null){
            tvPickupTime.setText("Pickup Time: "+favDetailData.getPickuptime());
        }
        if (favDetailData.getTotolItem()!=null){
            tvTotalItems.setText("Total Items: "+favDetailData.getTotolItem());
        }
        if (favDetailData.getAmout()!=null){
            tvTotalAmt.setText("Total Amount: $"+favDetailData.getAmout());
        }
        if (favDetailData.getCash_paying()!=null){
            tvCashPaying.setText("Cash Payed: $"+favDetailData.getCash_paying());
        }

        if (favDetailData.getOrder_status().equals("2")||favDetailData.getOrder_status().equals("3")){
            btnReorder.setVisibility(View.VISIBLE);
        }
        productData=new ArrayList<ProductDatum>();
        productData= (ArrayList<ProductDatum>) favDetailData.getProductData();

        mAdapter = new OrderSummaryAdapter(activity, productData);
        recyclerView.setAdapter(mAdapter);

        btnReorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               showDialog();
            }
        });
    }

    private void initview() {
        LinearLayoutManager layoutManager=new LinearLayoutManager(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_orderSummery);
        recyclerView.setLayoutManager(layoutManager);

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        txtToolbar=(TextView)findViewById(R.id.toolbar_text);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtToolbar.setText("Favourite Order Detail");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tvOrderNo = (TextView) findViewById(R.id.tv_orderNo);
        tv_orderDate = (TextView) findViewById(R.id.tv_orderDate);
        tvPickupTime = (TextView) findViewById(R.id.tv_pickupDate);
        tvTotalItems = (TextView) findViewById(R.id.tv_totalItems);
        tvTotalAmt = (TextView) findViewById(R.id.tv_totalAmount);
        tvNotificationText=(TextView)findViewById(R.id.notification_txt);
        tvCashPaying=(TextView)findViewById(R.id.tv_cashPaying);
        btnReorder=(Button)findViewById(R.id.btn_reOrder);
    }

    public void getReorder(String randomOrder){

//        List<ProductDatum> productdata = favDetailData.getProductData();
        List<CartProduct> cartlist = new ArrayList<>();

        for(int i=0;i<productData.size();i++){
            CartProduct product = new CartProduct();

            product.setProductName(productData.get(i).getProductName());
            product.setProduct_price(productData.get(i).getProductPrice());
            product.setProduct_quantity(productData.get(i).getTotalItems());
            product.setProductImage(productData.get(i).getProductImage());
            product.setProduct_id(productData.get(i).getProduct_id());
            cartlist.add(product);
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();
                    String pickuptime = dateFormat.format(date);

        Data data=new Data();
        logindata = Constant.getlogindata(this);
        data.setUserId(logindata.getId());
        data.setTotal_item(favDetailData.getTotolItem());
        data.setTotal_amount(favDetailData.getAmout());
        data.setProduct_list(cartlist);
        data.setName(favDetailData.getCustomerName());
        data.setContactno(favDetailData.getContact());
        data.setPickup_time(pickuptime);
        data.setCash_paying(favDetailData.getCash_paying());
        data.setOrder_number(randomOrder);

        ServerRequestApi server_request=new ServerRequestApi("place_order",data);
        Gson gson=new Gson();
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        UtilView.showLogCat("reorderReq",gsonString);
        ProgressDialog progressbar = UtilView.showProgressDialog(this);
        AQuery query=new AQuery(this);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);
        //Intent intent=new Intent(context, OrderSummryActivity.class);
//     startActivity(intent);
        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
        super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("orderListResult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            sweetAlertDialogView = new SweetAlertDialog(FavouriteOrderDetail_Activity.this, SweetAlertDialog.SUCCESS_TYPE);
                            sweetAlertDialogView.setTitleText("Success")
                                    .setContentText(" Your Order Is Placed Successfully.")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            finish();
                                        }
                                    })
                                    .show();
                            Button viewGroup = (Button) sweetAlertDialogView.findViewById(R.id.confirm_button);
                            viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.unsetColor));

                        } else {

                            Log.e("message", json.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;
        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    public void showDialog(){
                    AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("ReOrder?"); // set title
            dialog.setMessage("Are you sure you want to reorder"); // set message
            dialog.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            randomOrder=random();
                            getReorder(randomOrder);
                        }
                    });
            dialog.setNegativeButton("No",
                    new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            // dialog.create().show();
            AlertDialog alert = dialog.create();
            alert.show();
            Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(nbutton.getResources().getColor(R.color.colorBlack));
            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(nbutton.getResources().getColor(R.color.colorBlack));
     }
 }