package com.stopnshop.user.stopnshop;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.stopnshop.user.stopnshop.util.Constant;

/**
 * Created by Prateek on 22/12/16.
 */

public class NetworkUtil {


    public static final int WIFI = 1;
    public static final int MOBILE = 2;

    public static int getConnectionStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return WIFI;
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return MOBILE;
        }
        return Constant.INTERNET_NOT_CONNECTED;
    }

    public static String getConnectionStatusString (Context context) {
        int connectionStatus = NetworkUtil.getConnectionStatus(context);
        if (connectionStatus == NetworkUtil.WIFI)
            return "Connected to Wifi";
        if (connectionStatus == NetworkUtil.MOBILE)
            return "Connected to Mobile Data";
        return "No internet connection";
    }


}
