package com.stopnshop.user.stopnshop.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.Msg;
import com.yinglan.keyboard.HideUtil;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ForgetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    Button submit_btn;
    EditText email_txt;
    private String email;
    private AQuery aq;
    ImageView back_btn,imag;
    TextView title_txt,save_txt;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        HideUtil.init(this);
        initview();
    }

    private void initview() {

        submit_btn=(Button)findViewById(R.id.submit_btn);
        email_txt=(EditText)findViewById(R.id.email_txt);
        save_txt=(TextView) findViewById(R.id.save_txt);

        save_txt.setVisibility(View.GONE);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        title_txt = (TextView)findViewById(R.id.title_txt);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title_txt.setText("Forgot Password");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

      //  back_btn = (ImageView)findViewById(R.id.back_btn);
//        title_txt = (TextView)findViewById(R.id.title_txt);
//        title_txt.setText("Forgot Password");
      //  back_btn.setOnClickListener(this);
        submit_btn.setOnClickListener(this);
    }

    private void forgetpassword() {

        aq = new AQuery(this);
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        Map<String, Object> params = null;
        StringEntity sEntity = null;

        Data data=new Data();
        data.setEmail(email);
        ServerRequestApi pozo = new ServerRequestApi("forgot_password", data);
        final Gson gson = new Gson();
        String s = gson.toJson(pozo, ServerRequestApi.class);
        Log.e("@flow", s);

        try {
            sEntity = new StringEntity(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(aq.POST_ENTITY, sEntity);

        aq.progress(progressDialog).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                Log.e("@flow", json.toString());

                if (status.getCode() == 200) {

                    Log.e("@flow", json.toString());

                    try {

                        if (json.getInt("msg_code") == 1) {
                            Intent intent = new Intent(ForgetPasswordActivity.this, ResetPassword_Activity.class);
                            intent.putExtra("email",email);
                            startActivity(intent);
                        } else {

                            try {

                                Log.e("message", json.getString("message"));

                                if (json.has("message")  ){

                                    String s1= json.getString("message");
                                    Toast.makeText(ForgetPasswordActivity.this,s1,Toast.LENGTH_SHORT).show();

                                }


                            }catch (Exception e){

                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    private boolean edittextvalidation() {
        boolean flag = true;

        if (email_txt.getText().toString().length() == 0) {
            email_txt.setError("This field is required");
            email_txt.requestFocus();
            return false;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email_txt.getText().toString()).matches()) {
            email_txt.setError("Invalid Email id please enter valid email Id");

            flag=false;
        }

        return flag;
    }

    public boolean validateEmail(String email) {

        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){


            case R.id.submit_btn:
                if(edittextvalidation()){
                    email=  email_txt.getText().toString().trim();

                    if (InternetConnection.isNetworkAvailable(ForgetPasswordActivity.this)) {

                        forgetpassword();

                    } else {

                        Msg.t(ForgetPasswordActivity.this, "Please Check Internet Connection");

                    }

                    /// SignUp(data);
                }
                break;

        }
    }
}
