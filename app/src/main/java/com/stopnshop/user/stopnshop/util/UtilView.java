package com.stopnshop.user.stopnshop.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.widget.Toast;

import com.stopnshop.user.stopnshop.customeview.BadgeDrawable;
import com.stopnshop.user.stopnshop.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by neha on 14/9/17.
 */

public class UtilView {

    public static ProgressDialog showProgressDialog(Activity context){

        ProgressDialog progressDialog= new ProgressDialog(context);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");

        return progressDialog;
    }

    public static void showToast(Activity context,String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showLogCat(String tag,String message){
        Log.e("@Flow "+tag+": ",message);
    }

    public static void showErrorDialog(Activity activity, String error_msg){

        SweetAlertDialog sweetAlertDialog;
        sweetAlertDialog= new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText("Oops...")
                .setContentText(error_msg)
                .show();
//        Button viewGroup = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
//        viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.colorGreen));
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }



//    public static ArrayAdapter<String> setupAdapter(Activity activity, Spinner spinner, String[] stringArray, String spinner_title) {
//
//        String[] adapter_array = new String[0];
//        if(stringArray.length>0){
//            adapter_array=new String[stringArray.length+1];
//            adapter_array[0]=spinner_title;
//            for(int i=0;i<stringArray.length;i++){
//                adapter_array[i+1]=stringArray[i];
//            }
//        }
//        if(stringArray.length<=0){
//            adapter_array=new String[1];
//            adapter_array[0]=spinner_title;
//        }
//        // Initializing an ArrayAdapter
//        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(activity, R.layout.spinner_textitem,adapter_array){
//            @Override
//            public boolean isEnabled(int position){
//                if(position == 0)
//                {
//                    // Disable the first item from Spinner
//                    // First item will be use for hint
//                    return false;
//                }
//                else
//                {
//                    return true;
//                }
//            }
//            @Override
//            public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                View view = super.getDropDownView(position, convertView, parent);
//
//                TextView tv = (TextView) view.findViewById(R.id.textView1);
//                if(position == 0){
//                    // Set the hint text color gray
//                    tv.setTextColor(Color.GRAY);
//                }
//                else {
//                    tv.setTextColor(Color.BLACK);
//                }
//                return view;
//            }
//        };
//        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_textitem);
//        spinner.setAdapter(spinnerArrayAdapter);
//
//        return spinnerArrayAdapter;
//    }
//


}
