package com.stopnshop.user.stopnshop.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by my lenovo on 12/19/2016.
 */
public class Msg {

    public static void l(String tag,String msg){

        Log.e(tag,msg);


    }

    public static void t(Context context, String msg){

        Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
    }
}
