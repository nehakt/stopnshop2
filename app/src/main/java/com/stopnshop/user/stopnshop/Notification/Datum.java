
package com.stopnshop.user.stopnshop.Notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("addedDate")
    @Expose
    private String addedDate;
    @SerializedName("notification")
    @Expose
    private String notification;
    @SerializedName("notification_type")
    @Expose
    private String notification_type;



    /**
     * No args constructor for use in serialization
     * 
     */
    public Datum() {
    }

    /**
     * 
     * @param addedDate
     * @param notification
     */
    public Datum(String addedDate, String notification) {
        super();
        this.addedDate = addedDate;
        this.notification = notification;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(String notification_type) {
        this.notification_type = notification_type;
    }
}
