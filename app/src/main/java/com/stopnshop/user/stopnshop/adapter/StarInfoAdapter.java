package com.stopnshop.user.stopnshop.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.stopnshop.user.stopnshop.listener.OnItemTouchListener;
import com.stopnshop.user.stopnshop.model.StoreInfoData;
import com.stopnshop.user.stopnshop.R;

import java.util.List;

/**
 * Created by saurabh on 21-Aug-17.
 */

public class StarInfoAdapter extends RecyclerView.Adapter<StarInfoAdapter.MyViewHolder>

{
    List<StoreInfoData> data;
    Activity activity;
    List<String> shopimage;
    OnItemTouchListener onItemTouchListener;

    public StarInfoAdapter(Activity activity, List<String> shopimage, OnItemTouchListener itemTouchListener) {
        this.activity=activity;
        this.shopimage=shopimage;
        this.onItemTouchListener=itemTouchListener;
    }
    @Override
    public StarInfoAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.storeinfo_list_row, parent, false);

        return new StarInfoAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StarInfoAdapter.MyViewHolder holder, final int position) {
       // StoreInfoData data1 = data.get(position);
//            holder.restaurant_txt.setText( data1.getName());
//            holder.name_txt1.setText( data1.getAddress());
//            holder.mobilenum1.setText( data1.getConatact());
//            holder.email_txt.setText( data1.getEmail());

            Picasso.with(activity)
                    .load(shopimage.get(position))
                    .error(R.drawable.logo)
                    .placeholder(R.drawable.logo)
                    .into(holder.images1);
//        holder.restaurant_txt.setText("Shri Balaji Restaurant");
//        holder.name_txt1.setText("Yaswant plaza Indore ");
//        holder.name_txt2.setText("Station Road Near regal");
//        holder.mobilenum1.setText("9688645986");
//        holder.mobilenum2.setText("9685649756");
//        holder.email_txt.setText("sourabhjain@gmail.com");
//        holder.images1.setImageResource((Integer) imgList.get(position));
//        holder.images2.setImageResource((Integer) imgList.get(position));
//        holder.images3.setImageResource((Integer) imgList.get(position));


        holder.images1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemTouchListener.onCardViewTap(view,position);
            }
        });

    }
    @Override
    public int getItemCount()

    {
        return shopimage.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView images1,images2,images3;

        public MyViewHolder(View view) {
            super(view);

            images1 = (ImageView) view.findViewById(R.id.images1);
//            images2 = (ImageView) view.findViewById(R.id.images2);
//            images3 = (ImageView) view.findViewById(R.id.images3);
        }
    }
}