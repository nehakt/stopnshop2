package com.stopnshop.user.stopnshop.model.ComboDeal;


import com.android.tonyvu.sc.model.Saleable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by neha on 6/12/17.
 */

public class ComboDatum implements Serializable, Saleable {

    @SerializedName("id")
    @Expose
    private String comboId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private String price1;
    @SerializedName("combodetail")
    @Expose
    private List<ProductDetail> comboProductDetail = null;

    BigDecimal pPrice;
    int pId1;

    /**
     * No args constructor for use in serialization
     *
     */
    public ComboDatum() {
    }
    /**
     * @param price
     * @param description
     * @param name
     * @param image
     * @param combodetail
     */
    public ComboDatum(String name, String image, String description, String price, List<ProductDetail> combodetail) {
        super();
        this.name = name;
        this.image = image;
        this.description = description;
        this.price1 = price;
        this.comboProductDetail = combodetail;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice1() {
        return price1;
    }
    public void setPrice(String price) {
        this.price1 = price;
    }

    public List<ProductDetail> getcomboProductDetail() {
        return comboProductDetail;
    }

    public void setCombodetail(List<ProductDetail> comboProductDetail) {
        this.comboProductDetail = comboProductDetail;
    }

    public String getComboId() {
        return comboId;
    }

    public void setComboId(String comboId) {
        this.comboId = comboId;
    }

    public BigDecimal getPrice() {
        return pPrice;
    }

    public void setPrice(BigDecimal pPrice) {
        this.pPrice = pPrice;
    }
    public int getpId1() {
        return pId1;
    }

    public void setpId1(int pId1) {
        this.pId1 = pId1;
    }
}
