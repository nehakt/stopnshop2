
package com.stopnshop.user.stopnshop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RandomOrder {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("msg_code")
    @Expose
    private int msgCode;
    @SerializedName("pickup_time")
    @Expose
    private String pickupTime;
    @SerializedName("orderId")
    @Expose
    private String orderId;

    /**
     * No args constructor for use in serialization
     * 
     */
    public RandomOrder() {
    }

    /**
     * 
     * @param message
     * @param pickupTime
     * @param msgCode
     */
    public RandomOrder(String message, int msgCode, String pickupTime) {
        super();
        this.message = message;
        this.msgCode = msgCode;
        this.pickupTime = pickupTime;

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(int msgCode) {
        this.msgCode = msgCode;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
