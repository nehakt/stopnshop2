package com.stopnshop.user.stopnshop;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;

/**
 * Created by neha on 6/11/17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedTokenUser = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedTokenUser);

        //----------set device_token from shared prefernce-------------

        SharedPreferenceHelper.setSharedPreferenceString(this, Constant.device_token,refreshedTokenUser);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedTokenUser);
    }
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
    }
    
}
