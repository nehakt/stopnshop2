package com.stopnshop.user.stopnshop.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.stopnshop.user.stopnshop.View.MainActivity;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class PlaceOrderActivity extends AppCompatActivity implements View.OnClickListener {

    private  Toolbar toolbar;
    private TextView txtToolbar;
    TextView continueshopping_btn, myorder_btn,tvOutside,tvOrderNo,tvPickupTime;
    String pickupTime;
    String orderNo;
    private Login_AllData logindata;
    private String userid="",userName="";
    private SweetAlertDialog sweetAlertDialogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);

        pickupTime=getIntent().getStringExtra("pickuptime");
        orderNo=getIntent().getStringExtra("orderNo");

        initview();
    }

    private void initview() {

        txtToolbar = (TextView)findViewById(R.id.toolbar_text);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtToolbar.setText("Thank You!");
        continueshopping_btn = (TextView) findViewById(R.id.continueshopping_btn);
        myorder_btn = (TextView) findViewById(R.id.myorder_btn);
        tvOutside=(TextView)findViewById(R.id.tv_outside);
        tvOrderNo=(TextView)findViewById(R.id.tv_orderNo);
        tvPickupTime=(TextView)findViewById(R.id.tv_pickup);

        tvOrderNo.setText("OrderId: "+orderNo);
        tvPickupTime.setText("Pickup Time: "+pickupTime);

        continueshopping_btn.setOnClickListener(this);
        myorder_btn.setOnClickListener(this);
        tvOutside.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent intent;

        switch (v.getId()){

            case R.id.continueshopping_btn:
                String flag="skip";
                 intent = new Intent(PlaceOrderActivity.this, MainActivity.class);
                 //intent.putExtra("flag",flag);
                 startActivity(intent);

                break;

            case R.id.myorder_btn:
                String placeOrderData = "0";
                intent = new Intent(PlaceOrderActivity.this, MainActivity.class);
                //intent.putExtra("flag",flag);
                intent.putExtra("placeorderActivity", placeOrderData);
                startActivity(intent);

                break;

            case R.id.tv_outside:

                getOutside();
        }
    }

    public void getOutside(){

        logindata= Constant.getlogindata(this);

        if (logindata!=null){

            userid= logindata.getId();
            userName=logindata.getName();
        }
        Data data=new Data();
        data.setUserId(userid);
        data.setUserName(userName);
        data.setOrderId("");
        data.setUser_type(Constant.user_type);
        data.setNotification_type("outside");
        data.setNotification(userName+" is outside the store to pick their order and it is order_id is "+orderNo);

        ServerRequestApi server_request=new ServerRequestApi("storeOutsiteMessage",data);
        Gson gson=new Gson();
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        UtilView.showLogCat("outsideReq",gsonString);

        ProgressDialog progressbar = UtilView.showProgressDialog(this);
        AQuery query=new AQuery(this);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);
        //Intent intent=new Intent(context, OrderSummryActivity.class);
//                        startActivity(intent);
        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("orderListResult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            sweetAlertDialogView = new SweetAlertDialog(PlaceOrderActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                            sweetAlertDialogView.setTitleText("Success")
                                    .setContentText(" Your request is Send Successfully")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {

//                                      tvOutside.setVisibility(View.GONE);
                                           sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();
                            Button viewGroup = (Button) sweetAlertDialogView.findViewById(R.id.confirm_button);
                            viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.unsetColor));

                        } else
                        {
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    //----when click on back btn of mb go to the main activity------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Intent intent=new Intent(this,MainActivity.class);
            startActivity(intent);
        }
        return super.onKeyDown(keyCode, event);
    }
}