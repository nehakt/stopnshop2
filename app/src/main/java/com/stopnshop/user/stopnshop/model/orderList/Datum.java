
package com.stopnshop.user.stopnshop.model.orderList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Datum implements Serializable {

    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("favorite")
    @Expose
    private String favorite;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("order_number")
    @Expose
    private String orderNumber;
    @SerializedName("contact")
    @Expose
    private String contact;
//    @SerializedName("add_date")
//    @Expose
//    private String addDate;
    @SerializedName("amout")
    @Expose
    private String amout;
    @SerializedName("totol_item")
    @Expose
    private String totolItem;
    @SerializedName("pickuptime")
    @Expose
    private String pickuptime;
    @SerializedName("addedDate")
    @Expose
    private String addedDate;
    @SerializedName("cash_paying")
    @Expose
    private String cash_paying;
    @SerializedName("order_status")
    @Expose
    private String order_status;

    @SerializedName("productData")
    @Expose
    private List<ProductDatum> productData = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Datum() {
    }
    /**
     * 
     * @param customerName
     * @param addedDate
     * @param paymentType
     * @param pickuptime
     * @param orderNumber
     * @param productData
     * @param addDate
     * @param totolItem
     * @param amout
     * @param favorite
     * @param contact
     * @param orderId
     */
    public Datum(String orderId, String favorite, String customerName, String orderNumber, String contact, String addDate, String amout, String totolItem, String pickuptime, String addedDate, String paymentType, List<ProductDatum> productData) {
        super();
        this.orderId = orderId;
        this.favorite = favorite;
        this.customerName = customerName;
        this.orderNumber = orderNumber;
        this.contact = contact;
//        this.addDate = addDate;
        this.amout = amout;
        this.totolItem = totolItem;
        this.pickuptime = pickuptime;
        this.addedDate = addedDate;
        this.cash_paying = paymentType;
        this.productData = productData;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

//    public String getAddDate() {
//        return addDate;
//    }
//
//    public void setAddDate(String addDate) {
//        this.addDate = addDate;
//    }

    public String getAmout() {
        return amout;
    }

    public void setAmout(String amout) {
        this.amout = amout;
    }

    public String getTotolItem() {
        return totolItem;
    }

    public void setTotolItem(String totolItem) {
        this.totolItem = totolItem;
    }

    public String getPickuptime() {
        return pickuptime;
    }

    public void setPickuptime(String pickuptime) {
        this.pickuptime = pickuptime;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getCash_paying() {
        return cash_paying;
    }

    public void setCash_paying(String cash_paying) {
        this.cash_paying = cash_paying;
    }

    public List<ProductDatum> getProductData() {
        return productData;
    }

    public void setProductData(List<ProductDatum> productData) {
        this.productData = productData;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }
}
