package com.stopnshop.user.stopnshop.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.stopnshop.user.stopnshop.model.RandomOrder;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.model.productlist_pojo.CartProduct;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.Msg;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.stopnshop.user.stopnshop.util.UtilView;
import com.yinglan.keyboard.HideUtil;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PayatStoreActivity extends AppCompatActivity implements View.OnClickListener,Validator.ValidationListener{

    private static final int MAX_LENGTH = 10;

    private Toolbar toolbar;
    private TextView txtToolbar,paymeny_txt;
    @NotEmpty
    @BindView(R.id.paymeny_txt)
    EditText edcostPaying;
    TextView placeorder_btn,total_amount;
    private String totalItem;
    private String totalAmount;
    private ArrayList<CartProduct> cartproductList;
    private String name;
    private String contact_no;
    private String pickup_time;
    public static String order_no;
    private AQuery aq;
    Login_AllData logindata;
    String randomOrder;

    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payat_store2);
        ButterKnife.bind(this);
        HideUtil.init(this);


        validator = new Validator(this);
        validator.setValidationListener(this);

        Intent  in = getIntent();
        if(in!=null){

            totalItem = in.getStringExtra("totalItems");
            totalAmount = in.getStringExtra("totalAmount");
            cartproductList = (ArrayList<CartProduct>) in.getSerializableExtra("product_list");
            name = in.getStringExtra("name");
            contact_no = in.getStringExtra("contact_no");
            pickup_time = in.getStringExtra("pickuptime");
        }

        initview();
    }

    private void initview() {

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        txtToolbar = (TextView)findViewById(R.id.toolbar_text);
        paymeny_txt = (TextView)findViewById(R.id.paymeny_txt);
        total_amount = (TextView)findViewById(R.id.total_amount);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtToolbar.setText("Pay At Store");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

           onBackPressed();
            }
        });
        placeorder_btn = (TextView)findViewById(R.id.placeorder_btn);
        placeorder_btn.setOnClickListener(this);

        final Cart cart = CartHelper.getCart();
        total_amount.setText("$ "+totalAmount);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.placeorder_btn:

                validator.validate();

                break;
        }
    }

    private void placeorderrequest(String randomOrder) {

        aq = new AQuery(this);
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        Map<String, Object> params = null;
        StringEntity sEntity = null;

        logindata = Constant.getlogindata(this);
        Data data=new Data();
        data.setUserId(logindata.getId());
        data.setTotal_item(totalItem);
        data.setTotal_amount(totalAmount);
        data.setProduct_list(cartproductList);
        data.setName(name);
        data.setContactno(contact_no);
        data.setPickup_time(pickup_time);
        data.setCash_paying(paymeny_txt.getText().toString());
        data.setOrder_number(randomOrder);
       //---- 0=for pay at store
       //---- 1 for online payment
        data.setPaymentType("0");

        ServerRequestApi pozo = new ServerRequestApi("place_order", data);
        final Gson gson = new Gson();
        String s = gson.toJson(pozo, ServerRequestApi.class);
        UtilView.showLogCat("payatstore",s);

        try {
            sEntity = new StringEntity(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(aq.POST_ENTITY, sEntity);

        aq.progress(progressDialog).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                Log.e("@flow", json.toString());

                if (status.getCode() == 200) {

                    Log.e("@flow", json.toString());

                    try {

                        if (json.getInt("msg_code") == 1) {

                            CartHelper.getCart().clear();

                            SharedPreferenceHelper.setSharedPreferenceString(PayatStoreActivity.this,"cartdata","");

                            Gson gson = new Gson();
                            RandomOrder data = gson.fromJson(json.toString(),  RandomOrder.class);

                            order_no=data.getOrderId();
                            String pickup=data.getPickupTime();

                            Intent intent = new Intent(PayatStoreActivity.this, PlaceOrderActivity.class);
                            intent.putExtra("pickuptime",pickup);
                            intent.putExtra("orderNo",order_no);
                            startActivity(intent);

                        } else {

                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }
    // CREATING METHOD FOR RANDOM STRING VALUE
    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;
        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    @Override
    public void onValidationSucceeded() {

        if (InternetConnection.isNetworkAvailable(this)){

            randomOrder=random();

            UtilView.showLogCat("Rndomorder",randomOrder);
            placeorderrequest(randomOrder);
        }else {

            Msg.t(this, "Please Check Internet Connection");
        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }
}