package com.stopnshop.user.stopnshop.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.model.favoritelis_pojo.Datum;

import java.util.List;

/**
 * Created by shailendra on 15/9/17.
 */

public class FavoriteList_Adapter extends RecyclerView.Adapter<FavoriteList_Adapter.MyViewHolder> {

    Activity activity;
    List<Datum> favorite_data;


    public FavoriteList_Adapter(Activity activity, List<Datum> favorite_data) {
        this.activity=activity;
        this.favorite_data=favorite_data;

    }

    @Override
    public FavoriteList_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favoritelist_item, parent, false);

        return new FavoriteList_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FavoriteList_Adapter.MyViewHolder holder, int position) {

        holder.product_name.setText(favorite_data.get(position).getName());

        holder.product_decription.setText(favorite_data.get(position).getDescription());
        holder.product_price.setText("$"+favorite_data.get(position).getPrice());

        Glide.with(activity).load(favorite_data.get(position).getImage()).placeholder(R.drawable.icon1).error(R.drawable.iimg_not_avlbl).into(holder.product_image);

    }

    @Override
    public int getItemCount() {
        return favorite_data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView product_decription,product_name,product_price;
        public ImageView product_image;

        public MyViewHolder(View view) {
            super(view);

            product_name=(TextView)itemView.findViewById(R.id.product_name);
            product_decription=(TextView)itemView.findViewById(R.id.product_decription);
            product_price=(TextView)itemView.findViewById(R.id.product_price);
            product_image=(ImageView)itemView.findViewById(R.id.product_image);

        }
    }
}

