package com.stopnshop.user.stopnshop.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.model.productlist_pojo.CartProduct;
import com.stopnshop.user.stopnshop.util.Constant;
import com.yinglan.keyboard.HideUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CheckOutActivity extends AppCompatActivity implements View.OnClickListener,Validator.ValidationListener {

    private Toolbar toolbar;
    private TextView txtToolbar;
    TextView continue_btn;
    @NotEmpty
    @BindView(R.id.name_txt)
    EditText name_txt;
    @NotEmpty
    @BindView(R.id.mobile_txt)
    EditText mobile_txt;
//    @InjectView(R.id.tv_spinner)
//     TextView tvSpinner;
//    @NotEmpty
//    @InjectView(R.id.ed_pickupTime)
//    EditText edPickupTime;
    Spinner timeSpiner;
    Login_AllData logindata;
    private String totalItem;
    private String totalAmount;
    ArrayList<CartProduct> cartproductList;

//    private DatePickerDialog fromDatePickerDialog;
//    private SimpleDateFormat dateFormatter;
    private Validator validator;

    Calendar date;

    String pickupTime="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        initview();
        ButterKnife.bind(this);

        validator = new Validator(this);
        validator.setValidationListener(this);

        Intent in = getIntent();

        if (in != null) {

            totalItem = in.getStringExtra("totalItems");
            totalAmount = in.getStringExtra("totalAmount");
            cartproductList = (ArrayList<CartProduct>) in.getSerializableExtra("product_list");
        }

        logindata = Constant.getlogindata(this);

        name_txt.setText(logindata.getName());
        mobile_txt.setText(logindata.getContact());

        HideUtil.init(this);

        final List<String> list = new ArrayList<String>();

        list.add("Select time...");
        list.add("5 Min");
        list.add("10 Min");
        list.add("15 Min");
        list.add("30 Min");
        list.add("1 Hour");
        list.add("2 Hour");
        list.add("3 Hour");
        list.add("5 Hour");
        final ArrayAdapter<String> statusAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, list);

        statusAdapter.setDropDownViewResource(R.layout.spinner_item);
        timeSpiner.postInvalidate();
        timeSpiner.setAdapter(statusAdapter);
        timeSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTextColor(Color.WHITE); //Change selected text color
               }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

      //        dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        //setDateTimeField();
    }

    private void initview() {

        continue_btn = (TextView) findViewById(R.id.continue_btn);
        name_txt = (EditText) findViewById(R.id.name_txt);
        mobile_txt = (EditText) findViewById(R.id.mobile_txt);
        timeSpiner=(Spinner)findViewById(R.id.time_spinner);
//        edPickupTime = (EditText) findViewById(R.id.ed_pickupTime);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtToolbar = (TextView) findViewById(R.id.toolbar_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtToolbar.setText("Customer Info");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        continue_btn.setOnClickListener(this);
//        edPickupTime.setOnClickListener(this);
    }

 //        private boolean edittextvalidation() {
//        boolean flag = true;
//
//        if (name_txt.getText().toString().length() == 0) {
//            name_txt.setError("Name Is Required");
//            name_txt.requestFocus();
//            return false;
//        }
//
//        if(mobile_txt.getText().toString().length()==0){
//            mobile_txt.setError("Mobile Num Is Required");
//            mobile_txt.requestFocus();
//            return false;
//        }
//        if (edPickupTime.getText().toString().length()==0){
//            edPickupTime.setError("Pickup Time Is Required");
//            edPickupTime.requestFocus();
//            return false;
//        }
//        return flag;
//    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.continue_btn:
//                edittextvalidation();
                validator.validate();
//                if (edittextvalidation()) {

//                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//                    Date date = new Date();
//                    String pickuptime = dateFormat.format(date);

                // Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(CheckOutActivity.this, PaymentOptionsActivity.class);
//                    intent.putExtra("totalItems",totalItem);
//                    intent.putExtra("totalAmount",totalAmount);
//                    intent.putExtra("product_list",cartproductList);
//                    intent.putExtra("name",name_txt.getText().toString());
//                    intent.putExtra("contact_no",mobile_txt.getText().toString());
//                    intent.putExtra("pickuptime",edPickupTime.getText().toString());
//                    startActivity(intent);
//                    finish();
                break;

//               }
//            case R.id.ed_pickupTime:

//                fromDatePickerDialog.show();
//                showDateTimePicker();

//                break;
        }
    }

//    public void showDateTimePicker() {
//        final Calendar currentDate = Calendar.getInstance();
//        date = Calendar.getInstance();
//
//            new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
//                @Override
//                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                    date.set(year, monthOfYear, dayOfMonth);
//                    new TimePickerDialog(CheckOutActivity.this, new TimePickerDialog.OnTimeSetListener() {
//                        @Override
//                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                            date.set(Calendar.HOUR_OF_DAY, hourOfDay);
//                            date.set(Calendar.MINUTE, minute);
////                        Log.v(TAG, "The choosen one " + date.getTime());
//                            if ((currentDate.after(date) || (currentDate.equals(date)))){
//                                UtilView.showToast(CheckOutActivity.this, "Please select a valid date");
//                                edPickupTime.setText("");
//                            } else {
//                                edPickupTime.setText(dateFormatter.format(date.getTime()));
//                            }
//                        }
//                    }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
//                }
//            }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
//
//    }

//    private void setDateTimeField() {
//
//        edPickupTime.setOnClickListener(this);
////         toDateEtxt.setOnClickListener(this);
//
//
//
//        Calendar newCalendar = Calendar.getInstance();
//        fromDatePickerDialog = new DatePickerDialog(this,  R.style.DialogTheme,new DatePickerDialog.OnDateSetListener() {
//
//            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                Calendar newDate = Calendar.getInstance();
//                newDate.set(year, monthOfYear, dayOfMonth);
//
//                edPickupTime.setText(dateFormatter.format(newDate.getTime()));
//            }
//
//        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
//
//    }

    @Override
    public void onValidationSucceeded() {
        if (timeSpiner.getSelectedItem().toString().trim().equals("Select time...")) {
            Toast.makeText(this, "Please select time", Toast.LENGTH_SHORT).show();
        } else {

            pickupTime=timeSpiner.getSelectedItem().toString();
            Intent intent = new Intent(CheckOutActivity.this, PaymentOptionsActivity.class);
            intent.putExtra("totalItems", totalItem);
            intent.putExtra("totalAmount", totalAmount);
            intent.putExtra("product_list", cartproductList);
            intent.putExtra("name", name_txt.getText().toString());
            intent.putExtra("contact_no", mobile_txt.getText().toString());
            intent.putExtra("pickuptime", pickupTime);
//        intent.putExtra("pickuptime",edPickupTime.getText().toString());
            startActivity(intent);
//        finish();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
