package com.stopnshop.user.stopnshop.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stopnshop.user.stopnshop.model.productlist_pojo.Datum;
import com.stopnshop.user.stopnshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by neha on 14/11/17.
 */

public class NewProductDetail_Activity extends AppCompatActivity {

    @BindView(R.id.tv_productName)
    TextView tvProductName;
    @BindView(R.id.tv_productDetail)
    TextView tvProductDetail;
    @BindView(R.id.tv_userName)
    TextView tvUserName;
    @BindView(R.id.tv_userContactNo)
    TextView tvUserContactNo;
    @BindView(R.id.img_product)
    ImageView imgProduct;

    Toolbar toolbar;
    TextView txtToolbar;
    Datum productData;
    Activity activity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newproductdetail);

        ButterKnife.bind(this);

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        txtToolbar=(TextView)findViewById(R.id.toolbar_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtToolbar.setText("New Product Detail");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        productData= (Datum) getIntent().getSerializableExtra("productlist");

        if (productData.getProductName()!=null){
            tvProductName.setText(productData.getProductName());
        }
        if (productData.getProductDetails()!=null){
            tvProductDetail.setText(productData.getProductDetails());
        }
        if (productData.getProductImage()!=null){

            Picasso .with(activity)
                    .load(productData.getProductImage())
                    .error(R.drawable.icon1)
                    .placeholder(R.drawable.icon1)
                    .into(imgProduct);
        }
        if (productData.getName()!=null){
            tvUserName.setText("Customer Name: "+productData.getName());
        }
        if (productData.getContact()!=null){
            tvUserContactNo.setText("Contact No: "+productData.getContact());
        }
    }
}
