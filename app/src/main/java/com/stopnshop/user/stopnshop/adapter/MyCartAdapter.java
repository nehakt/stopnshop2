package com.stopnshop.user.stopnshop.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tonyvu.sc.model.Cart;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.stopnshop.user.stopnshop.activity.MyCartActivity;
import com.stopnshop.user.stopnshop.model.CartItem;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

/**
 * Created by saurabh on 23-Aug-17.
 */

public class MyCartAdapter extends RecyclerView.Adapter<MyCartAdapter.MyViewHolder>
{
    private List<CartItem> cartItems = Collections.emptyList();

    MyCartActivity context;
     static Cart cart;
     Gson gson;


    public MyCartAdapter(MyCartActivity myCartActivity) {
        this.context=myCartActivity;
        gson=new Gson();
    }

    public void updateCartItems(List<CartItem> cartItems) {

        //  cart = CartHelper.getCart();
        //this.cartItems = getCartItems(cart);
        this.cartItems =cartItems;
        notifyDataSetChanged();
    }

    @Override
    public MyCartAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_mycart, parent, false);

       // final CartItem cartItem = getItem(position);

        return new MyCartAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyCartAdapter.MyViewHolder holder, int position) {

        final GetProductListData1 productDetail=cartItems.get(position).getProduct();

                if (productDetail != null) {

                     if (productDetail.getName()!=null){
                        holder.product_name.setText(productDetail.getName());
                    }
                    if (productDetail.getPrice1()!=null){

                        //holder.product_price.setText("$ "+cart.getCost(productDetail)*cart.getTotalQuantity());

                       // holder.product_price.setText("$ " + String.valueOf(cart.getCost(productDetail).setScale(2, BigDecimal.ROUND_HALF_UP)));

                        BigDecimal quantityinbigdecimal = new BigDecimal(productDetail.getQuantity());

                        BigDecimal price = productDetail.getPrice().multiply(quantityinbigdecimal);

                        holder.product_price.setText("$ "+productDetail.getPrice1()+" X "+productDetail.getQuantity()+" = "
                               +String.valueOf(price.setScale(2, BigDecimal.ROUND_HALF_UP))+" $");
                    }

                    if (productDetail.getDescription()!=null){

                        holder.product_decription.setText(productDetail.getDescription());
                    }
                    if (productDetail.getImage()!=null){

                        Picasso.with(context)
                                .load(productDetail.getImage())
                                .error(R.drawable.icon1)
                                .placeholder(R.drawable.icon1)
                                .into(holder.product_image);
                    }

                    holder.quantity.setText(String.valueOf(productDetail.getQuantity()));

                    holder.incrementqty.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            BigDecimal price = new BigDecimal(productDetail.getPrice1());
//                            productDetail.setPrice(price);
//                            productDetail.setName(productDetail.getName());
//                            productDetail.setpId1(Integer.parseInt(productDetail.getId()));
//                            cart.add(productDetail,1,context);

                            int quantity = productDetail.getQuantity();
                            quantity=quantity+1;
                            productDetail.setQuantity(quantity);

                            String data = SharedPreferenceHelper.getSharedPreferenceString(context,"cartdata","");
                            Type type = new TypeToken<List<GetProductListData1>>(){}.getType();
                            List<GetProductListData1> list = gson.fromJson(data,type);

                            for(int i =0;i<list.size();i++){

                                if(productDetail.getId().equals(list.get(i).getId())){

                                    list.remove(i);
                                    list.add(productDetail);
                                    break;
                                }
                            }

                            String savedata = gson.toJson(list);
                            SharedPreferenceHelper.setSharedPreferenceString(context,"cartdata",savedata);
                            notifyDataSetChanged();
                        }
                    });

                    holder.decrementqty.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            BigDecimal price = new BigDecimal(productDetail.getPrice1());
//                            productDetail.setPrice(price);
//                            productDetail.setpId1(Integer.parseInt(productDetail.getId()));
//
//                            if (cart.getQuantity(productDetail)>1) {
//                                if (cart.getQuantity(productDetail) == 1) {
//                                    cart.remove(productDetail, 1);
//                                    context.checkCartList();
//                                } else {
//                                    cart.remove(productDetail, 1);
//                                    notifyDataSetChanged();
//                                }
//                            }else{
//                                Toast.makeText(context,"Quantity should not be less then 1.",Toast.LENGTH_LONG).show();
//                            }

                            int quantity = productDetail.getQuantity();
                            if(quantity>1) {
                                quantity = quantity - 1;

                                productDetail.setQuantity(quantity);

                                String data = SharedPreferenceHelper.getSharedPreferenceString(context,"cartdata","");

                                Type type = new TypeToken<List<GetProductListData1>>() {
                                }.getType();
                                List<GetProductListData1> list = gson.fromJson(data, type);

                                for (int i = 0; i < list.size(); i++) {

                                    if (productDetail.getId().equals(list.get(i).getId())) {

                                        list.remove(i);
                                        list.add(productDetail);
                                        break;
                                    }
                                }

                                String savedata = gson.toJson(list);
                                SharedPreferenceHelper.setSharedPreferenceString(context,"cartdata",savedata);


                                notifyDataSetChanged();

                                ((Activity) context).invalidateOptionsMenu();
                            }else {

                                Toast.makeText(context,"Quantity should not be less then 1.",Toast.LENGTH_LONG).show();

                            }
                        }
                    });

                    holder.deleteitem.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                            //dialog.setTitle("Delete?"); // set title
                            dialog.setTitle( Html.fromHtml("<font color='#006039'>Delete?</font>"));
                            dialog.setMessage("Are you sure you want to delete item ?"); // set message
                            dialog.setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //cart.remove(productDetail);
                                            String data = SharedPreferenceHelper.getSharedPreferenceString(context,"cartdata","");
                                            Type type = new TypeToken<List<GetProductListData1>>(){}.getType();

                                            List<GetProductListData1> detail = gson.fromJson(data,type);

                                            for(int i=0;i<detail.size();i++){

                                                if(detail.get(i).getId().equals(productDetail.getId())){

                                                    detail.remove(i);
                                                    break;
                                                }
                                            }

                                            if(detail.size()>0){

                                                String savedata = gson.toJson(detail);
                                                SharedPreferenceHelper.setSharedPreferenceString(context,"cartdata",savedata);

                                            }else {

                                                SharedPreferenceHelper.setSharedPreferenceString(context,"cartdata","");

                                            }


                                            notifyDataSetChanged();
                                            context.checkCartList();
                                            context.invalidateOptionsMenu();

                                        }
                                    });
                            dialog.setNegativeButton("No",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                            //  dialog.create().show();
                            AlertDialog alertDialog=dialog.create();
                            alertDialog.show();
                            Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                            nbutton.setTextColor(context.getResources().getColor(R.color.colorBlack));
                            Button pbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                            pbutton.setTextColor(context.getResources().getColor(R.color.colorBlack));
                        }
                    });
                }


    }

    @Override
    public int getItemCount()
    {
        return cartItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView product_name,product_decription,product_price,quantity,incrementqty,decrementqty;
        public ImageView product_image,deleteitem;

        public MyViewHolder(View view) {
            super(view);
            product_name = (TextView) view.findViewById(R.id.tv_productName);
            product_decription = (TextView) view.findViewById(R.id.tv_productDesc);
            product_price = (TextView) view.findViewById(R.id.tv_productPrice);
            product_image = (ImageView) view.findViewById(R.id.img_productImage);
            deleteitem = (ImageView) view.findViewById(R.id.deleteitem);
            quantity = (TextView) view.findViewById(R.id.quantity);
            incrementqty = (TextView) view.findViewById(R.id.incrementqty);
            decrementqty = (TextView) view.findViewById(R.id.decrementqty);
        }
    }
}