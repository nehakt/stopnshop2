package com.stopnshop.user.stopnshop.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import com.stopnshop.user.stopnshop.NetworkUtil;
import com.stopnshop.user.stopnshop.R;

import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.UtilView;
import com.yinglan.keyboard.HideUtil;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by neha on 19/9/17.
 */

public class EditProfile_Activity extends AppCompatActivity implements View.OnClickListener,Validator.ValidationListener{

    @NotEmpty
    @BindView(R.id.ed_firstName)
    EditText edFirstName;
    @NotEmpty
    @BindView(R.id.ed_lastName)
    EditText edLastName;
    @NotEmpty
    @Email
    @BindView(R.id.ed_userEmail)
    EditText edEmail;
    @NotEmpty
    @BindView(R.id.ed_phoneNo)
    EditText edPhoneNo;
    @NotEmpty
    @BindView(R.id.dob_edt)
    EditText dob_edt;
//    @NotEmpty
//    @InjectView(R.id.ed_year)
//    EditText edBYear;
//    @NotEmpty
//    @InjectView(R.id.ed_month)
//    EditText edBMonth;

    @BindView(R.id.btn_save)
    Button btnSave;

    private Toolbar toolbar;
    private TextView txtToolbar;

    Gson gson;
    AQuery query;

    String fname="",lname="",email="",phoneno="",dob="";

    Login_AllData logindata;
    private Validator validator;
    private String userid;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);

        ButterKnife.bind(this);
        HideUtil.init(this);

        setupView();

        gson=new Gson();
        query=new AQuery(this);
        dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        setDateTimeField();

        validator = new Validator(this);
        validator.setValidationListener(this);

        btnSave.setOnClickListener(this);
        dob_edt.setOnClickListener(this);
    }

    private void setDateTimeField() {
        dob_edt.setOnClickListener(this);

        final Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, R.style.DialogTheme,new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                if ((newCalendar.before(newDate))){
                    UtilView.showToast(EditProfile_Activity.this, "Please select a valid date");

                } else {

                    dob_edt.setText(dateFormatter.format(newDate.getTime()));
                }
//                dob_edt.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }
    private void setupView() {
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        txtToolbar=(TextView)findViewById(R.id.toolbar_text);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtToolbar.setText("Edit Profile");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        logindata= Constant.getlogindata(this);

        if (logindata!=null){

            if(logindata.getDob()!=null){

                    dob=logindata.getDob();
                   dob_edt.setText(dob);
//                    String[] filterage = dob.split("-");
//                    bmonth = filterage[0];
//                    bdate = filterage[1];
//                    byear = filterage[2];
//
//                    edBdate.setText(bdate);
//                    edBMonth.setText(bmonth);
//                    edBYear.setText(byear);
            }
            if (logindata.getId()!=null ){
                userid=logindata.getId();
            }
            if (logindata.getName()!=null && !logindata.getName().equals("")){
                edFirstName.setText(logindata.getName());
            }
            if (logindata.getLastName()!=null && !logindata.getLastName().equals("")){
                edLastName.setText(logindata.getLastName());
            }
            if (logindata.getEmail()!=null && !logindata.getEmail().equals("")){
                edEmail.setText(logindata.getEmail());
            }
            if (logindata.getContact()!=null && !logindata.equals("")){
                edPhoneNo.setText(logindata.getContact());
            }
        }
    }

    private void getEditProfile(){

        Data data=new Data();
        data.setFname(fname);
        data.setLname(lname);
        data.setEmail(email);
        data.setContactno(phoneno);
        data.setDob(dob);
        data.setUserId(userid);

        ServerRequestApi server_request = new ServerRequestApi("editUser", data);
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        UtilView.showLogCat("editprofile",gsonString);
        callEditQuery(gsonString);
    }

    private void callEditQuery(String gsonString) {

        ProgressDialog progressbar = UtilView.showProgressDialog(this);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);

        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("editprofileresult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

//                            String tag=json.getString("price");

                            //----this code is for rearrange the updated data into the sharedpreference------
                            Login_AllData lData= Constant.getlogindata(EditProfile_Activity.this);
                            lData.setName(fname);
                            lData.setLastName(lname);
                            lData.setEmail(email);
                            lData.setContact(phoneno);
                            lData.setDob(dob);

                            Constant.saveLogindata(EditProfile_Activity.this,lData);

                            new SweetAlertDialog(EditProfile_Activity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setContentText("Profile Updated Successfully.").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    finish();
                                }
                            })
                                    .show();
                        } else {
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                  }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btn_save:

                validator.validate();

                break;

            case R.id.dob_edt:
                fromDatePickerDialog.show();

             //   validator.validate();

                break;
        }
    }

    @Override
    public void onValidationSucceeded() {

        fname=edFirstName.getText().toString();
        lname=edLastName.getText().toString();
        email=edEmail.getText().toString();
        phoneno=edPhoneNo.getText().toString();
        dob=dob_edt.getText().toString();

       // dob=bdate+"-"+bmonth+"-"+byear;

        if(Constant.INTERNET_NOT_CONNECTED!= NetworkUtil.getConnectionStatus(this)){

            getEditProfile();

        }else {

            UtilView.showToast(this,"Internet not Connected");
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}