package com.stopnshop.user.stopnshop.View;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.stopnshop.user.stopnshop.fragment.Fragment_MyAccount;
import com.stopnshop.user.stopnshop.fragment.Fragment_OnlineBuy;
import com.stopnshop.user.stopnshop.fragment.MyOrderFragment;
import com.stopnshop.user.stopnshop.fragment.StoreInfoFragment;
import com.stopnshop.user.stopnshop.model.GasPrice.Datum;
import com.stopnshop.user.stopnshop.model.GasPrice.GasPrice;
import com.stopnshop.user.stopnshop.model.GetProductData;
import com.stopnshop.user.stopnshop.model.GetProductListData;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.Msg;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.img_home)
    ImageView imgHome;
    @BindView(R.id.img_starinfo)
    ImageView imgStarinfo;
    @BindView(R.id.img_myaccount)
    ImageView imgMyAccount;
    @BindView(R.id.img_order)
    ImageView imgOrder;

    @BindView(R.id.tv_home)
    TextView tvHome;
    @BindView(R.id.tv_storeinfo)
    TextView tvStoreinfo;
    @BindView(R.id.tv_myaccount)
    TextView tvMyAccount;
    @BindView(R.id.tv_myorder)
    TextView tvMyOrder;

    @BindView(R.id.ll_home)
    LinearLayout llHome;
    @BindView(R.id.ll_starinfo)
    LinearLayout llStarinfo;
    @BindView(R.id.ll_myaccount)
    LinearLayout llMyAccount;
    @BindView(R.id.ll_myorder)
    LinearLayout llMyOrder;

    @BindView(R.id.tv_gasPrice)
    TextView tvGasPrice;

    Toolbar toolbar;
    TextView txtToolbar;
    LinearLayout llToolbar;
    FragmentTransaction fragmentTransaction;

    ArrayList<GetProductListData> datum;
    private Login_AllData logindata;
    private String userid;
    private String flag;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;

    Intent intent;
    private List<Datum> gasData;
    private String placeOrder;

    @Override
    protected void onResume() {
        super.onResume();
//        getProductList();
        invalidateOptionsMenu();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        intent = getIntent();

        if (intent!= null) {
            placeOrder = intent.getStringExtra("placeorderActivity");
        }
            logindata= Constant.getlogindata(MainActivity.this);

//            if (logindata!=null){

                userid= logindata.getId();
                //  SharedPreferenceHelper.setSharedPreferenceString(MainActivity.this,Constant.usrid,userid);

        llHome.setOnClickListener(this);
        llStarinfo.setOnClickListener(this);
        llMyAccount.setOnClickListener(this);
        llMyOrder.setOnClickListener(this);

        if(placeOrder!=null && placeOrder.equals("0")){

            MyOrderFragment myOrder = new MyOrderFragment();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_layout, myOrder);
            fragmentTransaction.addToBackStack("");
            fragmentTransaction.commit();
            txtToolbar.setText("My Order");
            colorchangeMyOrder();
        }
        else if (InternetConnection.isNetworkAvailable(MainActivity.this)) {
            getProductList();
        } else {
            Msg.t(MainActivity.this, "Please Check Internet Connection");
        }

        getGasPrice();

    }

    public void initView(){
        ButterKnife.bind(this);

        llToolbar=(LinearLayout)findViewById(R.id.ll_toolbar);
        txtToolbar = (TextView)findViewById(R.id.toolbar_text);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void getProductList(){

        Data data=new Data();

        data.setUserId(userid);

        ServerRequestApi pozo = new ServerRequestApi("productList",data);
        Gson gson = new Gson();
        String gsonString = gson.toJson(pozo, ServerRequestApi.class);
        UtilView.showLogCat("productlistreq",gsonString);
        callProductListQuery(gsonString);
    }

    private void callProductListQuery(String gsonString) {

        ProgressDialog progressbar = UtilView.showProgressDialog(this);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        AQuery query = new AQuery(this);

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);

        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("signinresult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            Gson gson = new Gson();
                            GetProductData data = gson.fromJson(json.toString(), GetProductData.class);
                            datum = data.getData();

                            if(datum!=null) {

                                Fragment_OnlineBuy fragment_onlineBuy = new Fragment_OnlineBuy(datum);
                                txtToolbar.setText("Online Buy");
                                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.fragment_layout, fragment_onlineBuy);
                                fragmentTransaction.commit();
                                fragmentTransaction.addToBackStack("");
                                colorchangeHome();
                            }
                        } else
                        {
                            Fragment_OnlineBuy fragment_onlineBuy = new Fragment_OnlineBuy(datum);
                            fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.fragment_layout, fragment_onlineBuy);
                            fragmentTransaction.commit();
                            fragmentTransaction.addToBackStack("");
                            txtToolbar.setText("Online Buy");
                            colorchangeHome();
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Fragment_OnlineBuy fragment_onlineBuy = new Fragment_OnlineBuy(datum);
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_layout, fragment_onlineBuy);
                    fragmentTransaction.commit();
                    fragmentTransaction.addToBackStack("");
                    txtToolbar.setText("Online Buy");
                    colorchangeHome();
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    private void getGasPrice(){

        ServerRequestApi server_request=new ServerRequestApi("gas",null);
        Gson gson=new Gson();
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        UtilView.showLogCat("gasprice",gsonString);
        callGasPrice(gsonString);
    }

    private void callGasPrice(String gsonString) {

        ProgressDialog progressbar = UtilView.showProgressDialog(this);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        AQuery query = new AQuery(this);

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();
        params.put(query.POST_ENTITY, sEntity);
        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("gaspriceresult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            Gson gson=new Gson();

                            GasPrice gasPrice=gson.fromJson(json.toString(), GasPrice.class);
                            //EditProfileData listdata=gson.fromJson()
                            gasData=gasPrice.getData();
                            tvGasPrice.setText("Credit Gas Price"+" "+gasData.get(0).getPrice()+" "+"|"+" "+"Cabs Gas Price"+" "+gasData.get(1).getPrice());

                        } else {
                            Log.e("message", json.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    @OnClick({R.id.ll_home, R.id.ll_starinfo, R.id.ll_myaccount, R.id.ll_myorder})
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.ll_home:

                if (InternetConnection.isNetworkAvailable(MainActivity.this)) {
                    getProductList();
                } else {
                    Msg.t(MainActivity.this, "Please Check Internet Connection");
                }

//                if (placeOrder!=null && placeOrder.equals("0")) {
//
//                    if (InternetConnection.isNetworkAvailable(MainActivity.this)) {
//                        getProductList();
//                    } else {
//                        Msg.t(MainActivity.this, "Please Check Internet Connection");
//                    }
//                }else {
//                Fragment_OnlineBuy fragment_onlineBuy = new Fragment_OnlineBuy(datum);
//                fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.fragment_layout, fragment_onlineBuy);
//                fragmentTransaction.commit();
//                fragmentTransaction.addToBackStack("");
//                txtToolbar.setText("Online Buy");
//                colorchangeHome();
//                }
                break;

            case R.id.ll_starinfo:

                StoreInfoFragment storeInfoFragment = new StoreInfoFragment();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, storeInfoFragment);
                fragmentTransaction.commit();
                fragmentTransaction.addToBackStack("");
                txtToolbar.setText("Store Info");
                colorchangeStoreInfo();
                break;

            case R.id.ll_myaccount:

                String  flag= SharedPreferenceHelper.getSharedPreferenceString(MainActivity.this, Constant.skip,"");

                if (flag!=null && flag.equals("skip")){

                    txtToolbar.setText("My Account");
                }else {
                    txtToolbar.setText("My Profile");
                }
                Fragment_MyAccount myAccount = new Fragment_MyAccount();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, myAccount);
                fragmentTransaction.commit();
                fragmentTransaction.addToBackStack("");
                colorchangeMyAcc();
                break;

            case R.id.ll_myorder:
                MyOrderFragment myOrder = new MyOrderFragment();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_layout, myOrder);
                fragmentTransaction.addToBackStack("");
                fragmentTransaction.commit();
                txtToolbar.setText("My Order");
                colorchangeMyOrder();
                break;
        }
    }
    public void colorchangeHome(){

        llHome.setBackgroundColor(getResources().getColor(R.color.setColor));
        llStarinfo.setBackgroundColor(getResources().getColor(R.color.unsetColor));
        llMyAccount.setBackgroundColor(getResources().getColor(R.color.unsetColor));
        llMyOrder.setBackgroundColor(getResources().getColor(R.color.unsetColor));
    }

    public void colorchangeStoreInfo(){

        llHome.setBackgroundColor(getResources().getColor(R.color.unsetColor));
        llStarinfo.setBackgroundColor(getResources().getColor(R.color.setColor));
        llMyAccount.setBackgroundColor(getResources().getColor(R.color.unsetColor));
        llMyOrder.setBackgroundColor(getResources().getColor(R.color.unsetColor));
    }

    public void colorchangeMyAcc(){

        llHome.setBackgroundColor(getResources().getColor(R.color.unsetColor));
        llStarinfo.setBackgroundColor(getResources().getColor(R.color.unsetColor));
        llMyAccount.setBackgroundColor(getResources().getColor(R.color.setColor));
        llMyOrder.setBackgroundColor(getResources().getColor(R.color.unsetColor));
    }

    public void colorchangeMyOrder(){

        llHome.setBackgroundColor(getResources().getColor(R.color.unsetColor));
        llStarinfo.setBackgroundColor(getResources().getColor(R.color.unsetColor));
        llMyAccount.setBackgroundColor(getResources().getColor(R.color.unsetColor));
        llMyOrder.setBackgroundColor(getResources().getColor(R.color.setColor));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showExitConfirmDialog(); // call the function below
        }
        return super.onKeyDown(keyCode, event);
    }

    public void showExitConfirmDialog() { // just show an dialog
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Exit?"); // set title
        dialog.setMessage("Are you sure you want to exit?"); // set message
        dialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        MainActivity.this.finish(); // when click OK button, finish current activity!
                        finishAffinity();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    }
                });
        dialog.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        // dialog.create().show();
        AlertDialog alert = dialog.create();
        alert.show();
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(nbutton.getResources().getColor(R.color.colorBlack));
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(nbutton.getResources().getColor(R.color.colorBlack));
    }
    public void getUpdateMenu()
    {
        invalidateOptionsMenu();
    }
}