package com.stopnshop.user.stopnshop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by saurabh on 05-Sep-17.
 */

public class GetProductListData  implements Serializable{
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("productData")
    @Expose
    private List<GetProductListData1> productData = null;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<GetProductListData1> getProductData() {
        return productData;
    }

    public void setProductData(List<GetProductListData1> productData) {
        this.productData = productData;
    }

}
