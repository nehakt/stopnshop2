
package com.stopnshop.user.stopnshop.model.FavOrderList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum implements Serializable{


    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("order_number")
    @Expose
    private String orderNumber;
    @SerializedName("total_items")
    @Expose
    private String totalItems;
    @SerializedName("addedDate")
    @Expose
    private String addedDate;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("pickup_time")
    @Expose
    private String pickupTime;
    @SerializedName("order_status")
    @Expose
    private String order_status;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Datum() {
    }
    /**
     * 
     * @param amount
     * @param addedDate
     * @param pickupTime
     * @param orderNumber
     * @param totalItems
     */
    public Datum(String orderNumber, String totalItems, String addedDate, String amount, String pickupTime) {
        super();
        this.orderNumber = orderNumber;
        this.totalItems = totalItems;
        this.addedDate = addedDate;
        this.amount = amount;
        this.pickupTime = pickupTime;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
