package com.stopnshop.user.stopnshop.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.stopnshop.user.stopnshop.NetworkUtil;
import com.stopnshop.user.stopnshop.adapter.MyAllOrderAdapter;
import com.stopnshop.user.stopnshop.adapter.OrderSummaryAdapter;
import com.stopnshop.user.stopnshop.fragment.FavouriteOrderFragment;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.model.orderList.Datum;
import com.stopnshop.user.stopnshop.model.orderList.ProductDatum;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.Msg;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class OrderSummryActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtToolbar;
    RecyclerView recyclerView;

    TextView tvOrderNo, tv_orderDate, tvPickupTime, tvTotalItems, tvTotalAmt,tvNotificationText,tvCashPaying;
    ImageView imgFav;
    Button btnCancelOrder,btnOutside;

    OrderSummaryAdapter mAdapter;
    private Login_AllData logindata;
    private String userid="",userName="";

    Datum orderData;
    MyAllOrderAdapter.MyViewHolder holder;

    Activity activity;
    Gson gson;
    AQuery query;
    SweetAlertDialog sweetAlertDialogView;

    private ArrayList<ProductDatum> productData;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summry);

        initview();
        gson = new Gson();
        query=new AQuery(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        orderData = (Datum) getIntent().getSerializableExtra("orderList");

        if (orderData.getOrderId() != null) {
            tvOrderNo.setText("Order: " + orderData.getOrderId());
        }
        if (orderData.getAddedDate() != null) {
            tv_orderDate.setText("Date: " + orderData.getAddedDate());
        }
        if (orderData.getPickuptime() != null) {
            tvPickupTime.setText("PickupTime: " + orderData.getPickuptime());
        }
        if (orderData.getTotolItem() != null) {
            tvTotalItems.setText("Total Items" + orderData.getTotolItem());
        }
        if (orderData.getAmout() != null) {
            tvTotalAmt.setText("Total Amount: $" + orderData.getAmout());
        }
        if (orderData.getCash_paying()!=null){
            tvCashPaying.setText("Cash Payed: $"+orderData.getCash_paying());
        }

        //----fucnctionality for favourite and unfavourite order

        if (orderData.getFavorite() != null && orderData.getFavorite().equals("0")) {
            imgFav.setImageResource(R.drawable.favorite_white_a);
        } else if (orderData.getFavorite() != null && orderData.getFavorite().equals("1")) {
            imgFav.setImageResource(R.drawable.favorite_white_b);
        }
        imgFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (orderData.getFavorite() != null && orderData.getFavorite().equals("0")) {
                    if (InternetConnection.isNetworkAvailable(OrderSummryActivity.this)) {

                        getAddRemoveFavorite(orderData);

                    } else {

                        Msg.t(OrderSummryActivity.this, "Please Check Internet Connection");
                    }
                } else if (orderData.getFavorite() != null && orderData.getFavorite().equals("1")) {

                    if (InternetConnection.isNetworkAvailable(OrderSummryActivity.this)) {

                        getAddRemoveFavorite(orderData);

                    } else {

                        Msg.t(OrderSummryActivity.this, "Please Check Internet Connection");
                    }
                }
            }
        });

        logindata= Constant.getlogindata(this);

        if (logindata!=null){

            userid= logindata.getId();
            userName=logindata.getName();
        }
//
//        if (orderData.getOrder_status().equals("0")){
//            tvNotificationText.setText("I'am outside");
//            tvNotificationText.setBackgroundColor(Color.RED);
//            tvNotificationText.setTextColor(Color.WHITE);
//        }
        if (orderData.getOrder_status().equals("1") || orderData.getOrder_status().equals("0")){
            tvNotificationText.setText("In Progress");
            tvNotificationText.setTextColor(Color.GREEN);
        }

         else if (orderData.getOrder_status().equals("2")){
            btnOutside.setVisibility(View.GONE);
            btnCancelOrder.setVisibility(View.GONE);
            tvNotificationText.setVisibility(View.GONE);

        }
        else if (orderData.getOrder_status().equals("3")){
            btnOutside.setVisibility(View.GONE);
            btnCancelOrder.setVisibility(View.GONE);
            tvNotificationText.setText("Cancelled");
            tvNotificationText.setTextColor(Color.RED);
        }

            btnCancelOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (Constant.INTERNET_NOT_CONNECTED != NetworkUtil.getConnectionStatus(OrderSummryActivity.this)) {
                        getCancelOrder();
                    } else {
                        UtilView.showToast(OrderSummryActivity.this, "Internet not connected");
                    }
                }
            });

        btnOutside.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getOutside();
            }
        });

        //Create array list of ProductDatum
        productData = new ArrayList<ProductDatum>();

        productData = (ArrayList<ProductDatum>) orderData.getProductData();

        //send productData to adapter to show the all products to perticular order
        mAdapter = new OrderSummaryAdapter(activity, productData);
        recyclerView.setAdapter(mAdapter);
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        sweetAlertDialogView.dismiss();
//    }

    private void initview() {
        // layout_order = (LinearLayout) findViewById(R.id.layout_order);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_orderSummery);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtToolbar = (TextView) findViewById(R.id.toolbar_text);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtToolbar.setText("Order Summery");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvOrderNo = (TextView) findViewById(R.id.tv_orderNo);
        tv_orderDate = (TextView) findViewById(R.id.tv_orderDate);
        tvPickupTime = (TextView) findViewById(R.id.tv_pickupDate);
        tvTotalItems = (TextView) findViewById(R.id.tv_totalItems);
        tvTotalAmt = (TextView) findViewById(R.id.tv_totalAmount);
        imgFav = (ImageView) findViewById(R.id.profile_image1);
        btnCancelOrder=(Button)findViewById(R.id.btn_cancelOrder);
        tvNotificationText=(TextView)findViewById(R.id.notification_txt);
        tvCashPaying=(TextView)findViewById(R.id.tv_cashPaying);
        btnOutside=(Button) findViewById(R.id.btn_outside);
    }

    public void getAddRemoveFavorite(final Datum data) {

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        Map<String, Object> params = null;
        StringEntity sEntity = null;

        Data data1 = new Data();
        data1.setUserId(userid);
        data1.setOrderId(data.getOrderId());
        //0 means data is unfavourite
        //1 means data is favourite

        if (data.getFavorite().equals("1")) {
            data1.setFavoriteStatus("0");
        } else if (data.getFavorite().equals("0")) {
            data1.setFavoriteStatus("1");
        }
        ServerRequestApi server_request = new ServerRequestApi("favoriteOrder", data1);

        String gsonString = gson.toJson(server_request, ServerRequestApi.class);
        Log.e("favauriteorderrequest", gsonString.toString());
        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);

        query.progress(progressDialog).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                Log.e("@flow", json.toString());

                if (status.getCode() == 200) {

                    Log.e("@flow", json.toString());

                    try {

                        if (json.getInt("msg_code") == 1) {
                            if (data.getFavorite().equals("1")) {
                                imgFav.setImageResource(R.drawable.favorite_white_a);
                                data.setFavorite("0");
                                FavouriteOrderFragment.getFavoriteItemList(logindata, OrderSummryActivity.this);
                                //MyAllOrderAdapter.getAddRemoveFavorite(orderData,holder);

                            } else if (data.getFavorite().equals("0")) {
                                imgFav.setImageResource(R.drawable.favorite_white_b);
                                data.setFavorite("1");
                                FavouriteOrderFragment.getFavoriteItemList(logindata, OrderSummryActivity.this);
                                //MyAllOrderAdapter.getAddRemoveFavorite(orderData,holder);
                            }
                        } else {
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }
    public void getCancelOrder(){

        //order_status 0 is for user confirm order
        //order_status 1 is for admin packed the order
        //order_status 2 for admin picked the order
        //order_status 3 is for customer cancel the order

        Data data =new Data();

        data.setUser_type(Constant.user_type);
        data.setOrderId(orderData.getOrderId());
        data.setUserId(userid);
        data.setUserName(userName);
        data.setNotification(userName+" cancel the order its orderid is "+orderData.getOrderId());
        data.setNotification_type("cancel");
        if(orderData.getOrder_status()!=null && orderData.getOrder_status().equals("0")||orderData.getOrder_status().equals("1")){
            data.setOrder_status("3");
        }
        ServerRequestApi server_request=new ServerRequestApi("orderReady",data);
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        UtilView.showLogCat("ordercancelrequest",gsonString);
        callCancelQuery(gsonString);
    }

    private void callCancelQuery(String gsonString) {

        ProgressDialog progressbar = UtilView.showProgressDialog(this);
        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);

        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {
                    UtilView.showLogCat("orderlistResult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            sweetAlertDialogView = new SweetAlertDialog(OrderSummryActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                            sweetAlertDialogView.setTitleText("Success")
                                    .setContentText(" Your Order Is Cancel")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            finish();
                                        }
                                    })
                                    .show();
                            Button viewGroup = (Button) sweetAlertDialogView.findViewById(R.id.confirm_button);
                            viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.unsetColor));

                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    public void getOutside(){

        logindata= Constant.getlogindata(this);

        if (logindata!=null){

            userid= logindata.getId();
            userName=logindata.getName();
        }
        Data data=new Data();
        data.setUserId(userid);
        data.setUserName(userName);
        data.setOrderId(orderData.getOrderId());
        data.setUser_type(Constant.user_type);
        data.setNotification_type("outside");
        data.setNotification(userName+" is outside the store to pick their order and it is order_id is "+orderData.getOrderId());

        ServerRequestApi server_request=new ServerRequestApi("storeOutsiteMessage",data);
        Gson gson=new Gson();
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        UtilView.showLogCat("outsideReq",gsonString);

        ProgressDialog progressbar = UtilView.showProgressDialog(this);
        AQuery query=new AQuery(this);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);
        //Intent intent=new Intent(context, OrderSummryActivity.class);
//                        startActivity(intent);
        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("orderListResult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            sweetAlertDialogView = new SweetAlertDialog(OrderSummryActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                            sweetAlertDialogView.setTitleText("Success")
                                    .setContentText(" Your request is Send Successfully")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {

//                                            tvOutside.setVisibility(View.GONE);
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();
                            Button viewGroup = (Button) sweetAlertDialogView.findViewById(R.id.confirm_button);
                            viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.unsetColor));

                        } else
                        {
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }
}