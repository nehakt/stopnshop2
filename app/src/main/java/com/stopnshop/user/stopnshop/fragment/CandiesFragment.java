package com.stopnshop.user.stopnshop.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stopnshop.user.stopnshop.View.MainActivity;
import com.stopnshop.user.stopnshop.adapter.ItemsAdapter1;
import com.stopnshop.user.stopnshop.model.GetProductListData;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.R;

import java.util.List;

public class CandiesFragment extends Fragment {

    RecyclerView recyclerView;
    static ItemsAdapter1 mAdapter;
    // GridLayoutManager gridLayoutManager;
    static MainActivity context;
    List<GetProductListData1> productdata;
    List<GetProductListData> datum;
    TextView message_txt;

    public CandiesFragment(List<GetProductListData1> productdata,List<GetProductListData> datum){
        this.productdata=productdata;
        this.datum=datum;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context= (MainActivity) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        //--------again call oncreate option menu in CandiesFragment ------
        setHasOptionsMenu(isVisible());
        //proData();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_candies,container,false);
        message_txt = (TextView)view.findViewById(R.id.message_txt);
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_candiesview);

        final GridLayoutManager gridLayoutManager=new GridLayoutManager(getActivity(),2);
        recyclerView.setLayoutManager(gridLayoutManager);

        if (productdata.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            message_txt.setVisibility(View.VISIBLE);
        }
        else {
            recyclerView.setVisibility(View.VISIBLE);
            message_txt.setVisibility(View.GONE);

            mAdapter = new ItemsAdapter1(context, productdata, datum);
            recyclerView.setAdapter(mAdapter);
        }

//        for (int i=0;i<=productdata.size();i++) {
//            mAdapter = new ItemsAdapter1(context, productdata,datum);
//            recyclerView.setAdapter(mAdapter);
//        }

            return view;
    }

    //----------For Search Functionality---------

    public static void SearchCandies(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (mAdapter != null) mAdapter.getFilter().filter(newText);

                return true;
            }
        });
    }
    //---------End Search Functionality----------
}