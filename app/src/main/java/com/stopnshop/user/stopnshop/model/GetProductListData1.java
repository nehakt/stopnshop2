package com.stopnshop.user.stopnshop.model;

import com.android.tonyvu.sc.model.Saleable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.stopnshop.user.stopnshop.model.ComboDeal.ProductDetail;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by saurabh on 05-Sep-17.
 */

public class GetProductListData1 extends GetProductListData implements Saleable,Serializable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("favoriteStatus")
    @Expose
    private String favoriteStatus;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("price")
    @Expose
    private String price1;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("combodetail")
    @Expose
    private List<ProductDetail> comboProductDetail = null;


    BigDecimal pPrice;

    int pId1;

    int quantity;


    /**
     * No args constructor for use in serialization
     *
     */
    public GetProductListData1() {
    }
    /**
     *
     * @param id
     * @param price
     * @param description
     * @param name
     * @param favoriteStatus
     * @param image
     */
    public GetProductListData1(String id, String favoriteStatus, String name, String description, String price, String image) {
        super();
        this.id = id;
        this.favoriteStatus = favoriteStatus;
        this.name = name;
        this.description = description;
        this.price1 = price;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFavoriteStatus() {
        return favoriteStatus;
    }

    public void setFavoriteStatus(String favoriteStatus) {
        this.favoriteStatus = favoriteStatus;
    }

    public void setPrice(BigDecimal price) {
        this.pPrice = price;
    }

    @Override
    public BigDecimal getPrice() {
        return pPrice;
    }

    public int getpId1() {
        return pId1;
    }

    public void setpId1(int pId1) {
        this.pId1 = pId1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice1() {
        return price1;
    }

    public void setPrice1(String price1) {
        this.price1 = price1;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof GetProductListData1)) return false;

        return (this.pId1 == ((GetProductListData1) o).getpId1());
    }
    public int hashCode() {
        final int prime = 31;
        int hash = 1;
        hash = hash * prime + pId1;
        hash = hash * prime + (name == null ? 0 : name.hashCode());
        hash = hash * prime + (pPrice == null ? 0 : pPrice.hashCode());
        hash = hash * prime + (description == null ? 0 : description.hashCode());

        return hash;
    }

    public List<ProductDetail> getComboProductDetail() {
        return comboProductDetail;
    }

    public void setComboProductDetail(List<ProductDetail> comboProductDetail) {
        this.comboProductDetail = comboProductDetail;
    }

}