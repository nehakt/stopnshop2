package com.stopnshop.user.stopnshop;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.stopnshop.user.stopnshop.fragment.AllOrderFragment;
import com.stopnshop.user.stopnshop.fragment.FavouriteOrderFragment;

/**
 * Created by saurabh on 22-Aug-17.
 */

public class PagerAdapterMyOrder extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public PagerAdapterMyOrder(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                AllOrderFragment tab1 = new AllOrderFragment();
                return tab1;
            case 1:
                FavouriteOrderFragment tab2 = new FavouriteOrderFragment();
                return tab2;

            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount()
    {
        return tabCount;
    }
}