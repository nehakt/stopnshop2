package com.stopnshop.user.stopnshop.model.ComboDeal;

/**
 * Created by neha on 6/12/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ComboDealData {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("msg_code")
    @Expose
    private int msgCode;
    @SerializedName("data")
    @Expose
    private List<ComboDatum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public ComboDealData() {
    }

    /**
     *
     * @param message
     * @param msgCode
     * @param data
     */
    public ComboDealData(String message, int msgCode, List<ComboDatum> data) {
        super();
        this.message = message;
        this.msgCode = msgCode;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(int msgCode) {
        this.msgCode = msgCode;
    }

    public List<ComboDatum> getData() {
        return data;
    }

    public void setData(List<ComboDatum> data) {
        this.data = data;
    }
}