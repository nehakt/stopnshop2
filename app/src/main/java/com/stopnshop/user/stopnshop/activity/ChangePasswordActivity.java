package com.stopnshop.user.stopnshop.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.stopnshop.user.stopnshop.NetworkUtil;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.UtilView;
import com.yinglan.keyboard.HideUtil;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by neha on 20/9/17.
 */

public class ChangePasswordActivity extends AppCompatActivity implements Validator.ValidationListener, View.OnClickListener{

    @NotEmpty
    @BindView(R.id.ed_password)
    EditText edPassword;
    @NotEmpty
    @BindView(R.id.ed_reenterpassword)
    EditText edReEnterPassword;

    @BindView(R.id.btn_update)
    Button btnUpdate;

    Toolbar toolbar;
    TextView txtToolbar;

    Gson gson;
    AQuery query;

    String newPassword="",confirmPassword="",userid="";
    private Login_AllData logindata;
    private Validator validator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);

        ButterKnife.bind(this);
        HideUtil.init(this);

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        txtToolbar=(TextView)findViewById(R.id.toolbar_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtToolbar.setText("Change Password");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
        logindata= Constant.getlogindata(ChangePasswordActivity.this);
        userid=logindata.getId();

        query=new AQuery(this);
        gson=new Gson();

        validator = new Validator(this);
        validator.setValidationListener(this);

        btnUpdate.setOnClickListener(this);
    }

    private void getChangePassword(){

        Data data=new Data();
        data.setUserId(userid);
        data.setPassword(newPassword);

        ServerRequestApi seever_request=new ServerRequestApi("changePassword",data);
        String gsonString=gson.toJson(seever_request,ServerRequestApi.class);
        UtilView.showLogCat("changepassRequest",gsonString);
        callChangePassQuery(gsonString);
    }

    private void callChangePassQuery(String gsonString) {

        ProgressDialog progressbar = UtilView.showProgressDialog(this);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);

        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("changepassResponse",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            new SweetAlertDialog(ChangePasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setContentText("Password Updated Successfully.").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    finish();
                                }
                            })
                                    .show();

                        } else {
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_update:
                validator.validate();
                break;
        }
    }
    @Override
    public void onValidationSucceeded() {

        newPassword=edPassword.getText().toString();
        confirmPassword=edReEnterPassword.getText().toString();

        if(Constant.INTERNET_NOT_CONNECTED!= NetworkUtil.getConnectionStatus(this)){

            if (newPassword.equals(confirmPassword)) {
                getChangePassword();
            }else{

                UtilView.showToast(this,"Password and confirm password not match!");
            }

        }else {

            UtilView.showToast(this,"Internet not Connected");
        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

    }
}
