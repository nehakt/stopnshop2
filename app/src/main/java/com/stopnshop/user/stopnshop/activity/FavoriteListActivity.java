package com.stopnshop.user.stopnshop.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.adapter.FavoriteList_Adapter;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.favoritelis_pojo.Datum;
import com.stopnshop.user.stopnshop.model.favoritelis_pojo.FavoriteListPojoclass;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;


import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FavoriteListActivity extends AppCompatActivity {

    RecyclerView recycler_view;
    private Toolbar toolbar;
    private TextView txtToolbar;
    private String flag;
    private Login_AllData logindata;
    private AQuery aq;
    private List<Datum> favorite_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_list);
        initview();

        flag = SharedPreferenceHelper.getSharedPreferenceString(FavoriteListActivity.this, Constant.skip, "");

        if (flag != null && flag.equals("skip")) {

        } else {

            logindata = Constant.getlogindata(FavoriteListActivity.this);

            if (InternetConnection.isNetworkAvailable(FavoriteListActivity.this)) {

                favoriteitemlist();

            } else {


            }
        }
        LinearLayoutManager layoutManager=new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recycler_view.setLayoutManager(layoutManager);
    }

    private void favoriteitemlist() {
        aq = new AQuery(this);
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        Map<String, Object> params = null;
        StringEntity sEntity = null;

        Data data=new Data();

        data.setUserId(logindata.getId());

        ServerRequestApi pozo = new ServerRequestApi("favoriteList", data);
        final Gson gson = new Gson();
        String s = gson.toJson(pozo, ServerRequestApi.class);
        Log.e("@flow", s);

        try {
            sEntity = new StringEntity(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(aq.POST_ENTITY, sEntity);

        aq.progress(progressDialog).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                Log.e("@flow", json.toString());

                if (status.getCode() == 200) {

                    Log.e("@flow", json.toString());

                    try {

                        if (json.getInt("msg_code") == 1) {

                            FavoriteListPojoclass listData=gson.fromJson(json.toString(), FavoriteListPojoclass.class);
                            //EditProfileData listdata=gson.fromJson()
                            favorite_data=listData.getData();

                            FavoriteList_Adapter mAdapter=new FavoriteList_Adapter(FavoriteListActivity.this,favorite_data);
                            recycler_view.setAdapter(mAdapter);


                        } else {

                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    private void initview() {

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        txtToolbar=(TextView)findViewById(R.id.toolbar_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtToolbar.setText("Favourite List");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        recycler_view=(RecyclerView)findViewById(R.id.recycler_view);
    }
}