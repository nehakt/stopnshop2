package com.stopnshop.user.stopnshop.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.stopnshop.user.stopnshop.model.RandomOrder;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.model.productlist_pojo.CartProduct;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.ConfirmationActivity;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.PayPalConfig;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class PaymentOptionsActivity extends AppCompatActivity implements View.OnClickListener {

    TextView payatstore_btn,paypal_btn,tvTotalAmt;
    private Toolbar toolbar;
    private TextView txtToolbar,total_amount;
    private String totalItem;
    private String totalAmount;
    private ArrayList<CartProduct> cartproductList;
    private String name;
    private String contact_no;
    public static String pickup_time;

    //----Paypal Integration
    public static final int PAYPAL_REQUEST_CODE = 123;
    private static PayPalConfiguration config;
    private String paymentAmount;
    private String paymentDetails;

    String randomOrder;
    String order_no;

    private static final int MAX_LENGTH = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_options);

        Intent in = getIntent();
        if(in!=null){

            totalItem = in.getStringExtra("totalItems");
            totalAmount = in.getStringExtra("totalAmount");
            cartproductList = (ArrayList<CartProduct>) in.getSerializableExtra("product_list");
            name = in.getStringExtra("name");
            contact_no = in.getStringExtra("contact_no");
            pickup_time = in.getStringExtra("pickuptime");
        }

        initview();
        //----Paypal Integration
        config = new PayPalConfiguration()
                // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                // or live (ENVIRONMENT_PRODUCTION)
                .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
                .clientId(PayPalConfig.PAYPAL_CLIENT_ID);

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        //------End Paypal
    }

    private void initview() {
        payatstore_btn = (TextView)findViewById(R.id.payatstore_btn);
        paypal_btn=(TextView)findViewById(R.id.tvPaypal);
        tvTotalAmt=(TextView)findViewById(R.id.total_amount);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        txtToolbar=(TextView) findViewById(R.id.toolbar_text);
        total_amount=(TextView) findViewById(R.id.total_amount);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtToolbar.setText("Payment Options");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        payatstore_btn.setOnClickListener(this);
        paypal_btn.setOnClickListener(this);

        final Cart cart = CartHelper.getCart();
        total_amount.setText(totalAmount);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.payatstore_btn:
                Intent intent = new Intent(PaymentOptionsActivity.this, PayatStoreActivity.class);
                intent.putExtra("totalItems",totalItem);
                intent.putExtra("totalAmount",totalAmount);
                intent.putExtra("product_list",cartproductList);
                intent.putExtra("name",name);
                intent.putExtra("contact_no",contact_no);
                intent.putExtra("pickuptime",pickup_time);
                startActivity(intent);
//                finish();
                break;

            case R.id.tvPaypal:
                //----Paypal Integration
                getPayment();
        }
    }
    //----Paypal Integration
    private void getPayment() {
        //Getting the amount from editText
        paymentAmount = tvTotalAmt.getText().toString().trim();

        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(paymentAmount)), "USD", "Simplified Coding Fee",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);

                        randomOrder=random();
                        placeorderrequest(randomOrder);

                        //Starting a new activity for the payment details and also putting the payment details with intent
//                        startActivity(new Intent(this, ConfirmationActivity.class)
//                                .putExtra("PaymentDetails", paymentDetails)
//                                .putExtra("PaymentAmount", paymentAmount)
//                                .putExtra("orderNo",order_no));

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }//----End paypal

    private void placeorderrequest(String randomOrder) {

        Login_AllData logindata;

        AQuery aq=new AQuery(this);
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        Map<String, Object> params = null;
        StringEntity sEntity = null;

        logindata = Constant.getlogindata(this);
        Data data=new Data();
        data.setUserId(logindata.getId());
        data.setTotal_item(totalItem);
        data.setTotal_amount(totalAmount);
        data.setProduct_list(cartproductList);
        data.setName(name);
        data.setContactno(contact_no);
        data.setPickup_time(pickup_time);
        data.setCash_paying(totalAmount);
        data.setOrder_number(randomOrder);
        // ---- 0=for pay at store
        // ---- 1 for online payment
        data.setPaymentType("1");

        ServerRequestApi pozo = new ServerRequestApi("place_order", data);
        final Gson gson = new Gson();
        String s = gson.toJson(pozo, ServerRequestApi.class);
        UtilView.showLogCat("payatstore",s);

        try {
            sEntity = new StringEntity(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(aq.POST_ENTITY, sEntity);

        aq.progress(progressDialog).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                Log.e("@flow", json.toString());

                if (status.getCode() == 200) {

                    Log.e("@flow", json.toString());

                    try {
                        if (json.getInt("msg_code") == 1) {

                            CartHelper.getCart().clear();

                            Gson gson = new Gson();
                            RandomOrder data = gson.fromJson(json.toString(),  RandomOrder.class);

                            order_no=data.getOrderId();

                            Intent intent = new Intent(PaymentOptionsActivity.this, ConfirmationActivity.class);
                            intent.putExtra("PaymentDetails", paymentDetails);
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("orderNo",order_no);
                            startActivity(intent);

                        } else {

                            Log.e("message", json.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }
    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;
        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }
    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
}