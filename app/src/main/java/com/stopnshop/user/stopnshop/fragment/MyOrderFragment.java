package com.stopnshop.user.stopnshop.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stopnshop.user.stopnshop.PagerAdapterMyOrder;
import com.stopnshop.user.stopnshop.View.LoginActivity;
import com.stopnshop.user.stopnshop.activity.FavoriteListActivity;
import com.stopnshop.user.stopnshop.activity.MyCartActivity;
import com.stopnshop.user.stopnshop.activity.NotificationActivity;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MyOrderFragment extends Fragment implements TabLayout.OnTabSelectedListener {

    //This is our tablayout
    private TabLayout tabLayout;
    private Toolbar toolbar;
    //This is our viewPager
    private ViewPager viewPager;
    TextView browse_profile_txt;
    private MenuInflater inflater;
    Activity context;

    Cart cart;
    LayerDrawable cartmenuIcon;

    private Login_AllData logindata;
    private String userid="";
    private String flag;

    PagerAdapterMyOrder adapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (Activity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_order, container, false);
        browse_profile_txt = (TextView) view.findViewById(R.id.toolbar_text);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        //Adding the tabs using addTab() method

        userid = SharedPreferenceHelper.getSharedPreferenceString(context, "iserid", "");
        flag=SharedPreferenceHelper.getSharedPreferenceString(context,Constant.skip,"");

        if (userid != null && !userid.equals("")) {

        tabLayout.addTab(tabLayout.newTab().setText("All Orders"));
        tabLayout.addTab(tabLayout.newTab().setText("Favourite Order"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        setHasOptionsMenu(true);
        cart = CartHelper.getCart();

        //Initializing viewPager
        viewPager = (ViewPager) view.findViewById(R.id.pagermyorder);
        //Creating our pager adapter
         adapter = new PagerAdapterMyOrder(getActivity().getSupportFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        //tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        //Adding onTabSelectedListener to swipe views

        tabLayout.setOnTabSelectedListener(this);

    }else{
            SweetAlertDialog sweetAlertDialogView;
            sweetAlertDialogView=  new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
            sweetAlertDialogView.setTitleText("Login Failed!")
                    .setContentText("Please Login First.")
                    .show();

           // UtilView.showToast(context,"Please Login First!");
    }
        return view;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.startinfo_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem itemlogout=menu.findItem(R.id.logout);
        MenuItem itemnotification=menu.findItem(R.id.notification_bttn);
        MenuItem itemFav=menu.findItem(R.id.favorite_list);

        if (flag!=null&&flag.equals("skip")) {
            itemlogout.setTitle("Login");
            itemnotification.setVisible(false);
            itemFav.setVisible(false);
        }else {
            itemlogout.setVisible(true);
            itemlogout.setTitle("Logout");
            itemnotification.setVisible(true);
            itemFav.setVisible(true);
        }

        MenuItem item = menu.findItem(R.id.search_button);
        item.setVisible(false);
        MenuItem itemCart = menu.findItem(R.id.shopping_bttn);
        cartmenuIcon = (LayerDrawable) itemCart.getIcon();
        showBadgeCount();
        //UtilView.setBadgeCount(context, cartmenuIcon, "" + Cart.mapSize);
    }

    private void showBadgeCount() {

        Gson gson = new Gson();
        Type type = new TypeToken<List<GetProductListData1>>(){}.getType();

        String data = SharedPreferenceHelper.getSharedPreferenceString(context,"cartdata","");

        List<GetProductListData1> list = gson.fromJson(data,type);

        int quantity = 0;

        if(list!=null && list.size()>0) {

//            for (int i = 0; i < list.size(); i++) {
//
//                quantity = quantity + list.get(i).getQuantity();
//            }

            UtilView.setBadgeCount(context, cartmenuIcon, "" + list.size());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.notification_bttn:
                Intent intent=new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
              //  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;

            case R.id.shopping_bttn:
                Intent intent1=new Intent(getActivity(), MyCartActivity.class);
                startActivity(intent1);
               // intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;

            case R.id.logout:
                //------------clear the shared perference data(logindata and userid)------------
               getLogout();
                return true;

//            case R.id.login:
//                intent = new Intent(context, LoginActivity.class);
//                context.startActivity(intent);
//
//                return true;

            case R.id.favorite_list:
                intent=new Intent(getActivity(), FavoriteListActivity.class);
                startActivity(intent);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getLogout(){
        logindata= Constant.getlogindata(context);

        if (logindata!=null){

            userid= logindata.getId();

        }

        Data data=new Data();
        data.setUserId(userid);
        data.setUser_type(Constant.user_type1);

        ServerRequestApi server_request=new ServerRequestApi("logoutUser",data);
        Gson gson=new Gson();
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        UtilView.showLogCat("logoutReq",gsonString);

        ProgressDialog progressbar = UtilView.showProgressDialog(context);
        AQuery query=new AQuery(context);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);
        //Intent intent=new Intent(context, OrderSummryActivity.class);
//                        startActivity(intent);
        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("orderListResult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            SharedPreferenceHelper.setSharedPreferenceString(context, "iserid", "");
                            CartHelper.getCart().clear();
                            Intent intent = new Intent(context,LoginActivity.class);
                            startActivity(intent);
                            context.finish();
                        } else
                        {
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }
}