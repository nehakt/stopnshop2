package com.stopnshop.user.stopnshop.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.stopnshop.user.stopnshop.adapter.NewRequestAdapter;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.model.productlist_pojo.Datum;
import com.stopnshop.user.stopnshop.model.productlist_pojo.RequestProductListPojo;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.Msg;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewAddActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private TextView txtToolbar;
    TextView chekout_btn;
    ImageView back_btn,imgAddProduct;
    RecyclerView recyclerView;

    List<Object>list;
    private Login_AllData logindata;
    private List<Datum> productlistsata;

    private AQuery query;
    Gson gson;

    NewRequestAdapter mAdapter;

    public static NewAddActivity newAddActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_add);

        initview();

        newAddActivity=this;

        LinearLayoutManager layoutManager=new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);

        gson = new Gson();
        query = new AQuery(this);

        String flag=  SharedPreferenceHelper.getSharedPreferenceString(NewAddActivity.this, Constant.skip,"");

        if (flag!=null&&flag.equals("skip")){

        }else {

            logindata = Constant.getlogindata(NewAddActivity.this);
        }

        if (InternetConnection.isNetworkAvailable(this)){

            if (flag!=null&&flag.equals("skip")){

            }else {
                getProductList();
            }
        }else {

            Msg.t(this, "Please Check Internet Connection");
        }
    }

    private void initview() {

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        txtToolbar=(TextView)findViewById(R.id.toolbar_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtToolbar.setText("New Product Request");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        chekout_btn = (TextView)findViewById(R.id.chekout_btn);
        imgAddProduct = (ImageView) findViewById(R.id.img_addPruduct);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_mycart);

        imgAddProduct.setOnClickListener(this);
    }

    private void getProductList(){

        Data data=new Data();

        data.setUserId(logindata.getId());

        ServerRequestApi pozo = new ServerRequestApi("requestList", data);

        String gsonString = gson.toJson(pozo, ServerRequestApi.class);
        UtilView.showLogCat("@flow", gsonString);
        callProductListQuery(gsonString);
    }

    private void callProductListQuery(String gsonString) {

        ProgressDialog progressbar = UtilView.showProgressDialog(this);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);

        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("signinresult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            RequestProductListPojo listData=gson.fromJson(json.toString(), RequestProductListPojo.class);
                            //EditProfileData listdata=gson.fromJson()
                            productlistsata=listData.getData();

                            mAdapter=new NewRequestAdapter(NewAddActivity.this,productlistsata);
                            recyclerView.setAdapter(mAdapter);

//                            Intent intent = new Intent(AddProductActivity.this, NewAddActivity.class);
//                            startActivity(intent);
                        } else {

                            Log.e("message", json.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

             case R.id.img_addPruduct:
                 Intent intent = new Intent(NewAddActivity.this, AddProductActivity.class);
                 startActivity(intent);
                 //finish();
                 break;
        }
    }
    public void updateAdapter() {

        getProductList();
    }
}