package com.stopnshop.user.stopnshop.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.model.Saleable;
import com.android.tonyvu.sc.util.CartHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stopnshop.user.stopnshop.View.LoginActivity;
import com.stopnshop.user.stopnshop.adapter.MyCartAdapter;
import com.stopnshop.user.stopnshop.model.CartItem;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.model.productlist_pojo.CartProduct;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MyCartActivity extends AppCompatActivity implements View.OnClickListener{

    private Toolbar toolbar;
    private TextView txtToolbar,myordertext;

    RecyclerView recyclerView;

    MyCartAdapter cardAdapter;
    TextView chekout_btn;
    LinearLayout llEmptyCart;
    LinearLayout llCheckOut;
    ArrayList<CartProduct> cartproductList;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;

    Cart cart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);

        initview();

        cart = CartHelper.getCart();

        Set<Saleable> productSet = cart.getProducts();
        if (productSet != null && productSet.size() > 0) {
            myordertext.setVisibility(View.VISIBLE);
        }else {
            myordertext.setVisibility(View.GONE);
        }

        checkCartList();
    }

     private void initview() {

        chekout_btn = (TextView)findViewById(R.id.chekout_btn);
         llEmptyCart=(LinearLayout) findViewById(R.id.ll_emptycard);
        llCheckOut=(LinearLayout)findViewById(R.id.ll_checkout);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_mycart);

        LinearLayoutManager layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        chekout_btn.setOnClickListener(this);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        txtToolbar=(TextView)findViewById(R.id.toolbar_text);
        myordertext=(TextView)findViewById(R.id.myordertext);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtToolbar.setText("My Cart");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    public void checkCartList() {

//        if (cart != null) {
//            cardAdapter = new MyCartAdapter(this);
//            List<CartItem> cartItems = getCartItems(cart);
//            if (cartItems != null && cartItems.size()>0) {
//                cardAdapter.updateCartItems(getCartItems(cart));
//                recyclerView.setAdapter(cardAdapter);
//
//                showCartView();
//            }else {
//                hideCartView();
//            }
//            }

        cardAdapter = new MyCartAdapter(this);
        Gson gson=new Gson();

        String data =SharedPreferenceHelper.getSharedPreferenceString(this,"cartdata","");

        Type type = new TypeToken<List<GetProductListData1>>(){}.getType();

        List<GetProductListData1> details = gson.fromJson(data,type);

        List<CartItem> list = new ArrayList<>();

        if(details!=null && details.size()>=0) {

            for (int i = 0; i < details.size(); i++) {

                CartItem item = new CartItem();

                item.setQuantity(details.get(i).getQuantity());
                item.setProduct(details.get(i));
                list.add(item);

            }

        }

        if(list!=null && list.size()>0){


            cardAdapter.updateCartItems(list);
            recyclerView.setAdapter(cardAdapter);

            showCartView();
        }else {
            hideCartView();
        }

    }

    private void hideCartView() {
        llEmptyCart.setVisibility(View.VISIBLE);
        llCheckOut.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        myordertext.setVisibility(View.GONE);
        myordertext.setVisibility(View.GONE);
    }
    private void showCartView() {
        llEmptyCart.setVisibility(View.GONE);
        llCheckOut.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
        myordertext.setVisibility(View.VISIBLE);
        myordertext.setVisibility(View.VISIBLE);
    }
        private List<CartItem> getCartItems(Cart cart) {

                List<CartItem> cartItems = new ArrayList<CartItem>();
                Map<Saleable, Integer> itemMap = cart.getItemWithQuantity(this);
                for (Map.Entry<Saleable, Integer> entry : itemMap.entrySet()) {

                    CartItem cartItem = new CartItem();
                    cartItem.setProduct((GetProductListData1) entry.getKey());
                    cartItem.setQuantity(entry.getValue());
                    cartItems.add(cartItem);
                }
            return cartItems;
        }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.chekout_btn:
                Gson gson = new Gson();
                Type type = new TypeToken<List<GetProductListData1>>(){}.getType();
                String data = SharedPreferenceHelper.getSharedPreferenceString(this,"cartdata","");
                List<GetProductListData1> detail = gson.fromJson(data,type);

                    if (detail != null && detail.size() > 0) {
                        String flag=  SharedPreferenceHelper.getSharedPreferenceString(this, Constant.skip,"");

                        if(flag!=null && !flag.equals("") && flag.equals("skip")){
                            Intent intent = new Intent(this, LoginActivity.class);
                            startActivity(intent);
                        }else {
                           String totalQuantities = getTotalQuantity();

                           String totalAmount = String.valueOf(getTotalAmount().setScale(2, BigDecimal.ROUND_HALF_UP));

                            cartproductList = new ArrayList<>();
                            for(int i=0;i<detail.size();i++){
                                BigDecimal decimalquantity = new BigDecimal(detail.get(i).getQuantity());
                                BigDecimal finalprice = detail.get(i).getPrice().multiply(decimalquantity);
                                CartProduct cardPd = new CartProduct(detail.get(i).getId(), detail.get(i).getName(), "" + detail.get(i).getQuantity(), "" + finalprice, detail.get(i).getImage());
                                cartproductList.add(cardPd);
                            }

                            Intent intent = new Intent(MyCartActivity.this, CheckOutActivity.class);
                            intent.putExtra("totalItems",totalQuantities);
                            intent.putExtra("totalAmount",totalAmount);
                            intent.putExtra("product_list",cartproductList);
                            startActivity(intent);
                            finish();
                        }
                    }else {
                   }

                break;
        }
    }

    private BigDecimal getTotalAmount() {

        Gson gson = new Gson();
        Type type = new TypeToken<List<GetProductListData1>>(){}.getType();
        String data = SharedPreferenceHelper.getSharedPreferenceString(this,"cartdata","");
        List<GetProductListData1> productdetail = gson.fromJson(data,type);
        BigDecimal totalprice = null;
        for (int i=0;i<productdetail.size();i++){

            if(productdetail.get(i).getQuantity()>1){

                BigDecimal bigDecimalquantity = new BigDecimal(productdetail.get(i).getQuantity());

                if(totalprice!=null) {

                    totalprice = totalprice.add(productdetail.get(i).getPrice().multiply(bigDecimalquantity));

                }else {

                    totalprice=productdetail.get(i).getPrice().multiply(bigDecimalquantity);
                }
            }else {

                if(totalprice!=null) {

                    totalprice = totalprice.add(productdetail.get(i).getPrice());
                }else {

                    totalprice=productdetail.get(i).getPrice();
                }
            }
        }
        return totalprice;
    }

    private String getTotalQuantity() {

        Gson gson = new Gson();
        Type type = new TypeToken<List<GetProductListData1>>(){}.getType();

        String data = SharedPreferenceHelper.getSharedPreferenceString(this,"cartdata","");

        List<GetProductListData1> list = gson.fromJson(data,type);

        int quantity = 0;

        if(list!=null && list.size()>0) {

            for (int i = 0; i < list.size(); i++) {

                quantity = quantity + list.get(i).getQuantity();
            }

        }
        return "" +quantity;
    }
}