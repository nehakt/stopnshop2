package com.stopnshop.user.stopnshop.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stopnshop.user.stopnshop.View.MainActivity;
import com.stopnshop.user.stopnshop.adapter.ItemsAdapter;
import com.stopnshop.user.stopnshop.model.GetProductListData;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

public class AllFragment extends Fragment {

    RecyclerView recyclerView;
    static ItemsAdapter mAdapter;
    List<GetProductListData> datum;
    GetProductListData productListData;
    Login_AllData logindata;
    MainActivity context;

    TextView message_txt;

    public AllFragment(){

    }

    public AllFragment(List<GetProductListData> datum) {
        this.datum=datum;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context= (MainActivity) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(isVisible());
   }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_all,container,false);

        message_txt = (TextView)view.findViewById(R.id.message_txt);
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_all);

        GridLayoutManager gridLayoutManager=new GridLayoutManager(getActivity(),2);
        recyclerView.setLayoutManager(gridLayoutManager);

        String flag=  SharedPreferenceHelper.getSharedPreferenceString(getActivity(),Constant.skip,"");

        if (flag!=null&&flag.equals("skip")){

        }else {

            logindata = Constant.getlogindata(getActivity());
        }
       try{


            List<GetProductListData1> getProductListData1s=new ArrayList<>();

            for (final GetProductListData productListData:datum){

                getProductListData1s.addAll(productListData.getProductData());
            }
            if (getProductListData1s.isEmpty()){
                recyclerView.setVisibility(View.GONE);
                message_txt.setVisibility(View.VISIBLE);
            }

            mAdapter=new ItemsAdapter(context,getProductListData1s);
            recyclerView.setAdapter(mAdapter);
           message_txt.setVisibility(View.GONE);

        }catch (Exception e){
        }
        return view;
    }
//    public void proData(){
//
//        String flag=  SharedPreferenceHelper.getSharedPreferenceString(getActivity(),Constant.skip,"");
//
//        if (flag!=null&&flag.equals("skip")){
//
//        }else {
//
//            logindata = Constant.getlogindata(getActivity());
//        }
//        try{
//            List<GetProductListData1> getProductListData1s=new ArrayList<>();
//
//            for (final GetProductListData productListData:datum){
//
//                getProductListData1s.addAll(productListData.getProductData());
//            }
//
//            mAdapter=new ItemsAdapter(context,getProductListData1s);
//            recyclerView.setAdapter(mAdapter);
//
//        }catch (Exception e){
//
//            Log.e("Exception",e.toString());
//        }
//    }
      //----------For Search Functionality---------

        public static void Search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (mAdapter != null) mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }
    //---------End Search Functionality----------
}