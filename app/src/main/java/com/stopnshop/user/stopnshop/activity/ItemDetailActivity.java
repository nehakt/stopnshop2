package com.stopnshop.user.stopnshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.R;

/**
 * Created by neha on 22/9/17.
 */

public class ItemDetailActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtToolbar;
    ImageView imgProduct;
    TextView tvName;
    TextView tvDescription;
    TextView tvPrice;

    private GetProductListData1 datum;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itemdetail);

        Intent intent = getIntent();
        datum = (GetProductListData1) intent.getSerializableExtra("datum");//---intent from ItemsAdapter

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtToolbar = (TextView) findViewById(R.id.toolbar_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtToolbar.setText("Item Details");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imgProduct = (ImageView) findViewById(R.id.img_product);
        tvName = (TextView) findViewById(R.id.tv_itemName);
        tvDescription = (TextView) findViewById(R.id.tv_itemDescription);
        tvPrice = (TextView) findViewById(R.id.tv_itemPrice);

        if (intent != null) {
            if (datum != null) {

                tvName.setText("Name: "+datum.getName());
                tvDescription.setText("Description: "+datum.getDescription());
                tvPrice.setText("Price: $"+datum.getPrice1());
                Picasso.with(this)
                        .load(datum.getImage())
                        .error(R.drawable.icon1)
                        .placeholder(R.drawable.icon1)
                        .into(imgProduct);
            }
        }
    }
}