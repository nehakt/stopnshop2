package com.stopnshop.user.stopnshop.util;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.stopnshop.user.stopnshop.activity.PaymentOptionsActivity;
import com.stopnshop.user.stopnshop.activity.PlaceOrderActivity;
import com.stopnshop.user.stopnshop.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by neha on 6/7/17.
 */

public class ConfirmationActivity extends AppCompatActivity {

    Button btnFinish;
    String pickup;
    String order_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        btnFinish=(Button)findViewById(R.id.btn_finish);

        //Getting Intent
        Intent intent = getIntent();

        try {
            JSONObject jsonDetails = new JSONObject(intent.getStringExtra("PaymentDetails"));

            order_no=intent.getStringExtra("orderNo");

            //Displaying payment details
            showDetails(jsonDetails.getJSONObject("response"), intent.getStringExtra("PaymentAmount"));
        } catch (JSONException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickup= PaymentOptionsActivity.pickup_time;
//                order_no= PayatStoreActivity.order_no;
                Intent in=new Intent(ConfirmationActivity.this, PlaceOrderActivity.class);
                in.putExtra("pickuptime",pickup);
                in.putExtra("orderNo",order_no);
                startActivity(in);
            }
        });
    }

    private void showDetails(JSONObject jsonDetails, String paymentAmount) throws JSONException {
        //Views
        TextView textViewId = (TextView) findViewById(R.id.paymentId);
        TextView textViewStatus= (TextView) findViewById(R.id.paymentStatus);
        TextView textViewAmount = (TextView) findViewById(R.id.paymentAmount);

        //Showing the details from json object
        textViewId.setText(jsonDetails.getString("id"));
        textViewStatus.setText(jsonDetails.getString("state"));
        textViewAmount.setText(paymentAmount+" USD");
    }
}
