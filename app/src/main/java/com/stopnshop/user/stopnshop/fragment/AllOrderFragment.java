package com.stopnshop.user.stopnshop.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.stopnshop.user.stopnshop.adapter.MyAllOrderAdapter;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.model.orderList.Datum;
import com.stopnshop.user.stopnshop.model.orderList.OrderListData;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AllOrderFragment extends Fragment {

    RecyclerView recyclerView;
    MyAllOrderAdapter mAdapter;

    Activity context;
    private String userid;
    private Login_AllData logindata;

    List<Datum>orderList;

//    public static AllOrderFragment allOrderFragment;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context= (Activity) context;
//        allOrderFragment=this;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_order, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_myallorder);
        LinearLayoutManager layoutManager=new LinearLayoutManager(context);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);

        logindata= Constant.getlogindata(context);

        if (logindata!=null){

            userid= logindata.getId();
        }

       return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getOrderList();
    }

    private void getOrderList(){
        Data data=new Data();
        data.setUserId(userid);

        ServerRequestApi server_request=new ServerRequestApi("user_orderList",data);
        Gson gson=new Gson();
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        UtilView.showLogCat("OrderList",gsonString);
        callOrderListQuery(gsonString);
    }

    private void callOrderListQuery(final String gsonString) {

        ProgressDialog progressbar = UtilView.showProgressDialog(context);
        AQuery query=new AQuery(context);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);
      //Intent intent=new Intent(context, OrderSummryActivity.class);
//                        startActivity(intent);
        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("orderListResult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            logindata=Constant.getlogindata(getActivity());
//                            FavouriteOrderFragment.getFavoriteItemList(logindata,context);

                            Gson gson=new Gson();
                            OrderListData orderData=gson.fromJson(json.toString(),OrderListData.class);
                            orderList=orderData.getData();

                            List<Datum>orderDatumList=new ArrayList<Datum>();
                            for(Datum datum:orderList){
                                if (datum.getOrder_status()!=null && datum.getOrder_status().equals("0")
                                        ||datum.getOrder_status().equals("1")
                                        ||datum.getOrder_status().equals("2")
                                        ||datum.getOrder_status().equals("3")) {

                                    orderDatumList.add(datum);
                                }
                            }

                            mAdapter=new MyAllOrderAdapter(context,orderDatumList);
                            recyclerView.setAdapter(mAdapter);

                        } else
                        {
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

}