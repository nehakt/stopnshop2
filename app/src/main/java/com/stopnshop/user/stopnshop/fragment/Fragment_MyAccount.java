package com.stopnshop.user.stopnshop.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stopnshop.user.stopnshop.View.LoginActivity;
import com.stopnshop.user.stopnshop.View.SignUpActivity;
import com.stopnshop.user.stopnshop.activity.ChangePasswordActivity;
import com.stopnshop.user.stopnshop.activity.EditProfile_Activity;
import com.stopnshop.user.stopnshop.activity.FavoriteListActivity;
import com.stopnshop.user.stopnshop.activity.MyCartActivity;
import com.stopnshop.user.stopnshop.activity.NotificationActivity;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Fragment_MyAccount extends Fragment implements View.OnClickListener {

    Button btnUpdate;
    Button signin_btn, signup_btn;
    private String skip;
    LinearLayout profilelayout, sighnuplayout;
    EditText firstext_txt, lastname_txt, phone_edt, name_txt, dob_edt,edChangePassword;

    private String flag;

    Activity context;
    Intent intent;

    Cart cart;
    LayerDrawable cartmenuIcon;

    private Login_AllData logindata;
    private String userid="";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (Activity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_account, container, false);

        setHasOptionsMenu(true);

        cart= CartHelper.getCart();

        flag=SharedPreferenceHelper.getSharedPreferenceString(context,Constant.skip,"");

//        Bundle bundle = getArguments();

//        if (bundle != null) {
//
//            flag = bundle.getString("flag");
//        }
//
//        if (flag != null && flag.equals("skip")) {
//
//        } else {
//        }

        btnUpdate = (Button) v.findViewById(R.id.btn_updateprofile);
        signin_btn = (Button) v.findViewById(R.id.signin_btn);
        signup_btn = (Button) v.findViewById(R.id.signup_btn);
        profilelayout = (LinearLayout) v.findViewById(R.id.profilelayout);
        sighnuplayout = (LinearLayout) v.findViewById(R.id.sighnuplayout);

        firstext_txt = (EditText) v.findViewById(R.id.firstext_txt);
        lastname_txt = (EditText) v.findViewById(R.id.lastname_txt);
//        year_type = (EditText) v.findViewById(R.id.year_type);
//        month_type = (EditText) v.findViewById(R.id.month_type);
        name_txt = (EditText) v.findViewById(R.id.name_txt);
        phone_edt = (EditText) v.findViewById(R.id.phone_edt);
        dob_edt = (EditText) v.findViewById(R.id.dob_edt);
        edChangePassword=(EditText)v.findViewById(R.id.ed_changePassword);

        skip = SharedPreferenceHelper.getSharedPreferenceString(getActivity(), Constant.skip, "");


        if (skip != null && skip.equalsIgnoreCase("skip")) {

            sighnuplayout.setVisibility(View.VISIBLE);
            profilelayout.setVisibility(View.GONE);

        } else {

            logindata = Constant.getlogindata(getActivity());

            profilelayout.setVisibility(View.VISIBLE);
            sighnuplayout.setVisibility(View.GONE);

            firstext_txt.setText(logindata.getName());
            lastname_txt.setText(logindata.getLastName());
            phone_edt.setText(logindata.getContact());
            //year_type.setText(logindata.getName());
            name_txt.setText(logindata.getEmail());

            String dob = logindata.getDob();
            dob_edt.setText(dob);

//            try {
//
//                String[] filterage = dob.split("-");
//                String month = filterage[0];
//                String date = filterage[1];
//                String year = filterage[2];
//
//                year_type.setText(year);
//                month_type.setText(month);
//                date_edt.setText(date);
//
//            } catch (Exception e) {
//
//            }
            //use in edit profile save btn response
//            Login_AllData login_allData= new Login_AllData();
//
//            login_allData.setEmail("");
//
//            Constant.saveLogindata(getActivity(),login_allData);

            //  month_type.setText(logindata.ge());
        }

        signin_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });

        signup_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), SignUpActivity.class);
                startActivity(intent);
            }
        });

        btnUpdate.setOnClickListener(this);
        edChangePassword.setOnClickListener(this);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.startinfo_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem itemlogout=menu.findItem(R.id.logout);
//        MenuItem itemlogin=menu.findItem(R.id.login);
        MenuItem itemnotification=menu.findItem(R.id.notification_bttn);
        MenuItem itemFav=menu.findItem(R.id.favorite_list);

        if (flag!=null&&flag.equals("skip")) {
            itemlogout.setTitle("Login");
            itemnotification.setVisible(false);
            itemFav.setVisible(false);
        }else {
            itemlogout.setTitle("Logout");
            itemnotification.setVisible(true);
            itemFav.setVisible(true);
        }

        MenuItem item = menu.findItem(R.id.search_button);
        item.setVisible(false);
        MenuItem itemCart = menu.findItem(R.id.shopping_bttn);
        cartmenuIcon = (LayerDrawable) itemCart.getIcon();
        showBadgeCount();
        //UtilView.setBadgeCount(context, cartmenuIcon, "" + Cart.mapSize);
    }

    private void showBadgeCount() {

        Gson gson = new Gson();
        Type type = new TypeToken<List<GetProductListData1>>(){}.getType();

        String data = SharedPreferenceHelper.getSharedPreferenceString(context,"cartdata","");

        List<GetProductListData1> list = gson.fromJson(data,type);

        int quantity = 0;

        if(list!=null && list.size()>0) {

//            for (int i = 0; i < list.size(); i++) {
//
//                quantity = quantity + list.get(i).getQuantity();
//            }

            UtilView.setBadgeCount(context, cartmenuIcon, "" + list.size());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent;
        switch (item.getItemId()) {
            case R.id.notification_bttn:
                intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;

            case R.id.shopping_bttn:
                intent = new Intent(getActivity(), MyCartActivity.class);
                startActivity(intent);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;

            case R.id.logout:

                if (flag!=null&&flag.equals("skip")) {

                    intent = new Intent(context, LoginActivity.class);
                    context.startActivity(intent);
                }
                else {
                    getLogout();

                }

                //------------clear the shared perference data(logindata and userid)------------
              //  SharedPreferenceHelper.setSharedPreferenceString(context, Constant.usertlogindata, "");
//                SharedPreferenceHelper.setSharedPreferenceString(context, "iserid", "");
//                intent = new Intent(context, LoginActivity.class);
//                startActivity(intent);
                return true;

            case R.id.favorite_list:
                intent = new Intent(getActivity(), FavoriteListActivity.class);
                startActivity(intent);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getLogout(){
        logindata= Constant.getlogindata(context);

        if (logindata!=null){

            userid= logindata.getId();
        }

        Data data=new Data();
        data.setUserId(userid);
        data.setUser_type(Constant.user_type1);

        ServerRequestApi server_request=new ServerRequestApi("logoutUser",data);
        Gson gson=new Gson();
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        UtilView.showLogCat("logoutReq",gsonString);

        ProgressDialog progressbar = UtilView.showProgressDialog(context);
        AQuery query=new AQuery(context);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);
        //Intent intent=new Intent(context, OrderSummryActivity.class);
//                        startActivity(intent);
        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("orderListResult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            SharedPreferenceHelper.setSharedPreferenceString(context, "iserid", "");
                            CartHelper.getCart().clear();
                            Intent intent = new Intent(context,LoginActivity.class);
                            startActivity(intent);
                            context.finish();
                        } else
                        {
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_updateprofile:

                intent = new Intent(context, EditProfile_Activity.class);
                startActivity(intent);

                break;
            case R.id.ed_changePassword:
                intent=new Intent(context, ChangePasswordActivity.class);
                startActivity(intent);
                break;
        }
    }

     @Override
    public void onResume() {
        super.onResume();
        if (skip != null && skip.equals("skip")) {

            sighnuplayout.setVisibility(View.VISIBLE);

        } else {

            logindata = Constant.getlogindata(getActivity());

            profilelayout.setVisibility(View.VISIBLE);
            firstext_txt.setText(logindata.getName());
            lastname_txt.setText(logindata.getLastName());
            phone_edt.setText(logindata.getContact());
            //year_type.setText(logindata.getName());
            name_txt.setText(logindata.getEmail());

            String dob = logindata.getDob();

            dob_edt.setText(dob);
//            try {
//
//                String[] filterage = dob.split("-");
//                String month = filterage[0];
//                String date = filterage[1];
//                String year = filterage[2];
//
//                year_type.setText(year);
//                month_type.setText(month);
//                date_edt.setText(date);
//
//            } catch (Exception e) {
//
//            }
        }
    }
}