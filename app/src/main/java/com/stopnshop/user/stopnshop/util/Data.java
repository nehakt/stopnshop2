package com.stopnshop.user.stopnshop.util;

import com.stopnshop.user.stopnshop.model.productlist_pojo.CartProduct;

import java.util.List;

/**
 * Created by saurabh on 04-Sep-17.
 */

public class Data
{
    public  String name;
    public String lastName;
    public String email;
    public String dob;
    public String password;
    public String fname;
    public String lname;
    public String contactno;
    public String ubs_code;
    String userId,productId,contact,product_image,product_name,product_details,otp;
    public String total_item;
    public String total_amount;
    public List<CartProduct> product_list;
    public String pickup_time;
    public String cash_paying;
    public String order_number;
    public String orderId;
    public String favoriteStatus;
    public String userName;
    public String notification;
    public String notification_type;
    public String order_status;
    public String device_type;
    public String device_token;
    public String user_type;
    public String customerpay_amount;
    public String paymentType;

    public Data(String name, String lastName, String email, String dob, String password) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.dob = dob;
        this.password = password;
    }

    public Data(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Data(){

    }

    public String getUbs_code() {
        return ubs_code;
    }

    public void setUbs_code(String ubs_code) {
        this.ubs_code = ubs_code;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getProduct_image() {
        return product_image;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_details() {
        return product_details;
    }

    public void setProduct_details(String product_details) {
        this.product_details = product_details;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTotal_item() {
        return total_item;
    }

    public void setTotal_item(String total_item) {
        this.total_item = total_item;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public List<CartProduct> getProduct_list() {
        return product_list;
    }

    public void setProduct_list(List<CartProduct> product_list) {
        this.product_list = product_list;
    }

    public String getPickup_time() {
        return pickup_time;
    }

    public void setPickup_time(String pickup_time) {
        this.pickup_time = pickup_time;
    }

    public String getCash_paying() {
        return cash_paying;
    }

    public void setCash_paying(String cash_paying) {
        this.cash_paying = cash_paying;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getFavoriteStatus() {
        return favoriteStatus;
    }

    public void setFavoriteStatus(String favoriteStatus) {
        this.favoriteStatus = favoriteStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(String notification_type) {
        this.notification_type = notification_type;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getCustomerpay_amount() {
        return customerpay_amount;
    }

    public void setCustomerpay_amount(String customerpay_amount) {
        this.customerpay_amount = customerpay_amount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
}