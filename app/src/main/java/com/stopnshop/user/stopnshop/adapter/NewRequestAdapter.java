package com.stopnshop.user.stopnshop.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stopnshop.user.stopnshop.activity.NewProductDetail_Activity;
import com.stopnshop.user.stopnshop.model.productlist_pojo.Datum;
import com.stopnshop.user.stopnshop.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by saurabh on 26-Aug-17.
 */

public class NewRequestAdapter extends RecyclerView.Adapter<NewRequestAdapter.MyViewHolder>
{

    private static String date;
    private static String convertedTime;
    private List<Object> imgList;
    Activity activity;
    List<Datum> productlistdata;

   // 0 not available
//    public NewRequestAdapter(Activity context, List<Object> imgList) {
//        this.imgList=imgList;
//        this.context=context;
//    }

    public NewRequestAdapter(Activity activity, List<Datum> productlistsata) {

        this.productlistdata=productlistsata;
        this.activity=activity;
    }
    @Override
    public NewRequestAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_newproduct_request, parent, false);

        return new NewRequestAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NewRequestAdapter.MyViewHolder holder, final int position) {

        holder.product_name.setText(productlistdata.get(position).getName());
        holder.product_datetime.setText(parseDateToddMMyyyy(productlistdata.get(position).getAddedDate()));
        holder.product_notification.setText(productlistdata.get(position).getName());
        //holder.product_image.setText(productlistsata.get(position).getName());

        if (productlistdata.get(position).getStatus()!=null&&productlistdata.get(position).getStatus().equals("0")){

            holder.product_notification.setText("Request Pending");
            holder.product_notification.setTextColor(Color.RED);

        }else {

            holder.product_notification.setText("Available Now");
            holder.product_notification.setTextColor(Color.GREEN);

        }
        Glide.with(activity)
                .load(productlistdata.get(position).getProductImage())
                .error(R.drawable.logonew)
                .into(holder.product_image);

        // holder.product_datetime.setText("Sat 26 August 2017,2 PM");
       // holder.product_image.setImageResource((Integer) imgList.get(position));

        holder.llNewAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(activity,NewProductDetail_Activity.class);
                intent.putExtra("productlist",productlistdata.get(position));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return productlistdata.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView product_name, product_datetime, product_notification;
        public ImageView product_image;
        public LinearLayout llNewAddProduct;

        public MyViewHolder(View view) {
            super(view);
            product_name = (TextView) view.findViewById(R.id.product_name);
            product_datetime = (TextView) view.findViewById(R.id.product_datetime);
            product_notification = (TextView) view.findViewById(R.id.product_notification);
            product_image = (ImageView) view.findViewById(R.id.product_image);
            llNewAddProduct=(LinearLayout)view.findViewById(R.id.ll_newAddProduct);
        }
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "MMM dd, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}