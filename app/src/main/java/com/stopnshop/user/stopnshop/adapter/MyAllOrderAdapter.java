package com.stopnshop.user.stopnshop.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.stopnshop.user.stopnshop.activity.OrderSummryActivity;
import com.stopnshop.user.stopnshop.fragment.FavouriteOrderFragment;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.model.orderList.Datum;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.Msg;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by saurabh on 23-Aug-17.
 */

public class MyAllOrderAdapter extends RecyclerView.Adapter<MyAllOrderAdapter.MyViewHolder>
{

    private List<Datum> orderList;
    static Activity context;

    private static Login_AllData logindata;
    private static String userid;
    private String orderid;

    public MyAllOrderAdapter(Activity context, List<Datum> orderList) {
        this.orderList = orderList;
        this.context = context;
    }
    @Override
    public MyAllOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_myallorder_list, parent, false);

        logindata= Constant.getlogindata(context);

        if (logindata!=null){

            userid= logindata.getId();
        }

        return new MyAllOrderAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final MyAllOrderAdapter.MyViewHolder holder, final int position) {

            final Datum data=orderList.get(position);
            if (data.getOrderId()!=null) {
                holder.order_no.setText("OrderId: "+data.getOrderId());
            }
            if (data.getAddedDate()!=null){
                holder.orderdate_time.setText(data.getAddedDate());
            }
            if (data.getPickuptime()!=null){
                holder.pickup_timedate.setText("Pickup Time: "+data.getPickuptime());
            }
            if (data.getTotolItem()!=null){
                holder.total_items.setText("Total Items: "+data.getTotolItem());
            }
            if (data.getAmout()!=null){
                holder.total_amount.setText("Total Amount: $"+data.getAmout());
            }

//        if (data.getOrder_status().equals("0")){
//            holder.tvNotificationText.setText("In ");
//            holder.tvNotificationText.setTextColor(Color.WHITE);
//            holder.tvNotificationText.setBackgroundColor(Color.RED);
//        }
        if (data.getOrder_status().equals("0")|| data.getOrder_status().equals("1"))
        {
            holder.tvNotificationText.setText("In Progress");
            holder.tvNotificationText.setTextColor(Color.GREEN);
            holder.tvNotificationText.setBackgroundColor(0x00000000);
        }
        else if (data.getOrder_status().equals("2"))
        {
            holder.tvNotificationText.setText("Order Picked Up");
            holder.tvNotificationText.setTextColor(Color.GREEN);
            holder.tvNotificationText.setBackgroundColor(0x00000000);
        }
        else if (data.getOrder_status().equals("3")){
            holder.tvNotificationText.setText("Cancelled");
            holder.tvNotificationText.setTextColor(Color.RED);
            holder.tvNotificationText.setBackgroundColor(0x00000000);
        }

        //------first set fav & unfav in the adapter-------

        if (data.getFavorite()!=null && data.getFavorite().equals("0")) {
            holder.imgFav.setImageResource(R.drawable.favorite_white_a);
        }else if (data.getFavorite()!=null && data.getFavorite().equals("1")){
            holder.imgFav.setImageResource(R.drawable.favorite_white_b);
        }

        holder.lLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, OrderSummryActivity.class);
                intent.putExtra("orderList", orderList.get(position));
                context.startActivity(intent);
            }
        });

         holder.imgFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (data.getFavorite()!=null && data.getFavorite().equals("0")) {
                    if (InternetConnection.isNetworkAvailable(context)) {

                        getAddRemoveFavorite(data,holder);

                    } else {

                        Msg.t(context, "Please Check Internet Connection");
                    }
                }else if (data.getFavorite()!=null && data.getFavorite().equals("1")){

                    if (InternetConnection.isNetworkAvailable(context)) {

                        getAddRemoveFavorite(data,holder);

                    } else {

                        Msg.t(context, "Please Check Internet Connection");
                    }
                }
            }
        });
    }

    public static void getAddRemoveFavorite(final Datum data, final MyViewHolder holder){

        AQuery query = new AQuery(context);
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        Map<String, Object> params = null;
        StringEntity sEntity = null;

        Data data1=new Data();
        data1.setUserId(userid);
        data1.setOrderId(data.getOrderId());
        //0 means data is unfavourite
        //1 means data is favourite

        if(data.getFavorite().equals("1")){
           data1.setFavoriteStatus("0");
        }else if(data.getFavorite().equals("0")){
            data1.setFavoriteStatus("1");
        }
        ServerRequestApi server_request=new ServerRequestApi("favoriteOrder",data1);
        Gson gson=new Gson();
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        Log.e("favauriteorderrequest",gsonString.toString());
        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);

        query.progress(progressDialog).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                Log.e("@flow", json.toString());

                if (status.getCode() == 200) {

                    Log.e("@flow", json.toString());

                    try {

                        if (json.getInt("msg_code") == 1) {

                            if(data.getFavorite().equals("1")){
                                holder.imgFav.setImageResource(R.drawable.favorite_white_a);
                                data.setFavorite("0");
                                FavouriteOrderFragment.getFavoriteItemList(logindata,context);

                            }else if(data.getFavorite().equals("0")){
                                holder.imgFav.setImageResource(R.drawable.favorite_white_b);
                                data.setFavorite("1");
                                FavouriteOrderFragment.getFavoriteItemList(logindata,context);
                            }
                        } else
                        {
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
   }

    @Override
    public int getItemCount()

    {
      return orderList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView order_no, orderdate_time, pickup_timedate, total_items, total_amount,notification_txt;
        public ImageView imgFav;
        public LinearLayout lLayout;
        public TextView tvNotificationText;

        public MyViewHolder(View view) {
            super(view);
            order_no = (TextView) view.findViewById(R.id.order_name);
            orderdate_time = (TextView) view.findViewById(R.id.orderdate_time);
            pickup_timedate = (TextView) view.findViewById(R.id.pickup_timedate);
            total_items = (TextView) view.findViewById(R.id.total_items);
            total_amount = (TextView) view.findViewById(R.id.total_amount);
            notification_txt = (TextView) view.findViewById(R.id.notification_txt);
            imgFav=(ImageView)view.findViewById(R.id.profile_image);
            lLayout=(LinearLayout)view.findViewById(R.id.linear_layout);
            tvNotificationText=(TextView)view.findViewById(R.id.notification_txt);
        }
    }
}