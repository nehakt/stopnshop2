
package com.stopnshop.user.stopnshop.model.logindata_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class LoginClassPojo implements Serializable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("msg_code")
    @Expose
    private int msgCode;
    @SerializedName("data")
    @Expose
    private List<Login_AllData> data = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public LoginClassPojo() {
    }

    /**
     * 
     * @param message
     * @param msgCode
     * @param data
     */
    public LoginClassPojo(String message, int msgCode, List<Login_AllData> data) {
        super();
        this.message = message;
        this.msgCode = msgCode;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(int msgCode) {
        this.msgCode = msgCode;
    }

    public List<Login_AllData> getData() {
        return data;
    }

    public void setData(List<Login_AllData> data) {
        this.data = data;
    }

}
