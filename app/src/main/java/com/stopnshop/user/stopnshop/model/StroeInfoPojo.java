package com.stopnshop.user.stopnshop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by saurabh on 12-Sep-17.
 */

public class StroeInfoPojo {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("msg_code")
    @Expose
    private int msgCode;
    @SerializedName("data")
    @Expose
    private List<StoreInfoData> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(int msgCode) {
        this.msgCode = msgCode;
    }

    public List<StoreInfoData> getData() {
        return data;
    }

    public void setData(List<StoreInfoData> data) {
        this.data = data;
    }

}
