package com.stopnshop.user.stopnshop.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.R;

import java.util.List;

/**
 * Created by saurabh on 24-Aug-17.
 */

public class CandiesAdapter extends RecyclerView.Adapter<CandiesAdapter.MyViewHolder>

{
    List<GetProductListData1> productdata;
    Activity context;

    public CandiesAdapter(Activity context, List<GetProductListData1> productdata) {
        this.productdata=productdata;
        this.context=context;
    }

    @Override
    public CandiesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.candies_list_row, parent, false);

        return new CandiesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CandiesAdapter.MyViewHolder holder, int position) {

        GetProductListData1 data = productdata.get(position);


        holder.itemName.setText(data.getName());
        holder.price.setText(data.getPrice1());
        Picasso.with(context).load(data.getImage()).error(R.drawable.logo).placeholder(R.drawable.logo).into(holder.item);

//        for (int i=0;i<data2.size();i++)
//        {
//            holder.itemName.setText( data2.get(i).getName());
//            holder.price.setText(data2.get(i).getPrice());
//            Picasso.with(context).load(data2.get(i).getImage()).error(R.drawable.logo).placeholder(R.drawable.logo).into(holder.item);
//
//        }
    }

    @Override
    public int getItemCount()
    {
        return productdata.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView price, itemName;
        public ImageView item;

        public MyViewHolder(View view) {
            super(view);
            price = (TextView) view.findViewById(R.id.price);
            itemName = (TextView) view.findViewById(R.id.itemName);
            item = (ImageView) view.findViewById(R.id.item);
        }
    }
}