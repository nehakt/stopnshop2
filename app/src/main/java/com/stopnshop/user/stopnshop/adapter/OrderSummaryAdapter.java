package com.stopnshop.user.stopnshop.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stopnshop.user.stopnshop.model.orderList.ProductDatum;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.UtilView;


import java.util.List;

public class OrderSummaryAdapter  extends RecyclerView.Adapter<OrderSummaryAdapter.MyViewHolder>
{
    private List<ProductDatum> productList;
    Activity context;

    public OrderSummaryAdapter(Activity activity, List<ProductDatum> productData) {
        this.context=activity;
        this.productList=productData;
    }

    @Override
    public OrderSummaryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_ordersummary, parent, false);

        return new OrderSummaryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrderSummaryAdapter.MyViewHolder holder, int position) {

        ProductDatum datum=productList.get(position);

        if (datum!=null) {

            if (datum.getProductName() != null) {
                holder.tvProductName.setText(datum.getProductName());
            }
            if (datum.getProductPrice() != null) {
                holder.tvProductPrice.setText("$"+datum.getProductPrice());
            }
            if (datum.getTotalItems()!=null){
                holder.tvProductQty.setText("Qty: "+datum.getTotalItems());
            }
            if (datum.getProductImage() != null) {
                Picasso.with(context)
                        .load(datum.getProductImage())
                        .error(R.drawable.icon)
                        .placeholder(R.drawable.icon)
                        .into(holder.product_image);
            }
        }else{
            UtilView.showToast(context,"No Product Found!");
        }
    }
    @Override
    public int getItemCount()

    {
        return productList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName,tvProductType,tvProductDecription,tvProductPrice,tvProductQty;
        public ImageView product_image;

        public MyViewHolder(View view) {
            super(view);

            tvProductName = (TextView) view.findViewById(R.id.tv_productName);
//            tvProductType = (TextView) view.findViewById(R.id.tv_productType);
            tvProductDecription = (TextView) view.findViewById(R.id.tv_productDescription);
            tvProductPrice = (TextView) view.findViewById(R.id.tv_productPrice);
            product_image = (ImageView) view.findViewById(R.id.img_productImage);
            tvProductQty=(TextView)view.findViewById(R.id.tv_productQty);
        }
    }
}