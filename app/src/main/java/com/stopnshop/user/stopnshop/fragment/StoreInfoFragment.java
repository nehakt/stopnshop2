package com.stopnshop.user.stopnshop.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stopnshop.user.stopnshop.View.LoginActivity;
import com.stopnshop.user.stopnshop.activity.FavoriteListActivity;
import com.stopnshop.user.stopnshop.activity.MyCartActivity;
import com.stopnshop.user.stopnshop.activity.NotificationActivity;
import com.stopnshop.user.stopnshop.activity.StoreInfoFullImageActivity;
import com.stopnshop.user.stopnshop.adapter.StarInfoAdapter;
import com.stopnshop.user.stopnshop.listener.OnItemTouchListener;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.StoreInfoData;
import com.stopnshop.user.stopnshop.model.StroeInfoPojo;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class StoreInfoFragment extends Fragment implements OnMapReadyCallback {

    private List<Object> imgList;

    RecyclerView recyclerView;
    StarInfoAdapter mAdapter;
    MapView mMapView;
    private GoogleMap mMap;
    private GoogleMap googleMap;
    boolean latitude,longitude;
    ArrayList<String> shopimage = new ArrayList<>();

    AQuery aq;
    Activity context;
    public TextView restaurant_txt, name_txt1,name_txt2,mobilenum1,mobilenum2,email_txt;
    private double mLat=0,mLon=0;
    private List<StoreInfoData> shopdata;

    OnItemTouchListener itemTouchListener;

    Cart cart;
    LayerDrawable cartmenuIcon;

    private Login_AllData logindata;
    private String userid="";
    private String flag;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (Activity) context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_store_info, container, false);
        restaurant_txt = (TextView) view.findViewById(R.id.restaurant_txt);
        name_txt1 = (TextView) view.findViewById(R.id.name_txt1);
        name_txt2 = (TextView) view.findViewById(R.id.name_txt2);
        mobilenum1 = (TextView) view.findViewById(R.id.mobilenum1);
        mobilenum2 = (TextView) view.findViewById(R.id.mobilenum2);
        email_txt = (TextView) view.findViewById(R.id.email_txt);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_storeinfo);
        // final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));

        setHasOptionsMenu(true);

        cart= CartHelper.getCart();

        if (InternetConnection.isNetworkAvailable(getActivity())){

            ApiCaalling();

        }else {

        }


//        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
//                .findFragmentById(R.id.main_branch_map);
//        mapFragment.getMapAsync(this);




//        //   recyclerView.setLayoutManager(linearLayoutManager);
//
//        imgList=new ArrayList<>();
//
//        imgList.add(R.drawable.saasasas);
//        imgList.add(R.drawable.images2);
//        imgList.add(R.drawable.images5);
//        imgList.add(R.drawable.images5);
//        imgList.add(R.drawable.images5);
//        imgList.add(R.drawable.images5);

//        recyclerView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent = new Intent(context, StoreInfoFullImageActivity.class);
//                intent.putExtra("fullimg",shopimage);
//                context.startActivity(intent);
//            }
//        });

        itemTouchListener=new OnItemTouchListener() {
            @Override
            public void onCardViewTap(View view, int position) {

                Intent intent = new Intent(context, StoreInfoFullImageActivity.class);
                intent.putExtra("fullimg",shopimage);
                intent.putExtra("position",position);//to send current posion to StroreInfoFullActivity
                context.startActivity(intent);
            }

            @Override
            public void onSwipeViewTap(View view, int position) {

            }
        };


        return view;

    }

    private void ApiCaalling() {
        aq = new AQuery(context);
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        Map<String, Object> params = null;
        StringEntity sEntity = null;

        Data data=new Data();

        ServerRequestApi pozo = new ServerRequestApi("storeInformation",data);
        final Gson gson = new Gson();
        String s = gson.toJson(pozo, ServerRequestApi.class);
        Log.e("@flow", s);

        try {
            sEntity = new StringEntity(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(aq.POST_ENTITY, sEntity);

        aq.progress(progressDialog).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                Log.e("@flow", json.toString());

                if (status.getCode() == 200) {

                    Log.e("@flow", json.toString());

                    try {

                        if (json.getInt("msg_code") == 1) {
                           Gson gson = new Gson();
                            StroeInfoPojo datum=    gson.fromJson(json.toString(),StroeInfoPojo.class);

                         //   restaurant_txt, name_txt1,name_txt2,mobilenum1,mobilenum2,email_txt;
                            shopdata = datum.getData();

                            if (shopdata!=null){

                                restaurant_txt.setText(shopdata.get(0).getName());
                                name_txt1.setText(shopdata.get(0).getAddress());
                                name_txt2.setText(shopdata.get(0).getAddress());
                                mobilenum1.setText(shopdata.get(0).getConatact());
                                mobilenum2.setText(shopdata.get(0).getConatact());
                                email_txt.setText(shopdata.get(0).getEmail());

                                mLat = Double.parseDouble(shopdata.get(0).getLatitude().trim());
                                mLon = Double.parseDouble(shopdata.get(0).getLongitude().trim());

                                if (shopdata.get(0).getImage() != null && !shopdata.get(0).getImage().trim().isEmpty()) {
                                    shopimage.add(shopdata.get(0).getUrl() + "" + shopdata.get(0).getImage());

                                } else {

                                }

                                if (shopdata.get(0).getImage2() != null && !shopdata.get(0).getImage2().trim().isEmpty()) {
                                    shopimage.add(shopdata.get(0).getUrl() + "" + shopdata.get(0).getImage2());

                                } else {

                                }
                                if (shopdata.get(0).getImage3() != null && !shopdata.get(0).getImage3().trim().isEmpty()) {
                                    shopimage.add(shopdata.get(0).getUrl() + "" + shopdata.get(0).getImage3());

                                } else {
                                }

                                if (shopdata.get(0).getImage4() != null && !shopdata.get(0).getImage4().trim().isEmpty()) {
                                    shopimage.add(shopdata.get(0).getUrl() + "" + shopdata.get(0).getImage4());

                                } else {

                                }
                                if (shopdata.get(0).getImage5() != null && !shopdata.get(0).getImage5().trim().isEmpty()) {
                                    shopimage.add(shopdata.get(0).getUrl() + "" + shopdata.get(0).getImage5());

                                } else {

                                }
                                if (shopdata.get(0).getImage6() != null && !shopdata.get(0).getImage6().trim().isEmpty()) {
                                    shopimage.add(shopdata.get(0).getUrl() + "" + shopdata.get(0).getImage());

                                } else {

                                }
                                SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.main_branch_map);
                                mapFragment.getMapAsync(StoreInfoFragment.this);
                            }

//                           latitude= Boolean.parseBoolean(shopdata.get(0).getLatitude());
//                           longitude= Boolean.parseBoolean(shopdata.get(0).getLongitude());

                            mAdapter=new StarInfoAdapter(context,shopimage,itemTouchListener);
                            recyclerView.setAdapter(mAdapter);
                            recyclerView.setScrollingTouchSlop(0);
                        } else {

                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }
//    42.364270
//
//            -72.538948
//
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
       // LatLng UCA = new LatLng(42.364270, -72.538948);

        String lat="42.364270";
        String lng="-72.538948";

//        mLat = Double.parseDouble(lat);
//        mLon = Double.parseDouble(lng);

        if (mLat==0&&mLon==0){

        }else {

            LatLng UCA = new LatLng(mLat, mLon);
            // LatLng =shopdata.get(0).getLongitude();
            // LatLng UCA=new LatLng(shopdata.get(0).getLongitude(),shopdata.get(0).getLongitude());
            mMap.addMarker(new MarkerOptions().position(UCA).title("StopNShop")).showInfoWindow();

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(UCA,17));

            mLat=0;
            mLon=0;
        }
        //  double mLon = LONTITUDE;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.startinfo_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem itemlogout=menu.findItem(R.id.logout);
//        MenuItem itemlogin=menu.findItem(R.id.login);
        MenuItem itemnotification=menu.findItem(R.id.notification_bttn);
        MenuItem itemFav=menu.findItem(R.id.favorite_list);

        MenuItem itemSearch = menu.findItem(R.id.search_button);
        MenuItem itemCart = menu.findItem(R.id.shopping_bttn);

        flag=SharedPreferenceHelper.getSharedPreferenceString(context,Constant.skip,"");

        if (flag!=null&&flag.equals("skip")) {
            itemlogout.setTitle("Login");
            itemnotification.setVisible(false);
            itemFav.setVisible(false);
        }else {
            itemlogout.setTitle("Logout");
            itemnotification.setVisible(true);
            itemFav.setVisible(true);
        }


        cartmenuIcon = (LayerDrawable) itemCart.getIcon();

        //-----Cart.mapSize shows total no of items in cart---------

        showBadgeCount();
        //UtilView.setBadgeCount(context, cartmenuIcon, "" + Cart.mapSize);

        itemSearch.setVisible(false);
            }

    private void showBadgeCount() {

        Gson gson = new Gson();
        Type type = new TypeToken<List<GetProductListData1>>(){}.getType();

        String data = SharedPreferenceHelper.getSharedPreferenceString(context,"cartdata","");

        List<GetProductListData1> list = gson.fromJson(data,type);

        int quantity = 0;

        if(list!=null && list.size()>0) {

//            for (int i = 0; i < list.size(); i++) {
//
//                quantity = quantity + list.get(i).getQuantity();
//            }

            UtilView.setBadgeCount(context, cartmenuIcon, "" + list.size());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.notification_bttn:
                Intent intent=new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;

            case R.id.shopping_bttn:
                Intent intent1=new Intent(getActivity(), MyCartActivity.class);
                startActivity(intent1);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;

            case R.id.favorite_list:

               // Toast.makeText(getActivity(), "in progress", Toast.LENGTH_SHORT).show();
                 intent=new Intent(getActivity(), FavoriteListActivity.class);
                startActivity(intent);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;

            case R.id.logout:
                //------------clear the shared perference data(logindata and userid)------------
                //  SharedPreferenceHelper.setSharedPreferenceString(context, Constant.usertlogindata, "");
                if (flag!=null&&flag.equals("skip")) {

                    intent = new Intent(context, LoginActivity.class);
                    context.startActivity(intent);
                }
                else {
                    getLogout();
                }

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getLogout(){
        logindata= Constant.getlogindata(context);

        if (logindata!=null){

            userid= logindata.getId();

        }

        Data data=new Data();
        data.setUserId(userid);
        data.setUser_type(Constant.user_type1);

        ServerRequestApi server_request=new ServerRequestApi("logoutUser",data);
        Gson gson=new Gson();
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        UtilView.showLogCat("logoutReq",gsonString);

        ProgressDialog progressbar = UtilView.showProgressDialog(context);
        AQuery query=new AQuery(context);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);
        //Intent intent=new Intent(context, OrderSummryActivity.class);
//                        startActivity(intent);
        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("orderListResult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            SharedPreferenceHelper.setSharedPreferenceString(context, "iserid", "");
                            CartHelper.getCart().clear();
                            Intent intent = new Intent(context,LoginActivity.class);
                            startActivity(intent);
                            context.finish();
                        } else
                        {
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

}