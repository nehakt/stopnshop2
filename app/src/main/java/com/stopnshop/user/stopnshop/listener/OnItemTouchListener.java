package com.stopnshop.user.stopnshop.listener;

import android.view.View;

/**
 * Created by impetrosys on 24/1/17.
 */

public interface OnItemTouchListener {
    public void onCardViewTap(View view, int position);
    public void onSwipeViewTap(View view, int position);


}
