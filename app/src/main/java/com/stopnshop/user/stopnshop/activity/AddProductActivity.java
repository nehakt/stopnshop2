package com.stopnshop.user.stopnshop.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import com.stopnshop.user.stopnshop.R;

import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.Msg;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.yinglan.keyboard.HideUtil;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

public class AddProductActivity extends AppCompatActivity implements View.OnClickListener ,Validator.ValidationListener {


    Toolbar toolbar;
    TextView title_txt, save_txt;
    private Bitmap bm;
    private String encodedImage;

    private Validator validator;
    @NotEmpty
    private EditText product_name;
    @NotEmpty
    private EditText product_detail;
    @NotEmpty
    private EditText custmore_name;
    @NotEmpty
    private EditText ubscode_edt;

    ImageView imgAdd;

    String userName = "";

    private Bitmap bitmap;

    private EditText mobile_number;
    private String productnam, productdetails;
    private Login_AllData logindata;
    private AQuery aq;
    private String filePath;
    private String ubscode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        HideUtil.init(this);
        Nammu.init(AddProductActivity.this);
        validator = new Validator(this);
        validator.setValidationListener(this);

        initview();

        String flag = SharedPreferenceHelper.getSharedPreferenceString(AddProductActivity.this, Constant.skip, "");

        if (flag != null && flag.equals("skip")) {

        } else {
            logindata = Constant.getlogindata(AddProductActivity.this);
            custmore_name.setText(logindata.getName());
            mobile_number.setText(logindata.getContact());
        }
    }

    private void initview() {

        imgAdd = (ImageView) findViewById(R.id.img_add);
        save_txt = (TextView) findViewById(R.id.save_txt);
        custmore_name = (EditText) findViewById(R.id.custmore_name);
        mobile_number = (EditText) findViewById(R.id.mobile_number);
        product_name = (EditText) findViewById(R.id.product_name);
        product_detail = (EditText) findViewById(R.id.product_detail);
        ubscode_edt = (EditText) findViewById(R.id.ubscode_edt);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title_txt = (TextView) findViewById(R.id.title_txt);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title_txt.setText("Add Product Request");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        save_txt.setVisibility(View.VISIBLE);
        save_txt.setOnClickListener(this);
        imgAdd.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.save_txt:
                validator.validate();
                break;

            case R.id.img_add:
//                if (Build.VERSION.SDK_INT >= 23) {
//                    int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//                        Nammu.askForPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, new PermissionCallback() {
//                            @Override
//                            public void permissionGranted() {
//                                //Nothing, this sample saves to Public gallery so it needs permission
//                                Crop.pickImage(AddProductActivity.this);
//                            }
//
//                            @Override
//                            public void permissionRefused() {
//                                finish();
//                            }
//                        });
//                    } else {
//                        Crop.pickImage(AddProductActivity.this);
//                    }
//                } else {
//                    Crop.pickImage(AddProductActivity.this);
//                }

               setimage();
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {

        productnam = product_name.getText().toString().trim();
        productdetails = product_detail.getText().toString().trim();
        ubscode = ubscode_edt.getText().toString().trim();
        userName = custmore_name.getText().toString().trim();


        if (InternetConnection.isNetworkAvailable(this)) {
            productrequest();
        } else {
            Msg.t(this, "Please Check Internet Connection");
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void productrequest() {

        aq = new AQuery(this);
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        Map<String, Object> params = null;
        StringEntity sEntity = null;

        Data data = new Data();

        data.setProduct_name(productnam);
        data.setProduct_details(productdetails);
        data.setProduct_image(encodedImage);
        data.setUserId(logindata.getId());
        data.setUbs_code(ubscode);
        data.setUserName(userName);
        data.setNotification(userName + " sends the new " + productnam + " product request.");
        data.setNotification_type("newreq");

        ServerRequestApi pozo = new ServerRequestApi("addRequest", data);
        final Gson gson = new Gson();
        String s = gson.toJson(pozo, ServerRequestApi.class);
        Log.e("@flow", s);

        try {
            sEntity = new StringEntity(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(aq.POST_ENTITY, sEntity);

        aq.progress(progressDialog).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                Log.e("@flow", json.toString());

                if (status.getCode() == 200) {
                    Log.e("@flow", json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            NewAddActivity.newAddActivity.updateAdapter();

                            SweetAlertDialog sweetAlertDialog;
                            sweetAlertDialog = new SweetAlertDialog(AddProductActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                            sweetAlertDialog.setTitleText(productnam)
                                    .setContentText("Product added successfully")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            finish();
                                        }
                                    })
                                    .show();
                        } else {

                            Log.e("message", json.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    private void setimage() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ){

            Nammu.askForPermission(this, "android.permission.CAMERA", new PermissionCallback() {
                @Override
                public void permissionGranted() {

                     EasyImage.openChooserWithGallery(AddProductActivity.this, "choose photo", 1);
                    }
                    @Override
                    public void permissionRefused() {
                    }
                });

            } else {
                EasyImage.openChooserWithGallery(AddProductActivity.this, "choose photo", 1);
            }
        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
            }
            @Override
            public void onImagesPicked(List<File> imagesFiles, EasyImage.ImageSource source, int type) {
                //Handle the images

                filePath = imagesFiles.get(0).getPath();
                bm = BitmapFactory.decodeFile(filePath);
                imgAdd.setImageBitmap(bm);
//                bm = MediaStore.Images.Media.getBitmap(AddProductActivity.this.getContentResolver(), imagesFiles.get(0).getAbsoluteFile());
                bm = Bitmap.createScaledBitmap(bm, 300, 300, true);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
                byte[] byteArrayImage = baos.toByteArray();
                encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                // onPhotosReturned(imagesFiles);
//                imgAdd.setImageResource(bm);
            }
        });
    }
}