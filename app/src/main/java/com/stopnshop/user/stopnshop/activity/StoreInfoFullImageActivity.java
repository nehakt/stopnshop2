package com.stopnshop.user.stopnshop.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.stopnshop.user.stopnshop.adapter.FullImageAdapter;
import com.stopnshop.user.stopnshop.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by neha on 21/9/17.
 */

public class StoreInfoFullImageActivity extends Activity {

    @BindView(R.id.recycle_fullimg)
    RecyclerView recyclerViewFullImg;

    @BindView(R.id.img_close)
    ImageView imgClose;

    FullImageAdapter fullimgAdapter;

    Activity activity;

    private ArrayList<String> imagelist=new ArrayList<>();
    private int position;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullimage);

        ButterKnife.bind(this);

        //------img set as horizontally----------------
        LinearLayoutManager layoutManager=new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,true);
        recyclerViewFullImg.setLayoutManager(layoutManager);

        Intent intent=getIntent();
        if (intent!=null){

          imagelist= (ArrayList<String>) intent.getSerializableExtra("fullimg");
          position=intent.getIntExtra("position",0) ;//----0 is default value

           }

        fullimgAdapter=new FullImageAdapter(activity,imagelist,position);
        recyclerViewFullImg.setAdapter(fullimgAdapter);
        recyclerViewFullImg.scrollToPosition(position);//to set current position in recycler view to adapter

    imgClose.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    });
    }


}
