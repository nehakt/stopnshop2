
package com.stopnshop.user.stopnshop.model.productlist_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("addedDate")
    @Expose
    private String addedDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("product_details")
    @Expose
    private String productDetails;
    @SerializedName("product_image")
    @Expose
    private String productImage;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Datum() {
    }

    /**
     * 
     * @param addedDate
     * @param status
     * @param email
     * @param productDetails
     * @param name
     * @param productImage
     * @param productName
     * @param contact
     */
    public Datum(String name, String email, String contact, String productName, String addedDate, String status, String productDetails, String productImage) {
        super();
        this.name = name;
        this.email = email;
        this.contact = contact;
        this.productName = productName;
        this.addedDate = addedDate;
        this.status = status;
        this.productDetails = productDetails;
        this.productImage = productImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(String productDetails) {
        this.productDetails = productDetails;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

}
