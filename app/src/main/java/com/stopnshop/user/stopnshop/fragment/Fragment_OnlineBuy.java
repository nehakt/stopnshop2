
package com.stopnshop.user.stopnshop.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stopnshop.user.stopnshop.View.LoginActivity;
import com.stopnshop.user.stopnshop.activity.ComboDealActivity;
import com.stopnshop.user.stopnshop.activity.FavoriteListActivity;
import com.stopnshop.user.stopnshop.activity.MyCartActivity;
import com.stopnshop.user.stopnshop.activity.NewAddActivity;
import com.stopnshop.user.stopnshop.activity.NotificationActivity;
import com.stopnshop.user.stopnshop.model.GetProductListData;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.SEARCH_SERVICE;

public class Fragment_OnlineBuy extends Fragment {

    ImageView add_product,imgComboDeal;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    ArrayList<GetProductListData> datum;

    Activity context;

    Cart cart;
    LayerDrawable cartmenuIcon;

    private Login_AllData logindata;
    private String userid="";
    public static int position=0;
    String flag;

    SearchView searchView=null;
    Pager adapter;

    public Fragment_OnlineBuy(){
    }
    public Fragment_OnlineBuy(ArrayList<GetProductListData> datum) {
        this.datum = datum;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (Activity) context;
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume","nResume");
        cart = CartHelper.getCart();
//        context.invalidateOptionsMenu();
        //ActivityCompat.invalidateOptionsMenu(getActivity());
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_onlinebuy, container, false);

        add_product = (ImageView) view.findViewById(R.id.img_add_product);
        imgComboDeal=(ImageView)view.findViewById(R.id.img_comboDeal);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        viewPager = (ViewPager) view.findViewById(R.id.pager);

        setHasOptionsMenu(true);
        cart= CartHelper.getCart();
        flag=SharedPreferenceHelper.getSharedPreferenceString(context,Constant.skip,"");

        add_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NewAddActivity.class);
                startActivity(intent);
            }
        });
        imgComboDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),ComboDealActivity.class);
                startActivity(intent);
            }
        });
        tabLayout.addTab(tabLayout.newTab().setText("All"));

        if (datum != null) {

            for (int i = 0; i < datum.size(); i++) {
                tabLayout.addTab(tabLayout.newTab().setText(datum.get(i).getCategoryName()));
            }
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Initializing viewPager

        //Creating our page+r adapter
        adapter = new Pager(getActivity().getSupportFragmentManager(), tabLayout.getTabCount(), datum);

        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        viewPager.setCurrentItem(0, true);

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        //----------Adding onTabSelectedListener to swipe views--------------
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                position=tab.getPosition();
                Log.e("position",String.valueOf(position));
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        return view;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();//Menu shows in two or more times this problem solve using this code
        inflater.inflate(R.menu.startinfo_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem item=menu.findItem(R.id.logout);
        MenuItem item1=menu.findItem(R.id.favorite_list);
        MenuItem item2=menu.findItem(R.id.notification_bttn);

        if (flag!=null&&flag.equals("skip")) {
            item.setTitle("Login");
            item1.setVisible(false);
            item2.setVisible(false);
        }else {
            item.setTitle("Logout");
            item1.setVisible(true);
            item2.setVisible(true);
        }

        MenuItem  itemCart = menu.findItem(R.id.shopping_bttn);
        cartmenuIcon = (LayerDrawable) itemCart.getIcon();
        showBadgeCount();
        //UtilView.setBadgeCount(context, cartmenuIcon, "" + Cart.mapSize);

        //------For Search Functionality-------
        MenuItem search = menu.findItem(R.id.search_button);
        searchView = (SearchView) MenuItemCompat.getActionView(search);
        SearchManager searchManager = (SearchManager)context.getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(context.getComponentName()));

        if (position==0){
            AllFragment.Search(searchView);
        }else {
            CandiesFragment.SearchCandies(searchView);
        }
    }

    private void showBadgeCount() {

        Gson gson = new Gson();
        Type type = new TypeToken<List<GetProductListData1>>(){}.getType();

        String data = SharedPreferenceHelper.getSharedPreferenceString(context,"cartdata","");

        List<GetProductListData1> list = gson.fromJson(data,type);

        int quantity = 0;

        if(list!=null && list.size()>0) {

//            for (int i = 0; i < list.size(); i++) {
//
//                quantity = quantity + list.get(i).getQuantity();
//            }

            UtilView.setBadgeCount(context, cartmenuIcon, "" + list.size());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.notification_bttn:
                intent = new Intent(context, NotificationActivity.class);
                startActivity(intent);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;

            case R.id.shopping_bttn:
                intent = new Intent(getActivity(), MyCartActivity.class);
                startActivity(intent);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;

            case R.id.search_button:

                return true;

            case R.id.favorite_list:
                intent = new Intent(getActivity(), FavoriteListActivity.class);
                startActivity(intent);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;

            case R.id.logout:
                if (flag!=null&&flag.equals("skip")) {

                    intent = new Intent(context, LoginActivity.class);
                    context.startActivity(intent);
                }
                else {
                    getLogout();
                }
                //------------clear the shared perference data(logindata and userid)------------
                //  SharedPreferenceHelper.setSharedPreferenceString(context, Constant.usertlogindata, "");
//                SharedPreferenceHelper.setSharedPreferenceString(context, "iserid", "");
//                CartHelper.getCart().clear();
//                intent = new Intent(context,LoginActivity.class);
//                startActivity(intent);
//                context.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void getLogout(){
        logindata = Constant.getlogindata(context);

        if (logindata!=null){
            userid= logindata.getId();
        }
        Data data=new Data();
        data.setUserId(userid);
        data.setUser_type(Constant.user_type1);

        ServerRequestApi server_request=new ServerRequestApi("logoutUser",data);
        Gson gson=new Gson();
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        UtilView.showLogCat("logoutReq",gsonString);

        ProgressDialog progressbar = UtilView.showProgressDialog(context);
        AQuery query=new AQuery(context);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);
        //Intent intent=new Intent(context, OrderSummryActivity.class);
//                        startActivity(intent);
        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("orderListResult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            // SharedPreferenceHelper.setSharedPreferenceString(context,Constant.usertlogindata,"");
                            Login_AllData lData= Constant.getlogindata(getActivity());
                            lData.setName("");
                            lData.setLastName("");
                            lData.setEmail("");
                            lData.setContact("");
                            lData.setDob("");

                            Constant.clearLoginData(getActivity(),lData);
                            SharedPreferenceHelper.setSharedPreferenceString(context, "iserid", "");
                            CartHelper.getCart().clear();
                            Intent intent = new Intent(context,LoginActivity.class);
                            startActivity(intent);
                            context.finish();
                        } else
                        {
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    public class Pager extends FragmentStatePagerAdapter {
        private Fragment mCurrentFragment;
        //integer to count number of tabs
        int tabCount;
        List<GetProductListData> datum;

        int i;

        //Constructor to the class
        public Pager(FragmentManager fm, int tabCount, List<GetProductListData> datum) {
            super(fm);
            //Initializing tab count
            this.tabCount= tabCount;
            this.datum= datum;
        }
        @Override
        public Fragment getItem(int pos) {

            //Returning the current tabs

            if(pos==0){

                return new AllFragment(datum);
            }
            else
            {
                List<GetProductListData1> productdata = datum.get(pos-1).getProductData();
                return new CandiesFragment(productdata,datum);
            }
        }
        @Override
        public int getCount()
        {
            return tabCount;
        }
    }
}