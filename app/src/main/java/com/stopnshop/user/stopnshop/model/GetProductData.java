package com.stopnshop.user.stopnshop.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by saurabh on 05-Sep-17.
 */

public class GetProductData {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("msg_code")
    @Expose
    private int msgCode;
    @SerializedName("data")
    @Expose
    private ArrayList<GetProductListData> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(int msgCode) {
        this.msgCode = msgCode;
    }

    public ArrayList<GetProductListData> getData() {
        return data;
    }

    public void setData(ArrayList<GetProductListData> data) {
        this.data = data;
    }
}