package com.stopnshop.user.stopnshop.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.adapter.ComboAdapter;
import com.stopnshop.user.stopnshop.model.ComboDeal.ComboDatum;
import com.stopnshop.user.stopnshop.model.ComboDeal.ComboDealData;
import com.stopnshop.user.stopnshop.model.GetProductListData1;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by neha on 6/12/17.
 */

public class ComboDealActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView txtToolbar;
    RecyclerView recyclerComboDeal;

    ComboAdapter cAdapter;
    List<ComboDatum>comboDatumList;

    Cart cart;
    LayerDrawable cartmenuIcon;

    Gson gson;
    AQuery query;

    @Override
    protected void onResume() {
        super.onResume();
        cart = CartHelper.getCart();
        invalidateOptionsMenu();
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combodeal);

        initView();
        cart= CartHelper.getCart();

        gson=new Gson();
        query=new AQuery(this);

        LinearLayoutManager layout_manager = new LinearLayoutManager(this);
        recyclerComboDeal.setLayoutManager(layout_manager);

        getComboDeal();
    }

    private void initView() {

        recyclerComboDeal = (RecyclerView) findViewById(R.id.recycler_combodeal);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtToolbar = (TextView) findViewById(R.id.toolbar_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtToolbar.setText("Combo Deal");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void getComboDeal(){

        ServerRequestApi server_request=new ServerRequestApi("combodealList",null);
        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        UtilView.showLogCat("combodealReq",gsonString);

        ProgressDialog progressbar = UtilView.showProgressDialog(this);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);

        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("combodealResult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            ComboDealData comboData=gson.fromJson(json.toString(),ComboDealData.class);
                            comboDatumList=comboData.getData();

                            cAdapter=new ComboAdapter(ComboDealActivity.this,comboDatumList);
                            recyclerComboDeal.setAdapter(cAdapter);
                        } else {

                            Log.e("message", json.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
   }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.startinfo_menu, menu);
        //------For Search Functionality-------
        MenuItem item = menu.findItem(R.id.logout);
        MenuItem item1 = menu.findItem(R.id.favorite_list);
        MenuItem item2 = menu.findItem(R.id.notification_bttn);
        MenuItem search = menu.findItem(R.id.search_button);

        item.setVisible(false);
        item1.setVisible(false);
        item2.setVisible(false);
        search.setVisible(false);

        MenuItem itemCart = menu.findItem(R.id.shopping_bttn);
        cartmenuIcon = (LayerDrawable) itemCart.getIcon();
        showBadgeCount();
        //UtilView.setBadgeCount(this, cartmenuIcon, "" + Cart.mapSize);

        return true;
    }

    private void showBadgeCount() {

        Gson gson = new Gson();
        Type type = new TypeToken<List<GetProductListData1>>(){}.getType();

        String data = SharedPreferenceHelper.getSharedPreferenceString(this,"cartdata","");

        List<GetProductListData1> list = gson.fromJson(data,type);

        int quantity = 0;

        if(list!=null && list.size()>0) {

//            for (int i = 0; i < list.size(); i++) {
//
//                quantity = quantity + list.get(i).getQuantity();
//            }

            UtilView.setBadgeCount(this, cartmenuIcon, "" + list.size());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.shopping_bttn) {
            Intent intent = new Intent(ComboDealActivity.this, MyCartActivity.class);
            startActivity(intent);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}