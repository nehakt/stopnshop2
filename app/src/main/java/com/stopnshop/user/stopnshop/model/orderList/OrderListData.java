
package com.stopnshop.user.stopnshop.model.orderList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderListData {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("msg_code")
    @Expose
    private int msgCode;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public OrderListData() {
    }

    /**
     * 
     * @param message
     * @param msgCode
     * @param data
     */
    public OrderListData(String message, int msgCode, List<Datum> data) {
        super();
        this.message = message;
        this.msgCode = msgCode;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(int msgCode) {
        this.msgCode = msgCode;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

}
