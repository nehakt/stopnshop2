package com.stopnshop.user.stopnshop.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stopnshop.user.stopnshop.Notification.Datum;
import com.stopnshop.user.stopnshop.R;

import java.util.List;

/**
 * Created by saurabh on 18-Aug-17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>
{
    private List<Datum>notificationList;
    Activity context;

    public NotificationAdapter(Activity context, List<Datum> notificationList) {
        this.notificationList=notificationList;
        this.context=context;
    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_list_row, parent, false);

        return new NotificationAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.MyViewHolder holder, int position) {

        Datum ndata=notificationList.get(position);

        if (ndata.getAddedDate()!=null){
            holder.notification_date_time.setText("Date: "+ndata.getAddedDate());
        }

        if (ndata.getNotification()!=null){
            holder.notification_txt.setText(ndata.getNotification());
        }
    }

    @Override
    public int getItemCount()
    {
        return notificationList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView notification_date_time, notification_txt;

        public MyViewHolder(View view) {
            super(view);
            notification_date_time = (TextView) view.findViewById(R.id.tv_notificationDate);
            notification_txt = (TextView) view.findViewById(R.id.tv_notificationText);
        }
    }
}