package com.stopnshop.user.stopnshop.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.Msg;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ResetPassword_Activity extends AppCompatActivity implements View.OnClickListener, Validator.ValidationListener {

    ImageView back_btn;
    Button submit_btn;
    TextView title_txt,save_txt;
    Toolbar toolbar;


    private Validator validator;
    @NotEmpty
    private EditText reenter_password_edt;

    @NotEmpty
    private EditText password_edt;

    @NotEmpty
    private EditText otp_edt;
    private String email;
    private AQuery aq;
    private String otp,password,re_enterpoassword;
    private SweetAlertDialog sweetAlertDialogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password_);

        validator = new Validator(this);
        validator.setValidationListener(this);

        intiview();

        Intent intent=getIntent();

        if (intent!=null){

           email= intent.getStringExtra("email");
        }

    }

    private void intiview() {

       // back_btn=(ImageView)findViewById(R.id.back_btn);
        title_txt=(TextView)findViewById(R.id.title_txt);
        reenter_password_edt=(EditText)findViewById(R.id.reenter_password_edt);
        password_edt=(EditText)findViewById(R.id.password_edt);
        otp_edt=(EditText)findViewById(R.id.otp_edt);
        submit_btn=(Button) findViewById(R.id.submit_btn);

        save_txt=(TextView) findViewById(R.id.save_txt);

        save_txt.setVisibility(View.GONE);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        title_txt = (TextView)findViewById(R.id.title_txt);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title_txt.setText("Reset Password");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        back_btn.setOnClickListener(this);
        submit_btn.setOnClickListener(this);

    }


    private void resetpassword() {

        aq = new AQuery(this);
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        Map<String, Object> params = null;
        StringEntity sEntity = null;

        Data data=new Data();

       data.setEmail(email);
        data.setOtp(otp);
        data.setPassword(password);

        ServerRequestApi pozo = new ServerRequestApi("resetpassword",data);
        final Gson gson = new Gson();
        String s = gson.toJson(pozo, ServerRequestApi.class);
        Log.e("@flow", s);

        try {
            sEntity = new StringEntity(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(aq.POST_ENTITY, sEntity);

        aq.progress(progressDialog).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                Log.e("@flow", json.toString());

                if (status.getCode() == 200) {

                    Log.e("@flow", json.toString());

                    try {

                        if (json.getInt("msg_code") == 1) {


                            sweetAlertDialogView = new SweetAlertDialog(ResetPassword_Activity.this, SweetAlertDialog.SUCCESS_TYPE);
                            sweetAlertDialogView.setTitleText("Done")
                                    .setContentText("Password Reset Successfully!")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            Intent intent = new Intent(ResetPassword_Activity.this, LoginActivity.class);
                                            startActivity(intent);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                        }
                                    })
                                    .show();
                            Button viewGroup = (Button) sweetAlertDialogView.findViewById(R.id.confirm_button);
                            viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.unsetColor));


                        } else {
                            UtilView.showToast(ResetPassword_Activity.this,"Incorrect OTP");

                            Log.e("message", json.getString("message"));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.back_btn:
                onBackPressed();
                break;

            case R.id.submit_btn:
                validator.validate();
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {

        otp=  otp_edt.getText().toString().trim();
        password=   password_edt.getText().toString().trim();
        re_enterpoassword= reenter_password_edt.getText().toString().trim();

        if (password.equals(re_enterpoassword)){

            if (InternetConnection.isNetworkAvailable(this)){

                resetpassword();

            }else {

                Msg.t(this, "Please Check Internet Connection");
            }

        }else {

            reenter_password_edt.setError("Password Does not match");
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}