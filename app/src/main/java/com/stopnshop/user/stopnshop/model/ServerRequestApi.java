package com.stopnshop.user.stopnshop.model;


import com.stopnshop.user.stopnshop.util.Data;

/**
 * Created by saurabh on 19-Jun-17.
 */

public class ServerRequestApi {

    private String function_name;
    private Data data;

    public ServerRequestApi(String function_name, Data data) {
        this.function_name = function_name;
        this.data = data;
    }

    public String getFunction_name() {
        return function_name;
    }

    public void setFunction_name(String function_name) {

        this.function_name = function_name;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
