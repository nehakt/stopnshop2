package com.stopnshop.user.stopnshop.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.stopnshop.user.stopnshop.Notification.Datum;
import com.stopnshop.user.stopnshop.Notification.NotificationData;
import com.stopnshop.user.stopnshop.adapter.NotificationAdapter;
import com.stopnshop.user.stopnshop.model.ServerRequestApi;
import com.stopnshop.user.stopnshop.model.logindata_pojo.Login_AllData;
import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.Constant;
import com.stopnshop.user.stopnshop.util.Data;
import com.stopnshop.user.stopnshop.util.InternetConnection;
import com.stopnshop.user.stopnshop.util.Msg;
import com.stopnshop.user.stopnshop.util.UtilView;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView txtToolbar;
    RecyclerView recyclerView;

    List<Datum>notificationList;
    NotificationAdapter mAdapter;

    Gson gson;
    AQuery query;
    Activity activity;
    private Login_AllData logindata;
    private String userid="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        initview();

        gson=new Gson();
        query=new AQuery(this);

        logindata=Constant.getlogindata(this);
        if (logindata!=null) {
            userid =logindata.getId();
        }

        LinearLayoutManager layoutManager=new LinearLayoutManager(this);

        //-----------add new item at top in the list----------
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);

        if (InternetConnection.isNetworkAvailable(NotificationActivity.this)) {

           getNotification();

        } else {

            Msg.t(NotificationActivity.this, "Please Check Internet Connection");
        }
    }
    private void initview() {
        recyclerView = (RecyclerView)findViewById(R.id.recycler_notification);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        txtToolbar=(TextView)findViewById(R.id.toolbar_text);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtToolbar.setText("Notification");
        toolbar.setNavigationIcon(R.drawable.back_btn);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void getNotification(){

        Data data=new Data();
        data.setUserId(userid);

        ServerRequestApi server_request=new ServerRequestApi("Notification",data);

        String gsonString=gson.toJson(server_request,ServerRequestApi.class);
        callNotificationQuery(gsonString);
    }

    private void callNotificationQuery(final String gsonString) {

        ProgressDialog progressbar = UtilView.showProgressDialog(this);

        Map<String, Object> params = null;
        StringEntity sEntity = null;

        try {
            sEntity = new StringEntity(gsonString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params = new HashMap<String, Object>();

        params.put(query.POST_ENTITY, sEntity);

        query.progress(progressbar).ajax(Constant.loginurl, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {

                    UtilView.showLogCat("notificationResult",json.toString());
                    try {
                        if (json.getInt("msg_code") == 1) {

                            NotificationData ndata=gson.fromJson(json.toString(),NotificationData.class);
                            notificationList=ndata.getData();
                            List<Datum>notificationListData=new ArrayList<Datum>();

                            for (Datum notificationData:notificationList){
             if (notificationData.getNotification_type()!=null
                      &&notificationData.getNotification_type().equals("packed")
                      || notificationData.getNotification_type().equals("pickedup")
                      ||notificationData.getNotification_type().equals("available")
                      ||notificationData.getNotification_type().equals("replyfromstore")
                      ||notificationData.getNotification_type().equals("takepayment")){

                        notificationListData.add(notificationData);

                        mAdapter=new NotificationAdapter(activity,notificationListData);
                        recyclerView.setAdapter(mAdapter);
                    }
                            }

                        } else
                        {
                            Log.e("message", json.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }.header("Content-Type", "application/x-www-form-urlencoded"));
    }
}