package com.stopnshop.user.stopnshop.View;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.stopnshop.user.stopnshop.R;
import com.stopnshop.user.stopnshop.util.SharedPreferenceHelper;

/**
 * Created by neha on 19/9/17.
 */

public class SplashActivity extends AppCompatActivity {

public static int POSTDELAYTIME=200;
    String userid="";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

// get userid from shared preferense if not login use shardpreferencehelper
        userid=SharedPreferenceHelper.getSharedPreferenceString(SplashActivity.this,"iserid","");

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent;
                if (userid!=null && !userid.equals("")){

                    intent=new Intent(SplashActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }else {

                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        },POSTDELAYTIME);

    }
}

