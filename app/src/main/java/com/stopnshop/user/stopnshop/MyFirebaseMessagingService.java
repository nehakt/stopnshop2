package com.stopnshop.user.stopnshop;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.stopnshop.user.stopnshop.activity.NotificationActivity;

import java.util.Random;

/**
 * Created by neha on 6/11/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.d(TAG, "Fromuser: " + remoteMessage.getFrom());

        final int not_nu=generateRandom();

        Uri alarmSound = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.icon)
                        .setContentTitle("StopAndShop")
                        .setContentText("You've received new notification.")
                        .setSound(alarmSound);

            Intent resultIntent = new Intent(this, NotificationActivity.class);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this,not_nu , resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            mBuilder.setContentIntent(resultPendingIntent);
            mBuilder.setAutoCancel(true);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            manager.notify(not_nu , mBuilder.build());

//        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        }



    public int generateRandom(){
        Random random = new Random();
        return random.nextInt(9999 - 1000) + 1000;
    }
}

//-------------Firebase Project Name:FirebaseStopNShop----------------
