
package com.stopnshop.user.stopnshop.model.orderList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductDatum implements Serializable{

    @SerializedName("id")
    @Expose
    String product_id;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("productPrice")
    @Expose
    private String productPrice;
    @SerializedName("ProductImage")
    @Expose
    private String productImage;
    @SerializedName("total_item")
    @Expose
    private String totalItems;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ProductDatum() {
    }

    /**
     * 
     * @param productImage
     * @param productPrice
     * @param productName
     */
    public ProductDatum(String productName, String productPrice, String productImage) {
        super();
        this.productName = productName;
        this.productPrice = productPrice;
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
