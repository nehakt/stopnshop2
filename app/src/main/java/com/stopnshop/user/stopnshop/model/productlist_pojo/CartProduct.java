package com.stopnshop.user.stopnshop.model.productlist_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Created by impetrosys on 16/8/17.
 */

public class CartProduct implements Serializable{

    @SerializedName("id")
    @Expose
    String product_id;
    @SerializedName("price")
    @Expose
    String product_price;
    @SerializedName("quantity")
    @Expose
    String product_quantity;
    @SerializedName("name")
    @Expose
    String productName;

    @SerializedName("image")
    @Expose
    String productImage;

    public CartProduct() {
    }

    public CartProduct(String product_id,String productName, String product_quantity, String product_price,String productImage) {

        this.product_id=product_id;
        this.product_quantity=product_quantity;
        this.product_price=product_price;
        this.productName=productName;
        this.productImage=productImage;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_quantity() {
        return product_quantity;
    }

    public void setProduct_quantity(String product_quantity) {
        this.product_quantity = product_quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

}
